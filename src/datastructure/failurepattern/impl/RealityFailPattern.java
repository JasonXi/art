package datastructure.failurepattern.impl;

import java.util.Arrays;

import datastructure.ND.NPoint;
import datastructure.failurepattern.FailurePattern;
import util.TestProgram;

public class RealityFailPattern extends FailurePattern {

	String projectName;

	public RealityFailPattern(String projectName) {
		super();
		this.projectName = projectName;
	}

	@Override
	public void genFailurePattern() {
		// we don't need to generate failure pattern in reality projects
	}

	@Override
	public boolean isCorrect(NPoint p) {
		double[] pxn = p.getXn();
		boolean result = false;
		String message = "the test case p's dimension is not accordingly";
		switch (projectName) {
		case "airy":
			if (pxn.length == 1) {
				result = TestProgram.test_airy(pxn[0]);
			} else {
				System.out.println(message);
			}
			break;
		case "bessj":
			// 2 dimension
			if (pxn.length == 2) {
				result = TestProgram.test_bessj(pxn[0], pxn[1]);
			} else {
				System.out.println(message);
			}
			break;
		case "bessj0":
			// 1 dimension
			if (pxn.length == 1) {
				result = TestProgram.test_bessj0(pxn[0]);
			} else {
				System.out.println(message);
			}
			break;
		case "cel":
			// 4 dimension
			if (pxn.length == 4) {
				double a = pxn[0];
				double b = pxn[1];
				double c = pxn[2];
				double d = pxn[3];
				result = TestProgram.test_cel(a, b, c, d);
			} else {
				System.out.println(message);
			}
			break;
		case "el2":
			// 4
			if (pxn.length == 4) {
				double a = pxn[0];
				double b = pxn[1];
				double c = pxn[2];
				double d = pxn[3];
				result = TestProgram.test_el2(a, b, c, d);
			}
			break;
		case "erfcc":
			// 1
			if (pxn.length == 1) {
				result = TestProgram.test_erfcc(pxn[0]);
			} else {
				System.out.println(message);
			}
			break;
		case "gammq":
			// 2
			if (pxn.length == 2) {
				result = TestProgram.test_gammq(pxn[0], pxn[1]);
			} else {
				System.out.println(message);
			}
			break;
		case "golden":
			// 3
			if (pxn.length == 3) {
				result = TestProgram.test_golden(pxn[0], pxn[1], pxn[2]);
			} else {
				System.out.println(message);
			}
			break;
		case "plgndr":
			// 3
			if (pxn.length == 3) {
				result = TestProgram.test_plgndr(pxn[0], pxn[1], pxn[2]);
			} else {
				System.out.println(message);
			}
			break;
		case "probks":
			// 1
			if (pxn.length == 1) {
				result = TestProgram.test_probks(pxn[0]);
			} else {
				System.out.println(message);
			}
			break;
		case "sncndn":
			// 2
			if (pxn.length == 2) {
				result = TestProgram.test_sncndn(pxn[0], pxn[1]);
			} else {
				System.out.println(message);
			}
			break;
		case "tanh":
			if (pxn.length == 1) {
				result = TestProgram.test_tanh(pxn[0]);
			} else {
				System.out.println(message);
			}
			break;
		default:
			System.out.println("error program name!");
			break;
		}
		return result;
	}

	@Override
	public void showFailurePattern() {
		System.out.println( "RealityFailPattern [projectName=" + projectName + ", dimension=" + dimension + ", min="
				+ Arrays.toString(min) + ", max=" + Arrays.toString(max) + ", fail_rate=" + fail_rate + ", random="
				+ random + "]");
	}


}
