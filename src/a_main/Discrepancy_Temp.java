package a_main;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.simulations.art_orb.ORB_RRT_ND_Xi_Discrepancy;
import test.simulations.art_tp._ND.ART_TP_ND;
//import test.ART;
import java.util.Random;

/**
 * Created by xijiaxiang on 2018/8/23.
 */
public class Discrepancy_Temp {
    public static void main(String[] args) {
        int testCaseNumber = 100;
        int randomDomainsNumber = 1000;

        NPoint[] testSuite = new NPoint[testCaseNumber];
        NRectRegion[] randomRegions = new NRectRegion[randomDomainsNumber];

        //test cases
        double[] min = {0.0, 0.0, 0.0, 0.0};
        double[] max = {1.0, 1.0, 1.0, 1.0};
        double R = 0.75;
        FailurePattern pattern = new BlockPattern();
        pattern.fail_rate = 0.001;
        Random random = new Random(3);
        ORB_RRT_ND_Xi_Discrepancy orb_rrt_nd = new ORB_RRT_ND_Xi_Discrepancy(min, max, R, pattern, random);
        //ARTEP
        ART_TP_ND art_tp_nd=new ART_TP_ND(min,max,pattern,random);

        //testSuite = orb_rrt_nd.generateTC(testCaseNumber);
        for(int i=0;i<testCaseNumber;i++){
            testSuite[i]=art_tp_nd.generateNextTC();
        }

        //regions
        int randomRegionTestNumber[] = new int[randomDomainsNumber];
        for (int i = 0; i < randomDomainsNumber; i++) {
            int dimension = min.length;
            double randomRegionSize = random.nextDouble();
            double randomRegionLength = Math.pow(randomRegionSize, 1.0 / (double) dimension);
            double[] startXn = new double[dimension];
            double[] endXn = new double[dimension];
            for (int j = 0; j < dimension; j++) {
                startXn[j] = 0.0;
                endXn[j] = randomRegionLength;
            }
            NRectRegion region = new NRectRegion(new NPoint(startXn), new NPoint(endXn));
            randomRegions[i] = region;

            //test cases
            for (int j = 0; j < testCaseNumber; j++) {
                if (orb_rrt_nd.isPointInRegion(region, testSuite[j])) {
                    randomRegionTestNumber[i]++;
                }
            }
        }

        //
        double maxDiscrep = -1;
        for (int i = 0; i < randomDomainsNumber; i++) {
            //System.out.println(randomRegionTestNumber[i]);
            double temp = Math.abs(randomRegionTestNumber[i] / (double) testCaseNumber) - (randomRegions[i].size());
            //double temp = Math.abs(randomRegionTestNumber[i] / (double) testCaseNumber) - (randomRegions[i].size()/totalArea); Hilary
            // System.out.println(randomRegionTestNumber[i]+" ");
            if (maxDiscrep < temp) {
                maxDiscrep = temp;
            }
        }



        System.out.println("maxDiscrep:" + maxDiscrep);

        //jiaxiang xi dispersion
        double maxDistance = -1.0;
        for (int i = 0; i < testCaseNumber; i++) {
            //firstly, get ti's nearest TestCase(except ti self)
            double shortestDistance = Double.MAX_VALUE;
            NPoint nearestNPoint=null;
            for (int j = 0; j < testCaseNumber; j++) {
                if (i != j) {
                    double distance = orb_rrt_nd.calTwoPointDistance(testSuite[i], testSuite[j]);
                    if(distance<shortestDistance){
                        shortestDistance=distance;
                        nearestNPoint=testSuite[j];
                    }
                }
            }
            //second, calculate the distance of ti and ti's nearest
            double distance=orb_rrt_nd.calTwoPointDistance(testSuite[i],nearestNPoint);
            if(distance>maxDistance){
                maxDistance=distance;
            }
        }
        //after calculate the maxDistance, print it
        System.out.println("maxDispersion:"+(maxDistance));
    }


}
