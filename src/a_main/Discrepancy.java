package a_main;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.simulations.art_orb.ORB_RRT_ND;

import java.util.Random;

/**
 * Created by xijiaxiang on 2018/8/23.
 */
public class Discrepancy {
    public static void main(String[] args) {
        int testCaseNumber = 1000;
        int randomDomainsNumber = 100;

        NPoint[] testSuite = null;
        NRectRegion[] randomRegions = new NRectRegion[randomDomainsNumber];

        //test cases
        double[] min = {0.0, 0.0};
        double[] max = {1.0, 1.0};
        double R = 0.75;
        FailurePattern pattern = new BlockPattern();
        pattern.fail_rate = 0.001;
        Random random = new Random(3);
        ORB_RRT_ND orb_rrt_nd = new ORB_RRT_ND(min, max, R, pattern, random);
        testSuite = orb_rrt_nd.generateTC(testCaseNumber);
       /* for (int i = 0; i < testCaseNumber; i++) {
            System.out.println(testSuite[i]);
        }*/

        //regions
        int randomRegionTestNumber[] = new int[randomDomainsNumber];
        for (int i = 0; i < randomDomainsNumber; i++) {
            int dimension = min.length;
            double randomRegionSize = random.nextDouble();
            double randomRegionLength = Math.pow(randomRegionSize, 1.0 / (double) dimension);
            double[] startXn = new double[dimension];
            double[] endXn = new double[dimension];
            for (int j = 0; j < dimension; j++) {
                startXn[j] = 0.0;
                endXn[j] = randomRegionLength;
            }
            NRectRegion region = new NRectRegion(new NPoint(startXn), new NPoint(endXn));
            randomRegions[i] = region;

            //test cases
            for (int j = 0; j < testCaseNumber; j++) {
                if (orb_rrt_nd.isPointInRegion(region, testSuite[j])) {
                    randomRegionTestNumber[i]++;
                }
            }
        }

        //
        double maxDiscrep = -1;
        for (int i = 0; i < randomDomainsNumber; i++) {
            //System.out.println(randomRegionTestNumber[i]);
            double temp = Math.abs(randomRegionTestNumber[i] / (double) testCaseNumber) - (randomRegions[i].size());
           // System.out.println(randomRegionTestNumber[i]+" ");
            if (maxDiscrep < temp) {
                maxDiscrep = temp;
            }
        }
        System.out.println("maxDiscrep:" + maxDiscrep);
    }
}
