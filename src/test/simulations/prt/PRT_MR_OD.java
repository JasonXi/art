package test.simulations.prt;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

/***
 *
 * @author xijiaxiang PRT_MR_OD 最大区域，maxRegion
 */
public class PRT_MR_OD {
	public static void main(String[] args) {
		double exp = 3;
		double fail_rate = 0.005;
		int times = 5000;
		long sums = 0;
		// long startTime=System.nanoTime();
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			PRT_MR_OD prt = new PRT_MR_OD((i + 3) * 15, fail_rate, exp);
			int f_measure = prt.run();
			sums += f_measure;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	double fail_start;
	double fail_rate;
	int seedOfRandom;
	double exp;// 分布函数的指数

	ArrayList<TestCase> tests = new ArrayList<>();

	public PRT_MR_OD(int seed, double fail_rate, double exp) {
		this.seedOfRandom = seed;
		this.fail_rate = fail_rate;
		this.exp = exp;
	}

	// check is failure
	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public int run() {
		Random random = new Random(seedOfRandom);
		fail_start = random.nextDouble() * (1 - fail_rate);
		// System.out.println("fail:"+fail_start);
		int count = 0;
		double value = random.nextDouble();
		TestCase p = new TestCase();
		p.p = value;
		while (isCorrect(p.p)) {
			count++;
			/* sort tests */
			if (tests.size() == 0) {
				tests.add(p);
			} else {
				sortTestCases(p);
			}
			/* end sort */
			// init subRegion range
			// every subRegion low and high
			double low = 0.0, high = 1.0;
			double Max = 0.0;
			// Max subRegion low and high and index
			double Mhigh = 1.0, Mlow = 0.0;
			int indexOfMnode = 0;
			for (int i = 0; i < tests.size(); i++) {
				if (i == 0) {
					low = 0.0;
				} else {
					low = (tests.get(i).p + tests.get(i - 1).p) / 2.0;
				}
				if (i == (tests.size() - 1)) {
					high = 1.0;
				} else {
					high = (tests.get(i).p + tests.get(i + 1).p) / 2.0;
				}
				if (Max < (high - low)) {
					Max = high - low;
					Mhigh = high;
					Mlow = low;
					indexOfMnode = i;
				}
			}
			// System.out.println("max:("+Mlow+","+Mhigh+") "+indexOfMnode);
			// 求出概率分布函数的系数
			double cMaxNode = tests.get(indexOfMnode).p;
			double Co = (exp + 1.0)
					/ (Math.pow((cMaxNode - Mlow), (exp + 1.0)) + Math.pow(Mhigh - cMaxNode, (exp + 1.0)));
			// 概率学生成下一个测试用例
			double T = random.nextDouble();
			//// 根据概率分布算出一个随机的值
			// 第一段积分值为intgral(t-r)^3 (low,t)
			double FirstIntgral = Co * (Math.pow(cMaxNode - Mlow, (exp + 1.0)) / (exp + 1.0));
			// double SecondIntgral=Co*(Math.pow(Mhigh-cMaxNode,(exp+1.0))/(exp+1.0));
			p = new TestCase();
			if (T <= FirstIntgral) {
				p.p = cMaxNode - Math.pow((exp + 1.0) * (FirstIntgral - T) / Co, 1.0 / (exp + 1.0));
			} else {
				p.p = cMaxNode + Math.pow((exp + 1.0) * (T - FirstIntgral) / Co, 1.0 / (exp + 1.0));
			}
		}
		// System.out.println("last p:"+p);
		return count;
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}
}
