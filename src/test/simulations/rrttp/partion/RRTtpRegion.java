package test.simulations.rrttp.partion;

import java.util.ArrayList;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;

public class RRTtpRegion{
	public NRectRegion region;
	public NPoint p0;
	public NPoint p1;
	public NPoint p2;
	public NPoint p3;
	public double totalSize;
	public RRTtpRegion() {
		super();
	}
	public RRTtpRegion(NRectRegion region) {
		super();
		this.region = region;
		totalSize=region.size();
	}
	
	public double calAvailSize(double  radius){
		//2维
		double[] start=region.getStart().getXn();
		double[] end=region.getEnd().getXn();
		double xlength=end[0]-start[0];
		double ylength=end[1]-start[1];
		double size=0;
		if(p0!=null||p2!=null){
			//only p0,p2
			if(p0!=null&&p2!=null){
				//p0,p2
				size=twoPointIn(xlength, ylength, radius);
			}else if(p0==null){
				//p2
				size=onePointIn(xlength,ylength,radius);
			}else{
				//p0
				size=onePointIn(xlength, ylength, radius);
			}
		}
		else if(p1!=null||p3!=null){
			//only p1,p3
			if(p1!=null&&p3!=null){
				//p1,p3
				size=twoPointIn(xlength, ylength, radius);
			}else if(p1==null){
				//p3
				size=onePointIn(xlength, ylength, radius);
			}else{
				//p1
				size=onePointIn(xlength, ylength, radius);
			}
		}else{
			//none
			size=region.size();
		}
		return size;
	}
	private double twoPointIn(double xlength,double ylength,double radius){
		double size=0;
		if((xlength>2*radius&&ylength>radius)||(ylength>2*radius&&xlength>radius)){
			//两边都长
			double temp=region.size()-(radius*radius)*2;
			size+=temp;
		}else if(xlength>radius&&xlength<2*radius&&ylength>radius&&ylength<2*radius){
			//相交
			double temp=region.size()-2*(radius*radius);
			double xtemp=2*radius-xlength;
			double ytemp=2*radius-ylength;
			temp+=xtemp*ytemp;
			size=temp;
		}else if((xlength<radius&&ylength>2*radius)||(xlength>2*radius&&ylength<radius)){
			//缺少一块
			double min=xlength>ylength?ylength:xlength;
			double temp=region.size()-2*(radius*radius);
			temp+=2*(radius-min)*radius;
			size=temp;
		}else{size=0;}
		return size;
	}
	private double onePointIn(double xlength,double ylength,double radius){
		double size=0;
		if(xlength>radius&&ylength>radius){
			double temp=region.size()-radius*radius;
			size=temp;
		}else if((xlength>radius&&ylength<radius)||(xlength<radius&&ylength>radius)){
			//
			double min=xlength>ylength?ylength:xlength;
			double temp=region.size()-radius*radius;
			temp+=(radius-min)*radius;
			size=temp;
		}else{
			//none
			size=0;
		}
		return size;
	}
	
	@Override
	public String toString() {
		return "RRTtpRegion [region=" + region + ", p0=" + p0 + ", p1=" + p1 + ", p2=" + p2 + ", p3=" + p3
				+ ", totalSize=" + totalSize + "]";
	}
	
	public ArrayList<NRectRegion> twoPointRegion(double[] start,double[] end,double radius,int type){
		double xlength=end[0]-start[0];
		double ylength=end[1]-start[1];
		
		ArrayList<NRectRegion> regions=new ArrayList<>();
		if((xlength>2*radius&&ylength>2*radius)){
			//两边都长,分为三个区域
			if( type==0){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{start[0]+radius,end[1]})));
				regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0]-radius,end[1]})));
				regions.add(new NRectRegion(new NPoint(new double[]{end[0]-radius,start[1]}),new NPoint(new double[]{end[0],end[1]-radius})));
			}else{
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{start[0]+radius,end[1]-radius})));
				regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0]-radius,end[1]})));
				regions.add(new NRectRegion(new NPoint(new double[]{end[0]-radius,start[1]+radius}),new NPoint(new double[]{end[0],end[1]})));
			}
		}else if((xlength>2*radius&&ylength>radius&&ylength<2*radius)||(xlength>radius&&xlength<2*radius&&ylength>2*radius)){
			//一边长，一边适中
			if(type==0){
				//
				if(xlength>ylength){
					regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{start[0]+radius,end[1]})));
					regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0]-radius,end[1]})));
					regions.add(new NRectRegion(new NPoint(new double[]{end[0]-radius,start[1]}),new NPoint(new double[]{end[0],end[1]-radius})));
				}else{
					regions.add(new NRectRegion(new NPoint(new double[]{start[0],end[1]-radius}), new NPoint(new double[]{end[0]-radius,end[1]})));
					regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{end[0],end[1]-radius})));
					regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}),new NPoint(new double[]{end[0],start[1]+radius})));
				}
			}else{
				if(xlength>ylength){
					regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{start[0]+radius,end[1]-radius})));
					regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0]-radius,end[1]})));
					regions.add(new NRectRegion(new NPoint(new double[]{end[0]-radius,start[1]+radius}),new NPoint(new double[]{end[0],end[1]})));
				}else{
					regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{end[0]-radius,start[1]+radius})));
					regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{end[0],end[1]-radius})));
					regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,end[1]-radius}),new NPoint(new double[]{end[0],end[1]})));
				}
			}
		}else if(xlength>radius&&xlength<2*radius&&ylength>radius&&ylength<2*radius){
			//相交
			if(type==0){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{end[0]-radius,end[1]})));
				regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0],end[1]-radius})));
			}else{
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{end[0]-radius,end[1]-radius})));
				regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]+radius}), new NPoint(new double[]{end[0],end[1]})));
			}
		}else if((xlength<radius&&ylength>2*radius)||(xlength>2*radius&&ylength<radius)){
			//有一边是短的
			if(type==0){
				if(xlength>ylength){
					regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0]-radius,end[1]})));
				}else{
					regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{end[0],end[1]-radius})));
				}
			}else{
				if(xlength>ylength){
					regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0]-radius,end[1]})));
				}else{
					regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{end[0],end[1]-radius})));
				}
			}
		}else{
			
		}
		
		return regions;
	}

	public ArrayList<NRectRegion> onePointRegion(double[] start,double[] end,double radius,int type){
		ArrayList<NRectRegion> regions=new ArrayList<>();
		double xlength=end[0]-start[0];
		double ylength=end[1]-start[1];
		if(type==0){
			if(xlength>radius&&ylength>radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{end[0],end[1]})));
				regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0],start[1]+radius})));
			}else if(xlength>radius&&ylength<radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0],end[1]})));
				
			}else if(xlength<radius&&ylength>radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{end[0],end[1]})));
			}
		}else if(type==1){
			if(xlength>radius&&ylength>radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{end[0]-radius,start[1]+radius})));
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{end[0],end[1]})));
			}else if(xlength>radius&&ylength<radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{end[0]-radius,end[1]})));
			}else if(xlength<radius&&ylength>radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]+radius}), new NPoint(new double[]{end[0],end[1]})));
			}
		}else if(type==2){
			if(xlength>radius&&ylength>radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{end[0],end[1]-radius})));
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],end[1]-radius}), new NPoint(new double[]{end[0]-radius,end[1]})));
			}else if(xlength>radius&&ylength<radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{end[0]-radius,end[1]})));
			}else if(xlength<radius&&ylength>radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{end[0],end[1]-radius})));
			}
		}else{
			if(xlength>radius&&ylength>radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{end[0],end[1]-radius})));
				regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,end[1]-radius}), new NPoint(new double[]{end[0],end[1]})));
			}else if(xlength>radius&&ylength<radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0]+radius,start[1]}), new NPoint(new double[]{end[0],end[1]})));
			}else if(xlength<radius&&ylength>radius){
				regions.add(new NRectRegion(new NPoint(new double[]{start[0],start[1]}), new NPoint(new double[]{end[0],end[1]-radius})));
				
			}
		}
		return regions;
	}
	public ArrayList<NRectRegion> availRegions(double radius){
		double[] start=region.getStart().getXn();
		double[] end=region.getEnd().getXn();
		ArrayList<NRectRegion> results=null;
		if(p0!=null||p2!=null){
			//only p0,p2
			if(p0!=null&&p2!=null){
				//p0,p2
				results=twoPointRegion(start, end, radius, 0);
			}else if(p0==null){
				//p2
				results=onePointRegion(start, end, radius, 2);
			}else{
				//p0
				results=onePointRegion(start, end, radius, 0);
			}
		}
		else if(p1!=null||p3!=null){
			//only p1,p3
			if(p1!=null&&p3!=null){
				//p1,p3
				results=twoPointRegion(start, end, radius, 1);
			}else if(p1==null){
				//p3
				results=onePointRegion(start, end, radius, 3);
			}else{
				//p1
				results=onePointRegion(start, end, radius, 1);
			}
		}else{
			//none
			results=new ArrayList<>();
			results.add(region);
		}
		
		return results;
	}
	
}
