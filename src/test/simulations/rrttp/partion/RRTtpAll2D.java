package test.simulations.rrttp.partion;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import datastructure.failurepattern.impl.RealityFailPattern;
import test.ART;
import tested.bessj;
import tested.gammq;
import util.data.ZeroOneCreator;

/**
 * generate a point from all the available zone;
 * @author xijiaxiang
 * @date   2017/12/07 
 */

public class RRTtpAll2D extends ART {

	double R;
	double radius;
	List<RRTtpRegion> regions=new ArrayList<>();
	
	public RRTtpAll2D(double[] min, double[] max, Random random, FailurePattern failurePattern,double r) {
		super(min, max, random, failurePattern);
		this.R=r;
	}

	@Override
	public int run() {
		int count=0;
		NPoint p=randomCreator.randomPoint();
		//System.out.println("p0:"+p.toString());
		regions.add(new RRTtpRegion(new NRectRegion(new NPoint(min), new NPoint(max))));
		int tempRegionIndex=0;
		
		while(this.failPattern.isCorrect(p)){
			count++;
			
			radius=calculateRadius(count);
//			System.out.println("radius:"+radius);
//			System.out.println("split region:"+tempRegionIndex);
			splitRegions(tempRegionIndex,p);
			ArrayList result=genNextFromAvailableRegion();
			tempRegionIndex=(int)result.get(0);
			p=(NPoint)result.get(1);
		}
		return count;
	}
	private void splitRegions(int regionIndex,NPoint p){
		addRegionsIn2D(regionIndex, p);
	}
	private void addRegionsIn2D(int regionIndex, NPoint p) {
		//first delete it
		RRTtpRegion region=this.regions.remove(regionIndex);
		
		double xmin = region.region.getStart().getXn()[0];
		double ymin = region.region.getStart().getXn()[1];
		double xmax = region.region.getEnd().getXn()[0];
		double ymax = region.region.getEnd().getXn()[1];
		double pp = p.getXn()[0];
		double qq = p.getXn()[1];

		NRectRegion first = new NRectRegion(new NPoint(new double[] { xmin, ymin }),
				new NPoint(new double[] { pp, qq }));
		
		RRTtpRegion one=new RRTtpRegion(first);
		one.p0=region.p0;
		one.p2=p;
		
		
		NRectRegion second = new NRectRegion(new NPoint(new double[] { pp, ymin }),
				new NPoint(new double[] { xmax, qq }));
		
		RRTtpRegion two=new RRTtpRegion(second);
		two.p1=region.p1;
		two.p3=p;
		
		NRectRegion third = new NRectRegion(new NPoint(new double[] { pp, qq }),
				new NPoint(new double[] { xmax, ymax }));
		RRTtpRegion three=new RRTtpRegion(third);
		three.p0=p;
		three.p2=region.p2;
		
		
		NRectRegion fourth = new NRectRegion(new NPoint(new double[] { xmin, qq }),
				new NPoint(new double[] { pp, ymax }));

		RRTtpRegion four=new RRTtpRegion(fourth);
		four.p1=p;
		four.p3=region.p3;
		
		
//		System.out.println("four region:");
//		System.out.println(one);
//		System.out.println(two);
//		System.out.println(three);
//		System.out.println(four);
		
		this.regions.add(one);
		this.regions.add(two);
		this.regions.add(three);
		this.regions.add(four);
	}
	
	private ArrayList genNextFromAvailableRegion(){
		ArrayList result=new ArrayList<>();
		double allSize=0;
		for(int i=0;i<regions.size();i++){
			//System.out.println("region "+i+" size:"+regions.get(i).calAvailSize(radius));
			allSize+=regions.get(i).calAvailSize(radius);
		}
		double T=random.nextDouble()*allSize;
		double SumIntegral = 0.0;// 积分值总和
		double PreIntegral = 0.0;
		int temp = 0;
		for (int i = 0; i < regions.size(); i++) {
			double availSize=regions.get(i).calAvailSize(radius);
			if(availSize!=0.0){
			if (SumIntegral < T) {
				PreIntegral = SumIntegral;
				temp = i;
			}
			SumIntegral += regions.get(i).calAvailSize(radius);
			}else{
				continue;
			}
		}
		
		//System.out.println("gen next test case from :"+temp);
		//temp处生成测试用例
		RRTtpRegion tempRegion=this.regions.get(temp);
		ArrayList<NRectRegion> regions=tempRegion.availRegions(radius);
		NPoint p=randomCreator.randomPoint(regions);
		//System.out.println("p:"+p);
		//0 序号 1是p
		result.add(temp);
		result.add(p);
		return result;
	}
	public boolean inAvailRegion(RRTtpRegion region,NPoint p){
		boolean result=false;
		
		return result;
	}
	public double calculateRadius(int count) {
		if (this.dimension % 2 == 0) {
			int k = this.dimension / 2;
			double kjie = 1;
			for (int i = k; i > 0; i--) {
				kjie *= i;
			}
			double temp = (this.R * totalArea * kjie) / (count * Math.pow(Math.PI, k));

			return Math.pow(temp, 1 / (double) this.dimension);
		} else {
			int k = this.dimension / 2;
			double kjie = 1;
			double k2jie = 1;
			for (int i = k; i > 0; i--) {
				kjie *= i;
			}
			for (int i = (2 * k + 1); i > 0; i--) {
				k2jie *= i;
			}
			double temp = (this.R * totalArea * k2jie) / (kjie * Math.pow(2, 2 * k + 1) * Math.pow(Math.PI, k) * count);
			// System.out.println("return R");
			return Math.pow(temp, 1 / (double) this.dimension);
		}
	}

	public static void main(String[] args) {
		/*int d=2;
		int times=2000;
		
		double fail_rate=0.01;
		
		ZeroOneCreator dataCreator=new ZeroOneCreator();
		double[] min=dataCreator.minCreator(d);
		double[] max=dataCreator.maxCreator(d);
		
		int fm=0;
		for(int i=0;i<times;i++){
			FailurePattern pattern=new BlockPattern();
			pattern.fail_rate=fail_rate;
			RRTtpMZ2D tp=new RRTtpMZ2D(min, max, new Random(i*3), pattern,0.8);
			int temp=tp.run();
			fm+=temp;
		}
		System.out.println(fm/(double)times);*/
		
		testReality();
	}
	
	public static void testReality(){
		//int d=2;
		int times=2000;
		
		//double fail_rate=0.01;
		bessj bessj=new bessj();
		
		//ZeroOneCreator dataCreator=new ZeroOneCreator();
		double[] min=bessj.min;
		double[] max=bessj.max;
		
		int fm=0;
		long starttime=System.currentTimeMillis();
		for(int i=0;i<times;i++){
			FailurePattern pattern=new RealityFailPattern(bessj.getClass().getSimpleName());
			pattern.fail_rate=bessj.failureRate;
			RRTtpAll2D tp=new RRTtpAll2D(min, max, new Random(i*3), pattern,0.8);
			int temp=tp.run();
			fm+=temp;
		}
		long endtime=System.currentTimeMillis();
		System.out.println((fm/(double)times)+" time:"+((endtime-starttime)/(double)times));
	}

	@Override
	public int em() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NPoint generateNextTC() {
		// TODO Auto-generated method stub
		return null;
	}
}
