package test.simulations.rrttp.partion;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import test.simulations.rrt.RRT_ND;
import util.data.DataCreator;
import util.data.ZeroOneCreator;
import util.draw.StdDraw;

/**
 * @author xijiaxiang
 * @date 2017/11/26
 * problems
 */
public class RRTtp2D extends RRT_ND {
	
	List<NPoint> tests = new ArrayList<>((int) (1.0 / this.failPattern.fail_rate));

	List<NRectRegion> regions = new ArrayList<>();

	public RRTtp2D(double[] min, double[] max, Random random, FailurePattern failurePattern, double r) {
		super(min, max, failurePattern, random, r);
	}

	public void calAvailableRegions(NRectRegion currentRegion, NPoint p, double radius) {
		// cut the currentRegion into 8 regions
		// only for 2 dimesion now
		//
		drawSplitLines(currentRegion,p);
		calAvailableRegionsIn2D(currentRegion, p, radius);
	}
	public void drawSplitLines(NRectRegion currentRegion, NPoint p){
		
	}
	public void calAvailableRegionsIn2D(NRectRegion currentRegion, NPoint p, double radius) {
		double[] start = currentRegion.getStart().getXn();
		double[] end = currentRegion.getEnd().getXn();
		double[] pxn = p.getXn();

		// 1st part(contains two small parts)//
		double xlength = pxn[0] - start[0];
		double ylength = pxn[1] - start[1];
		if (ylength > radius) {
			// 有下半部分
			NRectRegion region1 = new NRectRegion(new NPoint(new double[] { start[0], start[1] }),
					new NPoint(new double[] { pxn[0], pxn[1] - radius }));
			this.regions.add(region1);
		}
		if (xlength > radius) {
			// 有左半部分
			NRectRegion region2 = new NRectRegion(
					new NPoint(new double[] { start[0], pxn[1] - radius > start[1] ? pxn[1] - radius : start[1] }),
					new NPoint(new double[] { pxn[0] - radius, pxn[1] }));
			this.regions.add(region2);
		}

		// 2st part
		xlength=end[0]-pxn[0];
		ylength=pxn[1]-start[1];
		if(xlength>radius){
			//有右半部份
			NRectRegion region3=new NRectRegion(
					new NPoint(new double[]{pxn[0]+radius,start[1]}), 
					new NPoint(new double[]{end[0],pxn[1]}));
			this.regions.add(region3);
		}
		if(ylength>radius){
			//左半部分
			NRectRegion region4=new NRectRegion(
					new NPoint(new double[]{pxn[0],start[1]}),
					new NPoint(new double[]{pxn[0]+radius<end[0]?pxn[0]+radius:end[0],pxn[1]-radius}));
			this.regions.add(region4);
		}
		
		//3st part
		xlength=end[0]-pxn[0];
		ylength=end[1]-pxn[1];
		if(xlength>radius){
			//右半部份
			NRectRegion region5=new NRectRegion(
					new NPoint(new double[]{pxn[0]+radius,pxn[1]}), 
					new NPoint(new double[]{end[0],end[1]}));
			this.regions.add(region5);
		}
		if(ylength>radius){
			//左半部分
			NRectRegion region6=new NRectRegion(
					new NPoint(new double[]{pxn[0],pxn[1]+radius}),
					new NPoint(new double[]{pxn[0]+radius<end[0]?pxn[0]+radius:end[0],end[1]}));
			this.regions.add(region6);
		}
		
		//4st part
		xlength=pxn[0] - start[0];
		ylength=end[1]-pxn[1];
		if(xlength>radius){
			//左边
			NRectRegion region7=new NRectRegion(
					new NPoint(new double[]{start[0],pxn[1]}),
					new NPoint(new double[]{pxn[0]-radius,pxn[1]+radius<end[1]?pxn[1]+radius:end[1]}));
			this.regions.add(region7);
		}
		if(ylength>radius){
			//上半部分
			NRectRegion region8=new NRectRegion(
					new NPoint(new double[]{start[0],pxn[1]+radius}),
					new NPoint(new double[]{pxn[0],end[1]}));
			this.regions.add(region8);
		}
		
		for(int i=0;i<regions.size();i++){
			System.out.println(regions.get(i).toString());
		}
	//	System.out.println(this.regions);
	}

	public NPoint genNextPoint() {
		double T = random.nextDouble();
		double allAvailableRegionsSize = 0.0;
		double Co = 0;
		for (int i = 0; i < regions.size(); i++) {
			allAvailableRegionsSize += regions.get(i).size();
		}
		Co = 1.0 / allAvailableRegionsSize;
		double PreIntegral = 0.0;
		double SumIntegral = 0.0;// 积分值总和
		int temp = 0;// 落在哪个区间
		for (int i = 0; i < regions.size(); i++) {
			if (SumIntegral < T) {
				PreIntegral = SumIntegral;
				temp = i;
			}
			SumIntegral += Co * regions.get(i).size();
		}
		//
		NRectRegion currentRegion = regions.remove(temp);
		NPoint p = randomCreator.randomPoint(currentRegion);
		System.out.println("p:"+p.toString());
		double radius=calculateRadius(this.tests.size()+1);
		System.out.println("radius:"+radius);
		calAvailableRegions(currentRegion, p,radius );

		return p;

	}

	@Override
	public int run() {
		int count = 0;
		
		StdDraw.rectangle(0.5, 0.5, 0.5, 0.5);
		failPattern.showFailurePattern();
		// random a point
		NPoint p = randomCreator.randomPoint();
		StdDraw.filledCircle(p.getXn()[0], p.getXn()[1],0.02);
		tests.add(p);
		
		System.out.println("p:"+p);
		//System.out.println("isCorrect:"+);
		double radius = calculateRadius(1);
		System.out.println("radius:"+radius);
		
		calAvailableRegions(new NRectRegion(new NPoint(min), new NPoint(max)), p, radius);
		System.out.println("-------------------");
		while(true){
			p=genNextPoint();
			tests.add(p);
			count++;
			if(!this.failPattern.isCorrect(p)){
				return count;
			}
			System.out.println("---------------");
		}
//		while (this.failPattern.isCorrect(p)) {
//			tests.add(p);
//			count++;
//			System.out.println("count:"+count);
//			p = genNextPoint();
//			System.out.println("p:"+p);
//		}
		//return count;
	}
	
	public static void main(String[] args) {
		int times=1;
		int d=2;
		double fail_rate=0.01;
		double R=1;
		
		DataCreator dataCreator=new ZeroOneCreator();
		
		double[] min=dataCreator.minCreator(d);
		double[] max=dataCreator.maxCreator(d);
		
		FailurePattern pattern=new BlockPattern();
		pattern.fail_rate=fail_rate;
		
		int fm=0;
		for(int i=0;i<times;i++){
			ART method=new RRTtp2D(min, max, new Random(i*3), pattern, R);
			int temp=method.run();
			fm+=temp;
		}
		System.out.println("fm:"+(fm/(double)times));
	}
}
