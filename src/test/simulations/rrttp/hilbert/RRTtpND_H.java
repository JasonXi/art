package test.simulations.rrttp.hilbert;

import java.util.ArrayList;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import test.simulations.rrt.RRT_ND;
import util.HilbertCurve2;
import util.data.ZeroOneCreator;

/**
 * RRT_TP的n维实现（利用希尔伯特曲线）
 * -Djava.library.path="${workspace_loc}/ART/Resource;${env_var:PATH}"
 */
public class RRTtpND_H extends ART {
	public static void main(String[] args) throws Exception {
		 testTCTime(2,10000);
		//testFm();
		//testEm(2, 0.01);
	}


	double R;
	HilbertCurve2 hiblert;

	ArrayList<NPoint> tests = new ArrayList<>();

	public RRTtpND_H(double[] min, double[] max, double r, FailurePattern pattern, Random random) {
		super(min, max, random, pattern);
		this.min = min;
		this.max = max;
		R = r;
		hiblert = new HilbertCurve2();
	}

	public NPoint mapOne2N(NPoint p) {
		double[] nDPoints = hiblert.oneD_2_nD3(p.getXn()[0] + "", this.dimension);
		// 根据min，max来调整
		for (int i = 0; i < nDPoints.length; i++) {
			double length = max[i] - min[i];
			double value = nDPoints[i] * length + min[i];
			nDPoints[i] = value;
		}
		//
		return new NPoint(nDPoints);
	}
	public void sortTestCases(NPoint p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		double value = p.getXn()[0];
		while (low <= high) {
			mid = (low + high) / 2;
			if (value > tests.get(mid).getXn()[0]) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (value < tests.get(mid).getXn()[0]) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}

	@Override
	public NPoint generateNextTC() {
		NPoint pp=null;
		
		if(tests.size()==0){
			NPoint p = randomCreator.randomPoint(0, 1);// 一维的测试用例
			pp = mapOne2N(p);
			tests.add(p);
			//System.out.println(p);
		}else{
			long size = tests.size();
			double radius = R / (2 * size);
			double max = -1;
			double start = 0.0;
			double end = 0.0;
			
			double length = 0;
			double tempstart = 0.0;
			double tempend = 0.0;
			for (int i = 0; i <= size; i++) {

				boolean flag = true;

				if (i == 0) {
					double temp = tests.get(0).getXn()[0];
					if (temp - radius > this.min[0]) {
						length = temp - radius - min[0];
						tempstart = min[0];
						tempend = temp - radius;
					} else {
						flag = false;
					}
				} else if (i == size) {
					double temp = tests.get(i - 1).getXn()[0];
					if (temp + radius <= this.max[0]) {
						length = this.max[0] - (temp + radius);
						tempstart = temp + radius;
						tempend = this.max[0];
					} else {
						flag = false;
					}
				} else {
					double temp1 = tests.get(i).getXn()[0];
					double temp2 = tests.get(i - 1).getXn()[0];
					if (temp1 - temp2 > 2 * radius) {
						length = temp1 - radius - (temp2 + radius);
						tempstart = temp2 + radius;
						tempend = temp1 - radius;
					} else {
						flag = false;
					}
				}
				if (flag) {
					if (max < length) {
						max = length;
						start = tempstart;
						end = tempend;
					}
				} else {
					continue;
				}
			}
			// System.out.println("start:" + start);
			// System.out.println("end:" + end);
			// 选取下一个测试用例
			NPoint p = new NPoint();
			p = randomCreator.randomPoint(start, end);
			
			sortTestCases(p);
			
			pp = mapOne2N(p);
			//System.out.println(p);
		}
		return pp;
	}
	

	public void time() {
		int count = 0;
		NPoint p = randomCreator.randomPoint(0, 1);// 一维的测试用例
		// map function
		NPoint pp = mapOne2N(p);
		while (count < tcCount) {
			count++;
			if (tests.size() == 0) {
				tests.add(p);
			} else {
				sortTestCases(p);
			}
			double radius = R / (2 * tests.size());
			double max = -1;
			double start = 0.0;
			double end = 0.0;
			long size = tests.size();
			double length = 0;
			double tempstart = 0.0;
			double tempend = 0.0;
			for (int i = 0; i <= size; i++) {

				boolean flag = true;

				if (i == 0) {
					double temp = tests.get(0).getXn()[0];
					if (temp - radius > this.min[0]) {
						length = temp - radius - min[0];
						tempstart = min[0];
						tempend = temp - radius;
					} else {
						flag = false;
					}
				} else if (i == size) {
					double temp = tests.get(i - 1).getXn()[0];
					if (temp + radius <= this.max[0]) {
						length = this.max[0] - (temp + radius);
						tempstart = temp + radius;
						tempend = this.max[0];
					} else {
						flag = false;
					}
				} else {
					double temp1 = tests.get(i).getXn()[0];
					double temp2 = tests.get(i - 1).getXn()[0];
					if (temp1 - temp2 > 2 * radius) {
						length = temp1 - radius - (temp2 + radius);
						tempstart = temp2 + radius;
						tempend = temp1 - radius;
					} else {
						flag = false;
					}
				}
				if (flag) {
					if (max < length) {
						max = length;
						start = tempstart;
						end = tempend;
					}
				} else {
					continue;
				}
			}
			// System.out.println("start:" + start);
			// System.out.println("end:" + end);
			// 选取下一个测试用例
			p = new NPoint();
			p = randomCreator.randomPoint(start, end);

			pp = mapOne2N(p);
		}
	}

	public static void testFm() {
		double[] theta = { 0.005 };// , 0.002, 0.0015, 0.001, 0.0005, 0.0001
		int[] d = { 2 };
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		for (int v = 0; v < d.length; v++) {

			double min[] = dataCreator.minCreator(d[v]);
			double max[] = dataCreator.maxCreator(d[v]);
			for (int k = 0; k < theta.length; k++) {

				int times = 2000;
				long sums = 0;
				int temp = 0;
				FailurePattern pattern = new BlockPattern();
				pattern.fail_rate = theta[k];

				long startTime = System.currentTimeMillis();
				for (int i = 0; i < times; i++) {
					RRTtpND_H rrt = new RRTtpND_H(min, max, 0.75, pattern, new Random(i * 3 + v * 5));
					temp = rrt.run();
					sums += temp;
				}
				long endTime = System.currentTimeMillis();
				double fm = (sums / (double) times);
				System.out.println("theta:" + theta[k] + " d:" + d[v] + " Fm:" + fm + " time:"
						+ ((endTime - startTime) / (double) times));

			}
		}
	}

	public static double testTCTime(int d, int tcCount) {
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 1;

		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			RRTtpND_H nd = new RRTtpND_H(min, max, 0.75, failurePattern, new Random(i * 3));
			nd.tcCount = tcCount;
			nd.time2();
		}
		long endTime = System.currentTimeMillis();
		System.out.println((endTime - startTime) / (double) times);
		return ((endTime - startTime) / (double) times);
	}

	public static double[] testEm(int dimension, double failrate) {
		int d = dimension;
		// double R = j == 1 ? 1 : 2;
		int emTime = 6;
		double[] result = new double[emTime];

		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = failrate;
		long startTime = System.currentTimeMillis();
		for (int k = 0; k < emTime; k++) {
			long sums = 0;
			int temp = 0;
			for (int i = 0; i < times; i++) {
				RRTtpND_H nd = new RRTtpND_H(min, max, 0.75, failurePattern, new Random(i * 3));
				nd.emCount = (k + 1) * 500;
				temp = nd.em();
				sums += temp;
			}
			long endTime = System.currentTimeMillis();
			double em = sums / (double) times;
			result[k] = em;
			System.out.println("em:" + em + " time:" + ((endTime - startTime) / (double) times));
		}
		return result;
	}
	
}
