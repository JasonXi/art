package test.simulations.rrttp.hilbert;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;
import util.file.FileUtils;

/**
 * -Djava.library.path="${workspace_loc}/ART/Resource;${env_var:PATH}"
 */
public class RRTtp1D {
	public static void main(String[] args) throws Exception {
		File file = FileUtils.createNewFile("RRTtp_OD");
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));
		double[] theta = { 0.005, 0.002, 0.0015, 0.001, 0.0005, 0.0001 };
		for (int k = 0; k < theta.length; k++) {
			for (int j = 0; j < 50; j++) {
				int times = 2000;
				long sums = 0;
				int temp = 0;
				// tan0 tested = new tan0();
				long startTime = System.currentTimeMillis();
				for (int i = 0; i < times; i++) {
					RRTtp1D rrt_od = new RRTtp1D(0, 1, 0.75, theta[k], i * 3 + j * 5);
					temp = rrt_od.run();
					sums += temp;
				}
				long endTime = System.currentTimeMillis();
				// System.out.print((sums / (double) times) + ",");
				writer.write((sums / (double) times) + ",");
				writer.flush();
			}
			writer.newLine();
		}
		writer.close();
	}
	double min;
	double max;
	double fail_start;
	// String ClassName;
	double fail_rate;
	double R;
	long randomseed;

	ArrayList<TestCase> tests = new ArrayList<>();

	public RRTtp1D(double min, double max, double r, double fail_rate, long randomseed) {
		super();
		this.min = min;
		this.max = max;
		R = r;
		this.fail_rate = fail_rate;
		this.randomseed = randomseed;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public TestCase randomTC(Random random) {
		TestCase temp = new TestCase();
		double temp_value = random.nextDouble() * (max - min) + min;
		temp.p = temp_value;
		return temp;
	}

	public int run() throws Exception {
		Random random = new Random(randomseed);

		int count = 0;
		TestCase p = new TestCase();
		fail_start = random.nextDouble() * (1 - fail_rate);
		// Class<?> classes = Class.forName(this.ClassName);
		double value = random.nextDouble() * (max - min) + min;
		p.p = value;

		while (isCorrect(p.p)) {
			count++;
			if (tests.size() == 0) {
				tests.add(p);
			} else {
				sortTestCases(p);
			}
			// generate next test case by rrttp
			double radius = R / (2 * tests.size());
			// System.out.println("radius:"+radius);
			double max = -1;
			double start = 0.0;
			double end = 0.0;
			for (int i = 0; i <= tests.size(); i++) {
				double length = 0;
				double tempstart = 0.0;
				double tempend = 0.0;
				boolean flag = true;
				// System.out.println("i:"+i);
				if (i == 0) {
					if (tests.get(0).p - radius > this.min) {
						length = tests.get(0).p - radius - min;
						tempstart = min;
						tempend = tests.get(0).p - radius;
						// System.out.println("temp:"+length+","+tempstart+","+tempend);
					} else {
						flag = false;
					}
				} else if (i == tests.size()) {
					if (tests.get(i - 1).p + radius <= this.max) {
						length = this.max - (tests.get(i - 1).p + radius);
						tempstart = tests.get(i - 1).p + radius;
						tempend = this.max;
						// System.out.println("temp:"+length+","+tempstart+","+tempend);
					} else {
						flag = false;
					}
				} else {
					if (tests.get(i).p - tests.get(i - 1).p > 2 * radius) {
						length = tests.get(i).p - radius - (tests.get(i - 1).p + radius);
						tempstart = tests.get(i - 1).p + radius;
						tempend = tests.get(i).p - radius;
						// System.out.println("temp:"+length+","+tempstart+","+tempend);
					} else {
						flag = false;
					}
				}
				if (flag) {
					if (max < length) {
						max = length;
						start = tempstart;
						end = tempend;
					}
				} else {
					continue;
				}
			}
			// System.out.println("start:" + start);
			// System.out.println("end:" + end);
			// 选取下一个测试用例
			p = new TestCase();
			p.p = random.nextDouble() * (end - start) + start;
			// System.out.println("p.p:" + p.p);
			// System.out.println("_______________________");
		}
		return count;
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}
}
