package test.simulations.trianglepartition;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;
import util.CRandomNumber;

//TODO 三角形分割方法，效果一般
public class TrianglePartition {
	public static void main(String[] args) {
		double[] min = { 0, 0 };
		double[] max = { 1, 1 };

		// 测试随机三角形点
		/*
		 * TrianglePartition test = new TrianglePartition(min, max, 3, 0.001);
		 * TriangleRegion testtri = new TriangleRegion(new TestCase(0, 0.5), new
		 * TestCase(0.5,1), new TestCase(1, 0)); StdDraw.line(0, 0.5, 0.5, 1.0);
		 * StdDraw.line(0.5, 1, 1, 0); StdDraw.line(0, 0.5, 1,0);
		 * System.out.println(testtri.getSquare()); for (int i = 0; i < 100; i++) {
		 * TestCase testcase = test.randomTC3(new Random(i*3),testtri);
		 * StdDraw.point(testcase.p, testcase.q); System.out.println(testcase);
		 * System.out.println("======================================="); }
		 */
		int sum = 0;
		int times = 3000;
		for (int i = 0; i < times; i++) {
			TrianglePartition test = new TrianglePartition(min, max, i, 0.01);
			int count = test.run();
			sum += count;
			System.out.println(count);
		}
		System.out.println("fm:" + (sum / (double) times));
	}
	double[] min;
	double[] max;
	long seed;
	double[] fail_start;
	double fail_regionS;

	double fail_rate;

	public TrianglePartition(double[] min, double[] max, long seed, double fail_rate) {
		this.min = min;
		this.max = max;
		this.seed = seed;
		this.fail_rate = fail_rate;
		fail_start = new double[min.length];
		this.fail_regionS = fail_rate * (max[1] - min[1]) * (max[0] - min[0]);
		CRandomNumber.initSeed(this.seed);
	}

	public boolean isCorrect(TestCase p) {
		boolean isCorrect = true;
		if (p.p > fail_start[0] && p.p < fail_start[0] + Math.sqrt(fail_regionS)) {
			if (p.q > fail_start[1] && p.q < fail_start[1] + Math.sqrt(fail_regionS)) {
				isCorrect = false;
			}
		}
		return isCorrect;
	}

	public TestCase randomTC(Random random) {
		// CRandomNumber

		TestCase temp = new TestCase();
		double p = CRandomNumber.randomi0i1() * (max[0] - min[0]) + min[0];
		double q = CRandomNumber.randomi0i1() * (max[1] - min[1]) + min[1];
		temp.p = p;
		temp.q = q;
		return temp;
	}
	// public Test

	public TestCase randomTC(Random random, TriangleRegion region) {
		double s1, s2, s3 = 0;
		TestCase temp = null;
		double sum = region.getSquare();
		int count = 0;
		do {
			temp = randomTC(random);
			TriangleRegion region1 = new TriangleRegion();
			region1.setPs(region.getP1(), region.getP2(), temp);
			s1 = region1.getSquare();
			TriangleRegion region2 = new TriangleRegion();
			region2.setPs(region.getP2(), region.getP3(), temp);
			s2 = region2.getSquare();
			TriangleRegion region3 = new TriangleRegion();
			region3.setPs(region.getP1(), region.getP3(), temp);
			s3 = region3.getSquare();
			count++;
			if (count > 2) {
				// System.out.println("again!!!");
			}
		} while ((s1 + s2 + s3) != sum);
		return temp;
	}

	// 使用向量生成(非均匀)
	public TestCase randomTC2(Random random, TriangleRegion region) {
		// Vector a=new Vector<>();
		// Vector2D vector1=new Vector2D(region.getP1().p,region.getP1().q);
		// Vector
		TestCase ji = region.getP1();
		TestCase jiao1 = region.getP2();
		TestCase jiao2 = region.getP3();
		/*
		 * System.out.println("ji:"+ji); System.out.println("jiao1:"+jiao1);
		 * System.out.println("jiao2:"+jiao2);
		 */
		TestCase u = new TestCase(jiao1.p - ji.p, jiao1.q - ji.q);
		TestCase v = new TestCase(jiao2.p - ji.p, jiao2.q - ji.q);
		// System.out.println("U:"+u+",v:"+v);
		TestCase temp = null;
		temp = randomTC(random);
		while (temp.p + temp.q > 1) {
			temp = randomTC(random);
		}
		// System.out.println("temp:"+temp);
		double x = u.p * temp.p + v.p * temp.q;
		double y = u.q * temp.p + v.q * temp.q;
		// System.out.println("---------------");
		return new TestCase(x + ji.p, y + ji.q);

	}

	// 非均匀
	public TestCase randomTC2(Random random, TriangleRegion region, double radius) {
		// Vector a=new Vector<>();
		// Vector2D vector1=new Vector2D(region.getP1().p,region.getP1().q);
		// Vector
		System.out.println(region);
		TestCase p1 = region.getP1();
		TestCase p2 = region.getP2();
		TestCase p3 = region.getP3();

		TestCase ji = p2;
		TestCase temp1 = new TestCase();
		temp1.p = (p1.p * radius - p2.p * radius + p1.p * p2.q - p1.p * p1.q) / (p2.q - p1.q);
		temp1.q = p1.q - radius;
		TestCase temp2 = new TestCase();
		temp2.p = (p1.p * radius - p3.p * radius + p1.p * p3.q - p1.p * p1.q) / (p3.q - p1.q);
		temp2.q = p1.q - radius;
		TestCase u1 = new TestCase(temp1.p - ji.p, temp1.q - ji.q);
		TestCase v1 = new TestCase(temp2.p - ji.p, temp2.q - ji.q);
		TestCase w1 = new TestCase(p3.p - ji.p, p3.q - ji.q);
		//
		TestCase result1 = null;
		result1 = randomTC(random);
		while (result1.p + result1.q > 1) {
			result1 = randomTC(random);
		}
		// System.out.println("temp:"+temp);
		result1.p = u1.p * result1.p + v1.p * result1.q;
		result1.q = u1.q * result1.p + v1.q * result1.q;
		// System.out.println("---------------");
		TestCase result2 = null;
		result2 = randomTC(random);
		while (result2.p + result2.q > 1) {
			result2 = randomTC(random);
		}
		// System.out.println("temp:"+temp);
		result2.p = v1.p * result2.p + w1.p * result2.q;
		result2.q = v1.q * result2.p + w1.q * result2.q;

		System.out.println("result1:" + result1);
		System.out.println("result2:" + result2);
		if (random.nextBoolean()) {
			return result1;
		} else {
			return result2;
		}
	}

	// 使用向量P=(1−√r1)A+(√r1(1−r2))B+(r2√r1)C
	public TestCase randomTC3(Random random, TriangleRegion region) {
		// Vector a=new Vector<>();
		// Vector2D vector1=new Vector2D(region.getP1().p,region.getP1().q);
		// Vector
		TestCase t1 = region.getP1();
		TestCase t2 = region.getP2();
		TestCase t3 = region.getP3();
		// TestCase vector1=new TestCase(t2.p-t1.p,t2.q-t1.q);
		// TestCase vector2=new TestCase(t3.p-t1.p,t3.q-t1.q);
		double r1 = randomTC(random).p;
		double r2 = randomTC(random).q;
		double x = (1 - Math.sqrt(r1)) * t1.p + Math.sqrt(r1) * (1 - r2) * t2.p + r2 * Math.sqrt(r1) * t3.p;
		double y = (1 - Math.sqrt(r1)) * t1.q + Math.sqrt(r1) * (1 - r2) * t2.q + r2 * Math.sqrt(r1) * t3.q;
		return new TestCase(x, y);
	}

	public TestCase randomTC3(Random random, TriangleRegion region, double radius) {
		TestCase p1 = region.getP1();
		TestCase p2 = region.getP2();
		TestCase p3 = region.getP3();
		TestCase p4 = new TestCase();
		TestCase p5 = new TestCase();
		p4.p = 1 - radius * ((p1.p - p2.p) / (p1.q - p2.q));
		p4.q = p1.q - radius;
		p5.p = 1 - radius * ((p1.p - p3.p) / (p1.q - p3.q));
		p5.q = p1.q - radius;
		// p2,p4,p5 一个三角形
		// p2,p3,p5
		double r1 = randomTC(random).p;
		double r2 = randomTC(random).q;
		double x = (1 - Math.sqrt(r1)) * p2.p + Math.sqrt(r1) * (1 - r2) * p4.p + r2 * Math.sqrt(r1) * p5.p;
		double y = (1 - Math.sqrt(r1)) * p2.q + Math.sqrt(r1) * (1 - r2) * p4.q + r2 * Math.sqrt(r1) * p5.q;
		TestCase result1 = new TestCase(x, y);
		//
		r1 = randomTC(random).p;
		r2 = randomTC(random).q;
		x = (1 - Math.sqrt(r1)) * p2.p + Math.sqrt(r1) * (1 - r2) * p3.p + r2 * Math.sqrt(r1) * p5.p;
		y = (1 - Math.sqrt(r1)) * p2.q + Math.sqrt(r1) * (1 - r2) * p3.q + r2 * Math.sqrt(r1) * p5.q;
		TestCase result2 = new TestCase(x, y);
		if (randomTC(random).p <= 0.5) {
			return result1;
		} else {
			return result2;
		}
	}

	public int run() {
		int count = 0;
		Random random = new Random(seed);
		ArrayList<TriangleRegion> regionlist = new ArrayList<>();
		ArrayList<TestCase> tests = new ArrayList<>();
		fail_start[0] = random.nextDouble() * (max[0] - min[0] - Math.sqrt(fail_regionS)) + min[0];
		fail_start[1] = random.nextDouble() * (max[1] - min[1] - Math.sqrt(fail_regionS)) + min[1];
		System.out.println("failstart:" + (fail_start[0] + "," + fail_start[1]));
		// 产生第一个测试用例
		TestCase p = randomTC(random);
		tests.add(p);
		System.out.println("p0:" + p);
		// 添加四块新区域
		TriangleRegion region1 = new TriangleRegion();
		region1.setPs(p, new TestCase(0, 0), new TestCase(0, 1));
		regionlist.add(region1);
		TriangleRegion region2 = new TriangleRegion();
		region2.setPs(p, new TestCase(0, 1), new TestCase(1, 1));
		regionlist.add(region2);
		TriangleRegion region3 = new TriangleRegion();
		region3.setPs(p, new TestCase(1, 0), new TestCase(1, 1));
		regionlist.add(region3);
		TriangleRegion region4 = new TriangleRegion();
		region4.setPs(p, new TestCase(0, 0), new TestCase(1, 0));
		regionlist.add(region4);
		while (isCorrect(p)) {
			count++;
			//
			// if (count == 3) {
			// // StdDraw.filledRectangle(0.5, 0.5, 0.5, 0.5);
			// StdDraw.setPenColor(StdDraw.BOOK_BLUE);
			// // StdDraw.line(0, 1, 1, 0);
			// // StdDraw.line(0, 0, 1, 1);
			// double tempradius = Math.sqrt(
			// 0.4 * (this.max[1] - this.min[1]) * (this.max[0] - this.min[0]) /
			// (tests.size() * Math.PI));
			// // StdDraw.line(0, 0, 1, 1);
			// for (int i = 0; i < tests.size(); i++) {
			//
			// TestCase temp = tests.get(i);
			// StdDraw.line(0, temp.q - tempradius, 1, temp.q - tempradius);
			//
			// StdDraw.filledCircle(temp.p, temp.q, 0.01);
			// }
			// }

			// 找出最大区域
			double max = 0;
			TriangleRegion maxregion = null;
			int maxindex = 0;
			for (int i = 0; i < regionlist.size(); i++) {
				double temp = regionlist.get(i).getSquare();
				if (temp > max) {
					max = temp;
					maxregion = regionlist.get(i);
					maxindex = i;
				}
			}
			/// 从这里开始加强
			// double radius = Math
			// .sqrt(0.4 * (this.max[1] - this.min[1]) * (this.max[0] -
			// this.min[0]) / (tests.size() * Math.PI));
			p = new TestCase();
			// maxregion=smallerRegion(maxregion);
			// p = randomTC3(random, maxregion);
			p.p = (maxregion.p1.p + maxregion.p2.p + maxregion.p3.p) / 3.0;
			p.q = (maxregion.p1.q + maxregion.p2.q + maxregion.p3.q) / 3.0;
			tests.add(p);
			System.out.println("p" + count + ":" + p);
			// 删除该最大区域，并重新划分最大区域
			// regionlist.remove(maxindex);
			regionlist.set(maxindex, new TriangleRegion(new TestCase(0, 0), new TestCase(0, 0), new TestCase(0, 0)));

			TriangleRegion temp1 = new TriangleRegion();
			temp1.setPs(p, maxregion.getP1(), maxregion.getP2());
			regionlist.add(temp1);
			TriangleRegion temp2 = new TriangleRegion();
			temp2.setPs(p, maxregion.getP1(), maxregion.getP3());
			regionlist.add(temp2);
			TriangleRegion temp3 = new TriangleRegion();
			temp3.setPs(p, maxregion.getP2(), maxregion.getP3());
			regionlist.add(temp3);

		}
		return count;
	}

	public TriangleRegion smallerRegion(TriangleRegion region) {
		TriangleRegion result = new TriangleRegion();
		result = region;
		// 待完成
		return result;
	}
}

class TriangleRegion {
	TestCase p1;
	TestCase p2;
	TestCase p3;

	public TriangleRegion() {
		super();
	}

	public TriangleRegion(TestCase p1, TestCase p2, TestCase p3) {
		super();
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}

	public TestCase getP1() {
		return p1;
	}

	public TestCase getP2() {
		return p2;
	}

	public TestCase getP3() {
		return p3;
	}

	public double getSquare() {
		// (1/2)*(p1.pp2.q+p2.pp3.q+p3.pp1.q-p1.pp3.q-p2.pp1.q-p3.pp2.q)
		/*
		 * System.out.println("p1:" + p1); System.out.println("p2:" + p2);
		 * System.out.println("p3:" + p3); System.out.println("square:" + ((0.5)
		 * Math.abs(p1.p * p2.q + p2.p * p3.q + p3.p * p1.q - p1.p * p3.q - p2.p * p1.q
		 * - p3.p * p2.q)));
		 */
		return (0.5) * Math.abs(p1.p * p2.q + p2.p * p3.q + p3.p * p1.q - p1.p * p3.q - p2.p * p1.q - p3.p * p2.q);
	}

	public void setP1(TestCase p1) {
		this.p1 = p1;
	}

	public void setP2(TestCase p2) {
		this.p2 = p2;
	}

	public void setP3(TestCase p3) {
		this.p3 = p3;
	}

	public void setPs(TestCase p1, TestCase p2, TestCase p3) {
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}

	@Override
	public String toString() {
		return "TriangleRegion [p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + "]";
	}

}
