package test.simulations.art_b;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import util.PaiLie;
import util.data.ZeroOneCreator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ART_B2_ND extends ART {
	public static void main(String[] args) {

		//testEm(1, 0.01);
		//testTCTime(2,10000);
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double[] min = dataCreator.minCreator(2);
		double[] max = dataCreator.maxCreator(2);
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		failurePattern.min = min;
		failurePattern.max = max;
		failurePattern.dimension = 2;


		/*ART_B2_ND test=new ART_B2_ND(min,max,new Random(3),failurePattern);

		for(int i=0;i<10;i++){
			NPoint p=test.generateNextTC();
			System.out.println(p);
		}*/

		int fm = 0;
		long startTime = System.currentTimeMillis();
		int times=100;
		for (int i = 0; i < times; i++) {
			ART_B2_ND test=new ART_B2_ND(min,max,new Random(i*3),failurePattern);
			int temp=test.run();
			fm += temp;
		}
		long endTime = System.currentTimeMillis();
		//  writer.write((fm / (double) times) + "");
		//  writer.newLine();
		System.out
				.println("Fm:" + (fm / (double) times) + " times:" + ((endTime - startTime) / (double) times));


		 fm = 0;
		 startTime = System.currentTimeMillis();
		// times=100;
		for (int i = 0; i < times; i++) {
			ART_B_ND test=new ART_B_ND(min,max,new Random(i*3),failurePattern);
			int temp=test.run();
			fm += temp;
		}
		 endTime = System.currentTimeMillis();
		//  writer.write((fm / (double) times) + "");
		//  writer.newLine();
		System.out
				.println("Fm:" + (fm / (double) times) + " times:" + ((endTime - startTime) / (double) times));


	}

	ArrayList<NRectRegion> untestedRegions = new ArrayList<>();
	ArrayList<NRectRegion> testedRegions = new ArrayList<>();

	ArrayList<NPoint> tests = new ArrayList<>();

	public ART_B2_ND(double[] min, double[] max, Random random, FailurePattern failurePattern) {
		super(min, max, random, failurePattern);
		untestedRegions.add(new NRectRegion(new NPoint(min), new NPoint(max)));
	}

	public NRectRegion findRandomRegionAndDelete(ArrayList<NRectRegion> regions) {
		int T = random.nextInt(regions.size());
		return regions.remove(T);
	}

	/*public NRectRegion findRectangleRegion(ArrayList<NRectRegion> regions){

	}*/
	public boolean hasPointInRegion(NRectRegion region) {
		boolean result = false;
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();

		for (int i = 0; i < tests.size(); i++) {
			double[] p = tests.get(i).getXn();
			boolean isPIn = true;
			for (int j = 0; j < p.length; j++) {
				if (p[j] < start[j] || p[j] > end[j]) {
					isPIn = false;
				}
			}
			if (isPIn) {
				result = true;
				break;
			}
		}
		return result;
	}
	public void addRegionsInND(NRectRegion region, NPoint p,ArrayList<NRectRegion> temp) throws Exception {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		//System.out.println("result1 size:"+result1.size());
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}
			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			//this.regions.add(tempRegion);
			if(hasPointInRegion(tempRegion)){
				temp.add(tempRegion);
			}else{
				this.untestedRegions.add(tempRegion);
			}

		}
	}

	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}
	private NPoint midPoint(NRectRegion region){
		double[] start=region.getStart().getXn();
		double[] end=region.getEnd().getXn();
		double[] result=new double[start.length];
		for(int i=0;i<result.length;i++){
			double midpoint=0.5*(start[i]+end[i]);
			result[i]=midpoint;
		}

		 return new NPoint(result);
	}
	@Override
	public NPoint generateNextTC() {
		NPoint p = null;
		if (untestedRegions.size() != 0) {
			// 直接生成一个点
			NRectRegion randomRegion = findRandomRegionAndDelete(untestedRegions);
			p = randomCreator.randomPoint(randomRegion);
			tests.add(p);
			testedRegions.add(randomRegion);
		} else {
			ArrayList<NRectRegion> temp = new ArrayList<>();
			if(untestedRegions.size()==0){
				for(int i=0;i<testedRegions.size();i++){
					//4块
					NPoint midPoint=midPoint(testedRegions.get(i));
					try {
						addRegionsInND(testedRegions.get(i),midPoint,temp);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				testedRegions = new ArrayList<>();
				testedRegions = temp;
			}


			// 分完区域重新生成一个点

/*
			int candidateSize=untestedRegions.size();
			NPoint[]  candidates=new NPoint[candidateSize];
			NRectRegion[] randomRegion=new NRectRegion[candidateSize];
			for(int i=0;i<candidateSize;i++){
				int T = random.nextInt(untestedRegions.size());
				randomRegion[i]= untestedRegions.get(T);
				candidates[i]=randomCreator.randomPoint(randomRegion[i]);
			}

			//fscs
			double maxDistance = -1.0;
			NPoint bestCandidate = null;
			int bestIndex=-1;
			for (int i = 0; i < candidateSize; i++) {
				//NPoint candidate = randomCreator.randomPoint();
				// 计算两个点的距离
				double minDistance = Double.MAX_VALUE;

				for (int j = 0; j < this.tests.size(); j++) {
					double tempDistance = candidates[i].distanceToAnotherPoint(tests.get(j));
					if (tempDistance < minDistance) {
						minDistance = tempDistance;
					}
				}
				if (maxDistance < minDistance) {
					maxDistance = minDistance;
					bestCandidate = candidates[i];
					bestIndex=i;
				}
			}
			p = bestCandidate;
			tests.add(p);
			NRectRegion removedRegion=untestedRegions.remove(bestIndex);
			testedRegions.add(removedRegion);

*/

			NRectRegion randomRegion = findRandomRegionAndDelete(untestedRegions);
			p = randomCreator.randomPoint(randomRegion);
			tests.add(p);
			testedRegions.add(randomRegion);
		}
		return p;
	}

	

}
