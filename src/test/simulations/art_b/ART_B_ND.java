package test.simulations.art_b;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import test.simulations.fscs.FSCS_ND;
import util.data.ZeroOneCreator;

public class ART_B_ND extends ART {
	public static void main(String[] args) {

		//testEm(1, 0.01);
		//testTCTime(2,10000);
		
		testFm();
	}

	ArrayList<NRectRegion> untestedRegions = new ArrayList<>();
	ArrayList<NRectRegion> testedRegions = new ArrayList<>();

	ArrayList<NPoint> tests = new ArrayList<>();

	public ART_B_ND(double[] min, double[] max, Random random, FailurePattern failurePattern) {
		super(min, max, random, failurePattern);
		untestedRegions.add(new NRectRegion(new NPoint(min), new NPoint(max)));
	}

	public NRectRegion findRandomRegionAndDelete(ArrayList<NRectRegion> regions) {
		int T = random.nextInt(regions.size());
		return regions.remove(T);
	}

	public boolean hasPointInRegion(NRectRegion region) {
		boolean result = false;
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();

		for (int i = 0; i < tests.size(); i++) {
			double[] p = tests.get(i).getXn();
			boolean isPIn = true;
			for (int j = 0; j < p.length; j++) {
				if (p[j] < start[j] || p[j] > end[j]) {
					isPIn = false;
				}
			}
			if (isPIn) {
				result = true;
				break;
			}
		}
		return result;
	}

	@Override
	public NPoint generateNextTC() {
		NPoint p = null;
		if (untestedRegions.size() != 0) {
			// 直接生成一个点
			NRectRegion randomRegion = findRandomRegionAndDelete(untestedRegions);
			p = randomCreator.randomPoint(randomRegion);
			tests.add(p);
			testedRegions.add(randomRegion);
		} else {
			ArrayList<NRectRegion> temp = new ArrayList<>();
			for (int i = 0; i < testedRegions.size(); i++) {
				// 找最长边
				NRectRegion tempRegion = testedRegions.get(i);
				double maxBian = 0.0;
				int maxIndex = 0;
				for (int j = 0; j < tempRegion.getStart().getXn().length; j++) {
					if (tempRegion.getEnd().getXn()[j] - tempRegion.getStart().getXn()[j] > maxBian) {
						maxBian = tempRegion.getEnd().getXn()[j] - tempRegion.getStart().getXn()[j];
						maxIndex = j;
					}
				}
				NRectRegion region1 = new NRectRegion();
				NRectRegion region2 = new NRectRegion();
				region1.setStart(tempRegion.getStart());
				double[] end = Arrays.copyOf(tempRegion.getEnd().getXn(), tempRegion.getEnd().getXn().length);
				double midValue1 = 0.5
						* (tempRegion.getEnd().getXn()[maxIndex] + tempRegion.getStart().getXn()[maxIndex]);
				end[maxIndex] = midValue1;
				region1.setEnd(new NPoint(end));
				if (hasPointInRegion(region1)) {
					temp.add(region1);
				} else {
					untestedRegions.add(region1);
				}

				double[] start = Arrays.copyOf(tempRegion.getStart().getXn(), tempRegion.getStart().getXn().length);
				start[maxIndex] = midValue1;
				region2.setStart(new NPoint(start));
				region2.setEnd(tempRegion.getEnd());
				if (hasPointInRegion(region2)) {
					temp.add(region2);
				} else {
					untestedRegions.add(region2);
				}
			}
			testedRegions = new ArrayList<>();
			testedRegions = temp;

			// 分完区域重新生成一个点
			NRectRegion randomRegion = findRandomRegionAndDelete(untestedRegions);
			p = randomCreator.randomPoint(randomRegion);
			tests.add(p);
			testedRegions.add(randomRegion);
		}
		return p;
	}

	
	public void time() {
		int count = 0;
		while (count < tcCount) {
			NPoint p = null;
			while (untestedRegions.size() != 0 && count < tcCount) {
				NRectRegion randomRegion = findRandomRegionAndDelete(untestedRegions);
				// System.out.println("randomRegion:"+randomRegion);
				p = randomCreator.randomPoint(randomRegion);
				// System.out.println("point:"+p);
				count++;
				tests.add(p);

				testedRegions.add(randomRegion);

			}
			ArrayList<NRectRegion> temp = new ArrayList<>();
			for (int i = 0; i < testedRegions.size(); i++) {
				// 找最长边
				NRectRegion tempRegion = testedRegions.get(i);
				double maxBian = 0.0;
				int maxIndex = 0;
				for (int j = 0; j < tempRegion.getStart().getXn().length; j++) {
					if (tempRegion.getEnd().getXn()[j] - tempRegion.getStart().getXn()[j] > maxBian) {
						maxBian = tempRegion.getEnd().getXn()[j] - tempRegion.getStart().getXn()[j];
						maxIndex = j;
					}
				}
				//
				NRectRegion region1 = new NRectRegion();
				NRectRegion region2 = new NRectRegion();

				region1.setStart(tempRegion.getStart());
				double[] end = Arrays.copyOf(tempRegion.getEnd().getXn(), tempRegion.getEnd().getXn().length);
				double midValue1 = 0.5
						* (tempRegion.getEnd().getXn()[maxIndex] + tempRegion.getStart().getXn()[maxIndex]);
				end[maxIndex] = midValue1;
				region1.setEnd(new NPoint(end));
				if (hasPointInRegion(region1)) {
					temp.add(region1);
				} else {
					untestedRegions.add(region1);
				}

				double[] start = Arrays.copyOf(tempRegion.getStart().getXn(), tempRegion.getStart().getXn().length);
				start[maxIndex] = midValue1;
				region2.setStart(new NPoint(start));
				region2.setEnd(tempRegion.getEnd());
				if (hasPointInRegion(region2)) {
					temp.add(region2);
				} else {
					untestedRegions.add(region2);
				}
			}
			testedRegions = temp;
		}
	}

	
	public static double testFm() {
		int d = 2;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_B_ND rt = new ART_B_ND(min, max, new Random(i * 3), failurePattern);
			temp = rt.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		double fm = sums / (double) times;
		System.out.println("fm:" + fm + " time:" + ((endTime - startTime) / (double) times));
		return fm;
	}

	public static void testEm(int dimension, double failrate) {
		int d = dimension;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 1000;

		int temp = 0;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = failrate;
		for (int k = 0; k < 6; k++) {
			long sums = 0;
			long startTime = System.currentTimeMillis();
			for (int i = 0; i < times; i++) {
				ART_B_ND rt = new ART_B_ND(min, max, new Random(i * 3), failurePattern);
				rt.emCount = (k + 1) * 500;
				temp = rt.em();
				sums += temp;
			}
			long endTime = System.currentTimeMillis();
			double em = sums / (double) times;
			System.out.println("em:" + em + " time:" + ((endTime - startTime) / (double) times));
		}
		System.out.println();
	}

	public static double testTCTime(int d, int tcCount) {
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 1;

		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_B_ND rt = new ART_B_ND(min, max, new Random(i * 3), failurePattern);
			rt.tcCount = tcCount;
			rt.time2();
		}
		long endTime = System.currentTimeMillis();
		System.out.println((endTime - startTime) / (double) times);
		return ((endTime - startTime) / (double) times);
	}

}
