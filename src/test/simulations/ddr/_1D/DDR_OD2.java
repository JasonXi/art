package test.simulations.ddr._1D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

public class DDR_OD2 {
	public static void main(String[] args) {
		int times = 70;
		long sums = 0;
		int temp = 0;
		int s = 10;
		//////////////
		///////////////
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			DDR_OD2 rrt_od = new DDR_OD2(0, 1, 0.75, s, 0.0001, i * 3);// 0.002 refers to failure rate
			temp = rrt_od.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	double min;
	double max;
	double fail_start;
	double fail_rate;
	double R;
	int s;
	int randomseed;

	ArrayList<TestCase> tests = new ArrayList<>();

	public DDR_OD2(double min, double max, double r, int s, double fail_rate, int randomseed) {
		super();
		this.min = min;
		this.max = max;
		R = r;
		this.s = s;
		this.fail_rate = fail_rate;
		this.randomseed = randomseed;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public TestCase randomTC(Random random) {
		TestCase temp = new TestCase();
		double temp_value = random.nextDouble() * (max - min) + min;
		temp.p = temp_value;
		return temp;
	}

	public int run() {
		Random random = new Random(randomseed);
		fail_start = random.nextDouble() * (1 - fail_rate);
		int count = 0;// 记录测试用例数量
		int rrtcount = 0;
		int fscscount = 0;
		int _10CandidateCount = 0;// 每十个一次的数量
		TestCase p = randomTC(random);// 第一个测试用例
		// System.out.println("p0:" + p.p);
		while (isCorrect(p.p)) {
			// different radius//s*(_10CandidateCount+1
			double radius = R / (s + _10CandidateCount);
			// 生成s个候选测试用例,从s个候选测试用例中挑选符合要求的测试用例
			boolean all_s_has_E_flag = true;
			double TS2C[] = new double[s];
			double Cvalue[] = new double[s];
			for (int k = 0; k < s; k++) {
				// 生成一个候选测试用例
				// System.out.println("k"+k);
				TestCase ck = randomTC(random);
				boolean this_ck_has_E_flag = false;
				double min = Double.MAX_VALUE;
				for (int i = 0; i < tests.size(); i++) {
					if ((ck.p > (tests.get(i).p - radius) && (ck.p < (tests.get(i).p + radius)))) {
						if (min > Math.abs(ck.p - tests.get(i).p)) {
							min = Math.abs(ck.p - tests.get(i).p);
							TS2C[k] = min;
							Cvalue[k] = ck.p;
						}
						this_ck_has_E_flag = true;
					}
				}
				if (!this_ck_has_E_flag) {
					all_s_has_E_flag = false;
					p = new TestCase();
					p.p = ck.p;
					if (!isCorrect(p.p)) {
						// System.out.println(count+","+rrtcount+","+fscscount);
						return count;
					} else {
						count++;
						rrtcount++;
						tests.add(p);
					}
				}
			}
			if (all_s_has_E_flag) {
				double max = 0;
				int index = 0;
				for (int i = 0; i < TS2C.length; i++) {
					if (max < TS2C[i]) {
						max = TS2C[i];
						index = i;
					}
				}
				p = new TestCase();
				p.p = Cvalue[index];
				// System.out.println("p" + count + ":" + p.p + " fscs");
				if (!isCorrect(p.p)) {
					// System.out.println(count+","+rrtcount+","+fscscount);
					return count;
				} else {
					count++;
					fscscount++;
					tests.add(p);
				}
			}
			_10CandidateCount++;
		}
		// System.out.println(count+","+rrtcount+","+fscscount);
		return count;
	}
}
