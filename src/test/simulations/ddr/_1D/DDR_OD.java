package test.simulations.ddr._1D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

public class DDR_OD {
	public static void main(String[] args) {
		int times = 50;
		long sums = 0;
		int temp = 0;
		int s = 10;
		//////////////
		///////////////
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			DDR_OD rrt_od = new DDR_OD(0, 1, 0.75, s, 0.0001, i * 3);
			temp = rrt_od.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	double min;
	double max;
	double fail_start;
	double fail_rate;
	double R;
	int s;
	int randomseed;

	ArrayList<TestCase> tests = new ArrayList<>();

	public DDR_OD(double min, double max, double r, int s, double fail_rate, int randomseed) {
		super();
		this.min = min;
		this.max = max;
		R = r;
		this.s = s;
		this.fail_rate = fail_rate;
		this.randomseed = randomseed;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public TestCase randomTC(Random random) {
		TestCase temp = new TestCase();
		double temp_value = random.nextDouble() * (max - min) + min;
		temp.p = temp_value;
		return temp;
	}

	public int run() {
		Random random = new Random(randomseed);
		fail_start = random.nextDouble() * (1 - fail_rate);
		// System.out.println("fail_start:"+fail_start);
		int count = 0;
		TestCase p = new TestCase();
		double value = random.nextDouble() * (max - min) + min;
		p.p = value;
		while (isCorrect(p.p)) {
			count++;
			// System.out.println("pp:"+p.p);
			tests.add(p);
			double radius = R / (2 * tests.size());
			// 生成s个候选测试用例
			boolean all_s_has_E_flag = true;
			double TS2C[] = new double[s];
			double Cvalue[] = new double[s];
			p = new TestCase();
			for (int k = 0; k < s; k++) {
				// 如果在候选测试用例圈中的已执行测试用例
				// 生成一个候选测试用例
				TestCase ck = randomTC(random);

				boolean this_ck_has_E_flag = false;
				for (int i = 0; i < tests.size(); i++) {
					double min = Double.MAX_VALUE;
					if ((ck.p > (tests.get(i).p - radius) && (ck.p < (tests.get(i).p + radius)))) {
						if (min > Math.abs(ck.p - tests.get(i).p)) {
							min = Math.abs(ck.p - tests.get(i).p);
							TS2C[k] = min;
							Cvalue[k] = ck.p;
						}
						this_ck_has_E_flag = true;
					}
				}
				if (!this_ck_has_E_flag) {
					// 没有已执行的
					all_s_has_E_flag = false;
					// System.out.println("test case number "+tests.size()+":"+p.p+"
					// (直接生成，没有在圈中的已执行用例) ");
					p.p = ck.p;
					break;
				}
			}
			if (all_s_has_E_flag) {
				// 如果都是有已执行的用例，计算距离来判断
				double max = 0;
				int index = 0;
				for (int i = 0; i < TS2C.length; i++) {
					if (max < TS2C[i]) {
						max = TS2C[i];
						index = i;
					}
					// System.out.println(TS2C[i]);
				}
				// System.out.println("test case number "+tests.size()+":"+Cvalue[index]+"
				// (由距离生成的) ");

				p.p = Cvalue[index];
			}
		}
		return count;
	}
}
