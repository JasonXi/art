package test.simulations.ddr._1D;
/* (Simulation) This code resets the radius anytime the all the candidate have empty exclusion regions. 
That is when the radius becomes too small due to multi-dimentional inputs.
*/

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

public class DDR3_threshold {
	public static void main(String[] args) {
		int times = 5000;
		long sums = 0;
		int temp = 0;
		int s = 10;
		int t = 10;//
		//////////////
		///////////////
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			DDR3_threshold rrt_od = new DDR3_threshold(0, 1, 0.2, s, t, 0.01, i * 3);// 0.002 refers to failure rate
			temp = rrt_od.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	double min;
	double max;
	double fail_start;
	double fail_rate;
	double R;
	int s;
	int t;
	int randomseed;

	ArrayList<TestCase> tests = new ArrayList<>();

	public DDR3_threshold(double min, double max, double r, int s, int t, double fail_rate, int randomseed) {
		super();
		this.min = min;
		this.max = max;
		R = r;
		this.s = s;
		this.fail_rate = fail_rate;
		this.t = t;
		this.randomseed = randomseed;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public TestCase randomTC(Random random) {
		TestCase temp = new TestCase();
		double temp_value = random.nextDouble() * (max - min) + min;
		temp.p = temp_value;
		return temp;
	}

	public int run() {
		Random random = new Random(randomseed);
		fail_start = random.nextDouble() * (1 - fail_rate);
		int count = 0;// 记录测试用例数量
		int rrtcount = 0;
		int fscscount = 0;
		int _10CandidateCount = 0;// 每十个一次的数量
		int ttemp = 0;
		double radius;
		TestCase p = randomTC(random);// 第一个测试用例
		// System.out.println("p0:" + p.p);
		while (isCorrect(p.p)) {
			if (ttemp == t) {
				_10CandidateCount = 0;
				System.out.println("Candidate reset");
			}
			radius = R / (2 * (s + _10CandidateCount));
			ttemp = 0;
			// 生成s个候选测试用例,从s个候选测试用例中挑选符合要求的测试用例
			boolean all_s_has_E_flag = true;
			double TS2C[] = new double[s];
			double Cvalue[] = new double[s];
			for (int k = 0; k < s; k++) {
				// 生成一个候选测试用例
				// System.out.println("k"+k);
				TestCase ck = randomTC(random);
				boolean this_ck_has_E_flag = false;
				double min = Double.MAX_VALUE;
				for (int i = 0; i < tests.size(); i++) {
					if ((ck.p > (tests.get(i).p - radius) && (ck.p < (tests.get(i).p + radius)))) {
						if (min > Math.abs(ck.p - tests.get(i).p)) {
							min = Math.abs(ck.p - tests.get(i).p);
							TS2C[k] = min;
							Cvalue[k] = ck.p;
						}
						this_ck_has_E_flag = true;
					}
				}
				if (!this_ck_has_E_flag) {
					all_s_has_E_flag = false;
					p = new TestCase();
					p.p = ck.p;
					if (!isCorrect(p.p)) {
						System.out.println(count + "," + rrtcount + "," + fscscount);
						return count;
					} else {
						count++;
						ttemp++;
						// System.out.println("t="+t);
						rrtcount++;
						tests.add(p);
					}
				}
			}
			if (all_s_has_E_flag) {
				double max = 0;
				int index = 0;
				for (int i = 0; i < TS2C.length; i++) {
					if (max < TS2C[i]) {
						max = TS2C[i];
						index = i;
					}
				}
				p = new TestCase();
				p.p = Cvalue[index];
				// System.out.println("p" + count + ":" + p.p + " fscs");
				if (!isCorrect(p.p)) {
					System.out.println(count + "," + rrtcount + "," + fscscount);
					return count;
				} else {
					count++;
					fscscount++;
					tests.add(p);
				}
			}
			_10CandidateCount++;
		}
		System.out.println(count + "," + rrtcount + "," + fscscount);
		return count;
	}
}
