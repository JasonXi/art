package test.simulations.ddr._2D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

public class DDR_TD2 {
	public static void main(String[] args) {
		int times = 100;
		long sums = 0;
		int temp = 0;
		int s = 10;
		//////////////
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			double min[] = { 0.0, 0.0 };
			double max[] = { 1.0, 1.0 };
			DDR_TD2 rrt_od = new DDR_TD2(min, max, 0.75, s, 0.001, i * 3);// 0.002 refers to failure rate
			temp = rrt_od.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	double[] min;
	double[] max;
	double[] fail_start;
	double fail_rate;
	double R;
	int s;
	int randomseed;
	double fail_regionS;

	ArrayList<TestCase> tests = new ArrayList<>();

	public DDR_TD2(double[] min, double[] max, double r, int s, double fail_rate, int randomseed) {
		super();
		this.min = min;
		this.max = max;
		R = r;
		this.s = s;
		this.fail_rate = fail_rate;
		this.randomseed = randomseed;
		this.fail_start = new double[this.min.length];
		this.fail_regionS = fail_rate * (max[1] - min[1]) * (max[0] - min[0]);
	}

	public boolean isCorrect(TestCase p) {
		boolean isCorrect = true;
		if (p.p > fail_start[0] && p.p < fail_start[0] + Math.sqrt(fail_regionS)) {
			if (p.q > fail_start[1] && p.q < fail_start[1] + Math.sqrt(fail_regionS)) {
				isCorrect = false;
			}
		}
		return isCorrect;
	}

	public TestCase randomTC(Random random) {
		TestCase temp = new TestCase();
		double p = random.nextDouble() * (max[0] - min[0]) + min[0];
		double q = random.nextDouble() * (max[1] - min[1]) + min[1];
		temp.p = p;
		temp.q = q;
		return temp;
	}

	public int run() {
		Random random = new Random(randomseed);
		// 失效率的范围
		fail_start[0] = random.nextDouble() * (max[0] - min[0] - Math.sqrt(fail_regionS)) + min[0];
		fail_start[1] = random.nextDouble() * (max[1] - min[1] - Math.sqrt(fail_regionS)) + min[1];
		System.out.println("fail_rate:(" + fail_start[0] + "," + fail_start[1] + ")");
		int count = 0;// 记录测试用例数量
		int _10CandidateCount = s;// 每十个一次的数量
		TestCase p = randomTC(random);// 第一个测试用例
		System.out.println("p0:" + p.toString() + isCorrect(p));
		while (isCorrect(p)) {
			// double radius =Math.sqrt(a) R / (2 * _10CandidateCount);
			double radius = Math.sqrt(R * (max[1] - min[1]) * (max[0] - min[0]) / (Math.PI * (s + _10CandidateCount)));
			// 生成s个候选测试用例,从s个候选测试用例中挑选符合要求的测试用例
			p = new TestCase();
			boolean all_s_has_E_flag = true;
			double TS2C[] = new double[s];
			double Pvalue[] = new double[s];
			double Qvalue[] = new double[s];
			for (int k = 0; k < s; k++) {
				// 生成一个候选测试用例
				TestCase ck = randomTC(random);
				// System.out.println("ck"+k+":"+ck.toString()+isCorrect(ck));
				boolean this_ck_has_E_flag = false;
				double min = Double.MAX_VALUE;
				for (int i = 0; i < tests.size(); i++) {
					// 没有在圈之中
					// if((Math.pow((ck.p-tests.get(i).p)*(ck.p-tests.get(i).p)+(ck.q-tests.get(i).q)*(ck.q-tests.get(i).q),
					// 0.5))<radius){
					// if(Math.abs(p.p-tests.get(i).p)<radius&&Math.abs(p.q-tests.get(i).q)<radius){
					if ((Math.pow((ck.p - tests.get(i).p) * (ck.p - tests.get(i).p)
							+ (ck.q - tests.get(i).q) * (ck.q - tests.get(i).q), 0.5)) < radius) {
						if (min > Math.sqrt(Math.pow(ck.p - tests.get(i).p, 2) + Math.pow(ck.q - tests.get(i).q, 2))) {
							min = Math.sqrt(Math.pow(ck.p - tests.get(i).p, 2) + Math.pow(ck.q - tests.get(i).q, 2));
							TS2C[k] = min;
							Pvalue[k] = ck.p;
							Qvalue[k] = ck.q;
						}
						this_ck_has_E_flag = true;
					}

				}
				if (!this_ck_has_E_flag) {
					all_s_has_E_flag = false;
					p = new TestCase();
					p.p = ck.p;
					p.q = ck.q;
					System.out.println("p" + count + ":" + p.toString() + isCorrect(p) + " rrt");
					if (!isCorrect(p)) {
						return count;
					} else {
						count++;
						tests.add(p);
					}
				}
			}
			if (all_s_has_E_flag) {
				double max = 0;
				int index = 0;
				for (int i = 0; i < TS2C.length; i++) {
					if (max < TS2C[i]) {
						max = TS2C[i];
						index = i;
					}
				}
				p = new TestCase();
				p.p = Pvalue[index];
				p.q = Qvalue[index];
				System.out.println("p" + count + ":" + p.toString() + isCorrect(p) + " fscs");
				if (!isCorrect(p)) {
					return count;
				} else {
					count++;
					tests.add(p);
				}
			}
			_10CandidateCount++;
		}
		return count;
	}
}
