package test.simulations.ddr._3D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

public class DDR_ThreeD2 {
	public static void main(String[] args) {
		int times = 100;
		long sums = 0;
		int temp = 0;
		int s = 10;
		//////////////
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			double min[] = { 0.0, 0.0, 0.0 };
			double max[] = { 1.0, 1.0, 1.0 };
			DDR_ThreeD2 rrt_od = new DDR_ThreeD2(min, max, 0.75, s, 0.001, i * 3);
			temp = rrt_od.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	double[] min;
	double[] max;
	double[] fail_start;
	double fail_rate;
	double R;
	int s;
	int randomseed;
	double fail_regionS;
	double periodLength;

	ArrayList<TestCase> tests = new ArrayList<>();

	public DDR_ThreeD2(double[] min, double[] max, double r, int s, double fail_rate, int randomseed) {
		super();
		this.min = min;
		this.max = max;
		R = r;
		this.s = s;
		this.fail_rate = fail_rate;
		this.randomseed = randomseed;
		this.fail_start = new double[this.min.length];
		this.fail_regionS = fail_rate;
		for (int i = 0; i < this.min.length; i++) {
			this.fail_regionS *= (max[i] - min[i]);
		}
		periodLength = Math.pow(this.fail_regionS, 1.0 / (double) this.min.length);
	}

	public boolean isCorrect(TestCase p) {
		boolean isCorrect = true;
		double periodLength = Math.pow(fail_regionS, 1.0 / (double) this.min.length);
		if (p.p > fail_start[0] && p.p < fail_start[0] + periodLength) {
			if (p.q > fail_start[1] && p.q < fail_start[1] + periodLength) {
				if (p.m > fail_start[2] && p.m < fail_start[2] + periodLength) {
					isCorrect = false;
				}
			}
		}
		return isCorrect;
	}

	public TestCase randomTC(Random random) {
		TestCase temp = new TestCase();
		double p = random.nextDouble() * (max[0] - min[0]) + min[0];
		double q = random.nextDouble() * (max[1] - min[1]) + min[1];
		double m = random.nextDouble() * (max[2] - min[2]) + min[2];
		temp.p = p;
		temp.q = q;
		temp.m = m;
		return temp;
	}

	public int run() {
		Random random = new Random(randomseed);
		for (int i = 0; i < this.min.length; i++) {
			fail_start[i] = random.nextDouble() * (max[i] - min[i] - periodLength) + min[i];
			System.out.print(fail_start[i] + " ");
		}
		System.out.println();
		int count = 0;
		int _10CandidateCount = 0;
		TestCase p = randomTC(random);// randomTC
		System.out.println("p0:" + p.toString() + isCorrect(p));
		while (isCorrect(p)) {
			double InputDomainA = 1.0;
			for (int i = 0; i < this.min.length; i++) {
				InputDomainA *= max[i] - min[i];
			}
			double radius = Math.pow((0.75 * (InputDomainA * R)) / ((_10CandidateCount + s) * (Math.PI)), 1.0 / 3.0);
			System.out.println("radius:" + radius);
			p = new TestCase();
			boolean all_s_has_E_flag = true;
			double TS2C[] = new double[s];
			double Pvalue[] = new double[s];
			double Qvalue[] = new double[s];
			double Mvalue[] = new double[s];

			for (int k = 0; k < s; k++) {
				TestCase ck = randomTC(random);
				boolean this_ck_has_E_flag = false;
				double min = Double.MAX_VALUE;
				for (int i = 0; i < tests.size(); i++) {
					// 没有在圈之中
					double distance = Math.pow((Math.pow((ck.p - tests.get(i).p), 2.0))
							+ (Math.pow(ck.q - tests.get(i).q, 2.0)) + (Math.pow(ck.m - tests.get(i).m, 2.0)), 0.5);// distance
					if (distance < radius) {
						if (min > distance) {
							min = distance;
							TS2C[k] = min;
							Pvalue[k] = ck.p;
							Qvalue[k] = ck.q;
							Mvalue[k] = ck.m;
						}
						this_ck_has_E_flag = true;
					}
				}
				if (!this_ck_has_E_flag) {
					all_s_has_E_flag = false;
					p = new TestCase();
					p.p = ck.p;
					p.q = ck.q;
					p.m = ck.m;
					System.out.println("p" + count + ":" + p.toString() + isCorrect(p) + " rrt");
					if (!isCorrect(p)) {
						return count;
					} else {
						count++;
						tests.add(p);
					}
				}
			}
			if (all_s_has_E_flag) {
				double max = 0;
				int index = 0;
				for (int i = 0; i < TS2C.length; i++) {
					if (max < TS2C[i]) {
						max = TS2C[i];
						index = i;
					}
				}
				p = new TestCase();
				p.p = Pvalue[index];
				p.q = Qvalue[index];
				p.m = Mvalue[index];
				System.out.println("p" + count + ":" + p.toString() + isCorrect(p) + " fscs");
				if (!isCorrect(p)) {
					return count;
				} else {
					count++;
					tests.add(p);
				}
			}
			_10CandidateCount++;
		}
		return count;
	}
}
