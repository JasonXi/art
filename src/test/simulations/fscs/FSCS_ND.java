package test.simulations.fscs;
/*
 * n维实现,包含1维2维等
 * */

import java.util.ArrayList;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.failurepattern.FailurePattern;
import test.ART;

public class FSCS_ND extends ART {
	public static void main(String[] args) {
		//testFm();
		//testTCTime(2,5000);
		//testEm(1, 0.005);
	}
	private int s = 10;// 表示候选集的数量初始值为10
	private ArrayList<NPoint> tests = new ArrayList<>();

	public FSCS_ND(double[] min, double[] max, int s, FailurePattern pattern, Random random) {
		super(min, max, random, pattern);
		this.s = s;
	}


	
	@Override
	public NPoint generateNextTC() {
		NPoint p = null;
		if (tests.size() == 0) {
			p = randomCreator.randomPoint();
			tests.add(p);
		} else {
			p = new NPoint();
			double maxDistance = -1.0;
			NPoint bestCandidate = null;
			for (int i = 0; i < s; i++) {
				NPoint candidate = randomCreator.randomPoint();
				// 计算两个点的距离
				double minDistance = Double.MAX_VALUE;

				for (int j = 0; j < this.tests.size(); j++) {
					double tempDistance = candidate.distanceToAnotherPoint(tests.get(j));
					if (tempDistance < minDistance) {
						minDistance = tempDistance;
					}
				}
				if (maxDistance < minDistance) {
					maxDistance = minDistance;
					bestCandidate = candidate;
				}
			}
			p = bestCandidate;
			tests.add(p);
		}
		return p;
	}

	
	/*public void time() {
		int count = 0;
		NPoint p = randomCreator.randomPoint();
		while (count <= tcCount) {
			count++;
			tests.add(p);
			p = new NPoint();
			double maxDistance = -1.0;
			NPoint bestCandidate = null;
			for (int i = 0; i < s; i++) {
				NPoint candidate =randomCreator.randomPoint();
				// 计算两个点的距离
				double minDistance = Double.MAX_VALUE;

				for (int j = 0; j < this.tests.size(); j++) {
					double tempDistance = calTwoPointDistance(candidate, tests.get(j));
					if (tempDistance < minDistance) {
						minDistance = tempDistance;
					}
				}
				if (maxDistance < minDistance) {
					maxDistance = minDistance;
					bestCandidate = candidate;
				}
			}
			p = null;
			p = bestCandidate;
		}
	}
	public static double testFm() {
		int d = 2;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int s=10;
		
		int temp = 0;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.005;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			FSCS_ND rt = new FSCS_ND(min, max, s, failurePattern, new Random(i * 3));
			temp = rt.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		double fm = sums / (double) times;
		System.out.println("fm:" + fm + " time:" + ((endTime - startTime) / (double) times));
		return fm;
	}
	
	
	public static double[] testEm(int dimension, double failrate) {
		int d = dimension;
		int emTime = 6;
		double result[] = new double[emTime];
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;
		int kk = 10;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = failrate;
		for (int k = 0; k < emTime; k++) {
			long sums = 0;
			long startTime = System.currentTimeMillis();
			for (int i = 0; i < times; i++) {
				FSCS_ND rt = new FSCS_ND(min, max, kk, failurePattern, new Random(i * 3));
				rt.emCount = (k + 1) * 500;
				temp = rt.em();
				sums += temp;
			}
			long endTime = System.currentTimeMillis();
			double em = sums / (double) times;
			result[k] = em;
			System.out.println("em:" + em + " time:" + ((endTime - startTime) / (double) times));
		}
		System.out.println();
		return result;
	}
	public static double testTCTime(int d, int tcCount) {
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 1;

		int s = 10;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			FSCS_ND rt = new FSCS_ND(min, max, s, failurePattern, new Random(i * 3));
			rt.tcCount = tcCount;
			rt.time2();
		}
		long endTime = System.currentTimeMillis();
		System.out.println((endTime - startTime) / (double) times);
		return ((endTime - startTime) / (double) times);
	}*/

}
