package test.simulations.bi_t;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import datastructure.failurepattern.impl.RealityFailPattern;
import test.ART;
import tested.*;
import util.PaiLie;
import util.data.ZeroOneCreator;

public class Bi_Test3 extends ART {

	public Bi_Test3(double[] min, double[] max, Random random, FailurePattern failurePattern) {
		super(min, max, random, failurePattern);
	}

	public ArrayList<NRectRegion> regions = new ArrayList<>();

	@Override
	public int run() {
		int count = 0;
		// NPoint p = randomCreator.randomPoint();
		count++;
		NRectRegion region = new NRectRegion(new NPoint(min), new NPoint(max));
		NPoint p = midPoint(region);

		try {
			addRegionsInND(region, p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//
		while (true) {
			ArrayList<NRectRegion> backups = new ArrayList<>();
			while (this.regions.size() != 0) {
				// 产生方式
				int index = random.nextInt(this.regions.size());
				// 测试
				NRectRegion tempRegion = this.regions.remove(index);
				backups.add(tempRegion);

				count++;
				boolean flag = this.failPattern.isCorrect(midPoint(tempRegion));
				if (!flag) {
					return count;
				}
			}
			// 分割
			for (int i = 0; i < backups.size(); i++) {
				try {
					addRegionsInND(backups.get(i), midPoint(backups.get(i)));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		// return 0;
	}

	//
	public NPoint midPoint(NRectRegion region) {
		double[] pn = region.getStart().getXn();
		double[] t2n = region.getEnd().getXn();
		double[] mid = new double[pn.length];
		for (int i = 0; i < mid.length; i++) {
			mid[i] = (pn[i] + t2n[i]) / 2.0;
		}
		return new NPoint(mid);
	}

	@Override
	public int em() {
		return 0;
	}

	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}

	public void addRegionsInND(NRectRegion region, NPoint p) throws Exception {
		// int regions=(int) Math.pow(2, this.dimension);
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		// System.out.println(result1.size());
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}

			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			this.regions.add(tempRegion);
		}
	}

	public static void main(String[] args) {
		testReality();
	}

	public static void testSimulation() {
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double[] min = dataCreator.minCreator(2);
		double[] max = dataCreator.maxCreator(2);

		int times = 200;
		FailurePattern pattern = new BlockPattern();
		pattern.fail_rate = 0.01;

		long fm = 0;

		for (int i = 0; i < times; i++) {
			Bi_Test3 test = new Bi_Test3(min, max, new Random(i * 3), pattern);
			int temp = test.run();
			fm += temp;
		}

		System.out.println(fm / (double) times);
	}

	public static void testReality() {
		tanh tested = new tanh();
		// ZeroOneCreator dataCreator=new ZeroOneCreator();
		double[] min = tested.min;
		double[] max = tested.max;

		int times = 1000;
		FailurePattern pattern = new RealityFailPattern(tested.getClass().getSimpleName());
		// pattern.fail_rate=0.01;

		long fm = 0;

		for (int i = 0; i < times; i++) {
			Bi_Test3 test = new Bi_Test3(min, max, new Random(i * 3), pattern);
			int temp = test.run();
			fm += temp;
		}

		System.out.println(fm / (double) times);
	}

	@Override
	public NPoint generateNextTC() {
		// TODO Auto-generated method stub
		return null;
	}
}
