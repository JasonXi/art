package test.simulations.bi_t;

import java.util.Random;

public class Bi_Test {
	public static void main(String[] args) {
		int cishu = 3000;
		long sumOfF = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < cishu; i++) {
			Bi_Test bi_t = new Bi_Test((i * 3));

			int f_measure = bi_t.run();
			sumOfF += f_measure;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sumOfF / (double) cishu);
		System.out.println("Time: " + (endTime - startTime) / (double) cishu);
	}
	double fail_start;
	double fail_rate = 0.0005;

	int randomseed;
	// ArrayList<Double> al = new ArrayList<>();

	public Bi_Test(int seed) {
		randomseed = seed;
	}

	public int run() {
		Random random = new Random(randomseed);
		int count = 0;
		fail_start = random.nextDouble() * (1 - fail_rate);
		double p = 0.5;
		int i = 1, m = 0;
		while (test(p)) {
			count++;
			m = (int) (count + 1 - Math.pow(2, i));
			p = (Math.pow(2, -(i + 1))) * (2 * m + 1);
			if (2 * m + 1 == (Math.pow(2, i + 1)) - 1) {
				i++;
			}

		}
		return count;
	}

	public boolean test(double p) {
		boolean flag;
		if (p > fail_start && p < (fail_start + fail_rate)) {
			flag = false;
		} else {
			flag = true;
		}
		return flag;
	}
}
