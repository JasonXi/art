package test.simulations.bi_t;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

/*
 * random select test cases;
 * 
 */
public class Bi_Test2 {
	public static void main(String[] args) {
		int cishu = 1;
		long sumOfF = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < cishu; i++) {
			Bi_Test2 bi_t = new Bi_Test2((i * 4));

			int f_measure = bi_t.run();
			sumOfF += f_measure;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sumOfF / (double) cishu);
		System.out.println("Time: " + (endTime - startTime) / (double) cishu);
	}
	double fail_start;
	double fail_rate = 0.01;

	int randomseed;
	// ArrayList<Double> al = new ArrayList<>();

	public Bi_Test2(int seed) {
		randomseed = seed;
	}

	public int run() {
		Random random = new Random(randomseed);
		int count = 0;
		fail_start = random.nextDouble() * (1 - fail_rate);
		double p = 0.5;
		int i = 1, m = 0;
		boolean flag = true;
		ArrayList<TestCase> tests = new ArrayList<>();
		while (flag) {
			System.out.println(p);
			count++;
			m = (int) (count + 1 - Math.pow(2, i));
			TestCase temp1=new TestCase();
			temp1.p=(Math.pow(2, -(i + 1))) * (2 * m + 1);
			tests.add(temp1);
			if (2 * m + 1 == (Math.pow(2, i + 1)) - 1) {
				// 下一轮
				i++;
				// 当前轮测试，随机挑选测试用例
				ArrayList<TestCase> temp = new ArrayList<>(tests);
				for (int l = 0; l < tests.size(); l++) {

					int index = random.nextInt(temp.size());
					System.out.println("index:"+index);
					if (!test(temp.get(index).p)) {
						flag = false;
					}
					temp.remove(index);
				}
			}

		}
		return count;
	}

	public boolean test(double p) {
		boolean flag;
		if (p > fail_start && p < (fail_start + fail_rate)) {
			flag = false;
		} else {
			flag = true;
		}
		return flag;
	}
}
