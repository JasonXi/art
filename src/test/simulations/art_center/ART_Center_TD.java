package test.simulations.art_center;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;
import datastructure.TD._2DPoint;
import datastructure.TD._2DRegion;
import util.CRandomNumber;

/*
 * 在最大的划分区间（RP）中间生成测试用例
 * */
@Deprecated
public class ART_Center_TD {
	public static void main(String[] args) {
		int times = 3000;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_Center_TD art_RP_TD = new ART_Center_TD(i * 3, new _2DRegion(new _2DPoint(0, 0), new _2DPoint(1, 1)), 0.01);
			int fm = art_RP_TD.run();
			sums += fm;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	long seed;
	public _2DRegion initRegion;
	public double xfail_start;
	public double yfail_start;
	public double fail_rate;
	ArrayList<TestCase> tests = new ArrayList<>();

	ArrayList<_2DRegion> regions = new ArrayList<>();

	public ART_Center_TD(long seed, _2DRegion initRegion, double fail_rate) {
		this.seed = seed;
		this.initRegion = initRegion;
		this.fail_rate = fail_rate;
		this.seed = seed;
		CRandomNumber.initSeed(seed);
	}

	public boolean isCorrect(TestCase p) {
		double x = p.p;
		double y = p.q;
		double bianchang = Math.pow(fail_rate, 0.5);
		if ((x > xfail_start && x < xfail_start + bianchang) && (y > yfail_start && y < yfail_start + bianchang)) {
			return false;
		} else {
			return true;
		}
	}

	public int run() {
		Random random = new Random(seed);
		int count = 0;
		xfail_start = random.nextDouble() * (1.0 - Math.sqrt(fail_rate));
		yfail_start = random.nextDouble() * (1.0 - Math.sqrt(fail_rate));
		_2DRegion region = initRegion;
		TestCase p = new TestCase();
		p.p = random.nextDouble() * (region.max.x - region.min.x) + region.min.x;
		p.q = random.nextDouble() * (region.max.y - region.min.y) + region.min.y;
		tests.add(p);
		//System.out.println(p);
		regions.add(initRegion);
		while (isCorrect(p)) {
			count++;
			// 找出最大区域
			double maxsize = 0;
			_2DRegion maxregion = null;
			int maxregion_index = 0;
			for (int i = 0; i < regions.size(); i++) {
				_2DRegion temp = regions.get(i);
				if (temp.size() > maxsize) {
					maxsize = temp.size();
					maxregion = temp;
					maxregion_index = i;
				}
			}
			//
			regions.remove(maxregion_index);
			// generate next one test case
			double xmin = maxregion.min.x;
			double xmax = maxregion.max.x;
			double ymin = maxregion.min.y;
			double ymax = maxregion.max.y;
			// System.out.println(xmin + "," + xmax + " " + ymin + "," + ymax);
			p = new TestCase();
			double eachtemplength = 1.0 / 8.0;
			double fenshu1 = 2;
			double fenshu2 = 6;
			double newstart = xmin + eachtemplength * fenshu1 * (xmax - xmin);
			double newend = xmin + eachtemplength * fenshu2 * (xmax - xmin);
			p.p = random.nextDouble() * (newend - newstart) + newstart;
			double newstart1 = newstart = ymin + eachtemplength * fenshu1 * (ymax - ymin);
			double newend1 = ymin + eachtemplength * fenshu2 * (ymax - ymin);
			p.q = random.nextDouble() * (newend1 - newstart1) + newstart1;
			// 添加fscs
			// int s = 3;
			// double TS2C[] = new double[s];
			// for (int i = 0; i < TS2C.length; i++) {
			// TS2C[i] = -1;
			// }
			// double Pvalue[] = new double[s];
			// double Qvalue[] = new double[s];
			// double maxMin = 0;
			// int index = 0;
			// for (int k = 0; k < 3; k++) {
			// // 生成一个候选测试用例
			// TestCase ck = new TestCase();
			// ck.p = random.nextDouble() * (newend - newstart) + newstart;
			// ck.q = random.nextDouble() * (newend1 - newstart1) + newstart1;
			// // System.out.println("first:" + ck.p + " Second" + ck.q);
			// double min = Double.MAX_VALUE;
			// for (int i = 0; i < tests.size(); i++) {
			// double distance = Math
			// .sqrt(Math.pow(ck.p - tests.get(i).p, 2) + Math.pow(ck.q - tests.get(i).q,
			// 2));
			// if (min > distance) {
			// min = distance;
			// TS2C[k] = min;
			// Pvalue[k] = ck.p;
			// Qvalue[k] = ck.q;
			// }
			//
			// }
			// if (maxMin < TS2C[k]) {
			// maxMin = TS2C[k];
			// index = k;
			// }
			// }
			// p.p = Pvalue[index];
			// p.q = Qvalue[index];
			// p.p = random.nextDouble() * (xmax - xmin) + xmin;
			// p.q = random.nextDouble() * (ymax - ymin) + ymin;
			tests.add(p);
			//System.out.println(p);
			// System.out.println(isCorrect(p));
			// add four regions
			_2DRegion first = new _2DRegion(new _2DPoint(xmin, ymin), new _2DPoint(p.p, p.q));
			_2DRegion second = new _2DRegion(new _2DPoint(p.p, ymin), new _2DPoint(xmax, p.q));
			_2DRegion third = new _2DRegion(new _2DPoint(p.p, p.q), new _2DPoint(xmax, ymax));
			_2DRegion fourth = new _2DRegion(new _2DPoint(xmin, p.q), new _2DPoint(p.p, ymax));
			regions.add(first);
			regions.add(second);
			regions.add(third);
			regions.add(fourth);
		}
		return count;
	}
}
