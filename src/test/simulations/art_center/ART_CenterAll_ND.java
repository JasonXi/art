package test.simulations.art_center;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import util.PaiLie;
import util.data.ZeroOneCreator;

public class ART_CenterAll_ND extends ART {

	// public int allParts = 1000;
	// public int startParts = 425;
	// public int endParts = 575;

	public double howManyParts = 0.220;

	public ArrayList<NPoint> tests = new ArrayList<>();
	public ArrayList<NRectRegion> regions = new ArrayList<>();

	public ART_CenterAll_ND(double[] min, double[] max, Random random, FailurePattern failurePattern) {
		super(min, max, random, failurePattern);
	}

	@Override
	public int run() {
		int count = 0;
		NPoint p = randomCreator.randomPoint();
		NRectRegion maxRegion = new NRectRegion(new NPoint(min), new NPoint(max));
		this.regions.add(maxRegion);
		int index = 0;

		while (this.failPattern.isCorrect(p)) {
			// count++;
			count++;

			// add tests
			this.tests.add(p);

			// split region
			splitRegion(index, p);

			// //make the region min
			// NRectRegion
			// smallerRegion=makeMaxRegionSmall(this.regions.get(index));
			// another point
			// p=new NPoint();
			ArrayList result = randomPoint();
			p = (NPoint) result.get(0);
			index = (int) result.get(1);
		}
		return count;
	}

	public ArrayList randomPoint() {
		// 将p填入
		// NPoint p = new NPoint();
		double Size = 0.0;
		for (int i = 0; i < this.regions.size(); i++) {
			Size += this.regions.get(i).size();
		}
		double T = random.nextDouble() * Size;
		double SumIntegral = 0.0;// 积分值总和
		double PreIntegral = 0.0;
		int temp = 0;
		for (int i = 0; i < this.regions.size(); i++) {
			if (SumIntegral < T) {
				PreIntegral = SumIntegral;
				temp = i;
			}
			SumIntegral += this.regions.get(i).size();
		}
		// 在temp处生成下一个随机点
		NPoint p = randomCreator.randomPoint(makeMaxRegionSmall(this.regions.get(temp)));
		// NRectRegion tempRegion=region[temp];
		// return randomPoint(tempRegion);
		ArrayList list = new ArrayList<>();
		list.add(p);
		list.add(temp);
		return list;
	}

	// 只生成测试用例的方法
	public NPoint generateTC() {
		NPoint p = null;

		if (this.tests.size() == 0) {
			p = randomCreator.randomPoint();
			this.regions.add(new NRectRegion(new NPoint(min), new NPoint(max)));
			splitRegion(0, p);
		} else {
			ArrayList list = randomPoint();
			p = (NPoint) list.get(0);
			int index = (int) list.get(1);
			splitRegion(index, p);
		}
		this.tests.add(p);

		return p;
	}

	public NRectRegion makeMaxRegionSmall(NRectRegion maxregion) {
		double[] start = maxregion.getStart().getXn();
		double[] end = maxregion.getEnd().getXn();
		double[] s1 = new double[start.length];
		double[] e1 = new double[end.length];

		// double eachtemplength = 1.0 / (double)allParts;
		for (int i = 0; i < start.length; i++) {
			double length = (end[i] - start[i]);

			s1[i] = 0.5 * (end[i] + start[i]) - howManyParts * length * 0.5;
			e1[i] = 0.5 * (end[i] + start[i]) + howManyParts * length * 0.5;

		}

		return new NRectRegion(new NPoint(s1), new NPoint(e1));

	}

	public void splitRegion(int index, NPoint p) {
		if (index < 0 || index >= this.regions.size()) {
			System.out.println("split region error! index not correct!");
			return;
		}
		// first remove it
		NRectRegion region = this.regions.remove(index);
		try {
			// add regions;
			addRegionsInND(region, p);
		} catch (Exception e) {
			System.out.println("split region error in split region rec");
		}
	}

	public void addRegionsInND(NRectRegion region, NPoint p) throws Exception {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		// System.out.println(result1.size());
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}

			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			this.regions.add(tempRegion);
		}
	}

	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}

	public int maxRegion() {
		int index = 0;
		double maxRegionSize = 0.0;
		for (int i = 0; i < this.regions.size(); i++) {
			double tempRegionSize = this.regions.get(i).size();
			if (tempRegionSize > maxRegionSize) {
				maxRegionSize = tempRegionSize;
				index = i;
			}
		}
		return index;
	}

	public static void main(String[] args) {

		double howmany = 0.50;

		int fm = 0;
		int times = 2000;

		int d = 2;
		double failure_rate = 0.001;

		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double[] min = dataCreator.minCreator(d);
		double[] max = dataCreator.maxCreator(d);

		for (int i = 0; i < times; i++) {
			FailurePattern pattern = new BlockPattern();
			pattern.fail_rate = failure_rate;
			ART_CenterAll_ND center = new ART_CenterAll_ND(min, max, new Random(i * 3), pattern);
			center.howManyParts = howmany;
			int temp = center.run();
			fm += temp;
		}
		System.out.println(fm / (double) times);
	}

	@Override
	public int em() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NPoint generateNextTC() {
		// TODO Auto-generated method stub
		return null;
	}
}
