package test.simulations.rrt;

import java.util.ArrayList;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import util.data.ZeroOneCreator;

/*
 * n维实现，包含了1,2,3,4维，以及其他维度
 * 根据长度不同来定排除范围的比例
 * */
public class RRT_ND2 extends ART {
	public static void main(String[] args) {

		int d = 2;
		double R=1.0;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		// bessj beTested = new bessj();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);
		// double[] theta = { 0.005 };

		int times = 2000;
		long sums = 0;
		int temp = 0;

		

		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			FailurePattern failurePattern = new BlockPattern();
			failurePattern.fail_rate = 0.01;
			RRT_ND2 nd = new RRT_ND2(min, max, failurePattern, new Random(i * 5), R);
			temp = nd.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		double fm = sums / (double) times;
		// System.out.println(fm);
		// System.out.println(
		// timewaste + "," + timewaste1 + "," + timewaste2 + " " +
		// (timewaste + timewaste1 + timewaste2));

		System.out.println(" Fm:" + fm + " time:" + ((endTime - startTime) / (double) times));

	}
	public double R;

	public ArrayList<NPoint> tests = new ArrayList<>();

	public RRT_ND2(double[] min, double[] max, FailurePattern pattern, Random random, double r) {
		super(min, max, random, pattern);
		this.R = r;
	}

	public double calculateRadius(int count) {
		if (this.dimension % 2 == 0) {
			int k = this.dimension / 2;
			double kjie = 1;
			for (int i = k; i > 0; i--) {
				kjie *= i;
			}
			double temp = (this.R * totalArea * kjie) / (count * Math.pow(Math.PI, k));

			return Math.pow(temp, 1 / (double) this.dimension);
		} else {
			int k = this.dimension / 2;
			double kjie = 1;
			double k2jie = 1;
			for (int i = k; i > 0; i--) {
				kjie *= i;
			}
			for (int i = (2 * k + 1); i > 0; i--) {
				k2jie *= i;
			}
			double temp = (this.R * totalArea * k2jie) / (kjie * Math.pow(2, 2 * k + 1) * Math.pow(Math.PI, k) * count);
			// System.out.println("return R");
			return Math.pow(temp, 1 / (double) this.dimension);
		}
	}

	public NPoint randomTC() {
		NPoint point = new NPoint();
		point.dimension = this.dimension;
		double[] xn = new double[this.dimension];
		for (int i = 0; i < xn.length; i++) {
			xn[i] = random.nextDouble() * (max[i] - min[i]) + min[i];
		}
		point.setXn(xn);
		return point;
	}

	
	@Override
	public int run() {
		int count = 0;
		NPoint p = randomTC();
		// while (this.failPattern.isCorrect(p)) {
		while (this.failPattern.isCorrect(p)) {
			// StdDraw.filledCircle(p.getXn()[0], p.getXn()[1], 0.01);
			count++;
			tests.add(p);

			double eachExclusionArea=(this.totalArea*R)/(double)tests.size();
			//double radius = calculateRadius(tests.size());
			//System.out.println(radius);
			double minValue=Math.pow((eachExclusionArea/this.totalArea),1.0/(double)this.dimension);
			boolean flag = true;
			while (flag) {
				flag = false;
				p = randomTC();
				for (int i = 0; i < tests.size(); i++) {
					// 排除区域是圆
					// 计算距离
					/*double[] tested = tests.get(i).getXn();
					double distance = 0;
					double[] untested = p.getXn();
					for (int j = 0; j < this.dimension; j++) {
						distance += Math.pow((tests.get(i).getXn()[j] - untested[j]), 2);
					}
					distance = Math.sqrt(distance);
					if (distance < radius) {
						flag = true;
						break;
					}*/
					
					  //排除区域是正方形 if(Math.abs(p.p-tests.get(i).p)<radius){
					boolean isInArea=true;
					for(int j=0;j<p.getXn().length;j++){
						double testxn[]=tests.get(i).getXn();
						double[] pxn=p.getXn();
						 double length=(this.max[j]-this.min[j]);
						if((pxn[j]>testxn[j]+0.5*minValue*length)||(pxn[j]<testxn[j]-0.5*minValue*length)){
							isInArea=false;
						}
					}
					 if(isInArea){
						 flag=true;
					 }
				}

			}

		}
		return count;
	}

	@Override
	public int em() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NPoint generateNextTC() {
		// TODO Auto-generated method stub
		return null;
	}
}
