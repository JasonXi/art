package test.simulations.rrt;

import java.util.ArrayList;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import datastructure.failurepattern.impl.PointPatternIn1D;
import test.ART;
import test.simulations.art_b.ART_B_ND;
import test.simulations.rt.RT_ND;
import util.data.ZeroOneCreator;

/*
 * n维实现，包含了1,2,3,4维，以及其他维度
 * */
public class RRT_ND extends ART {
	public static void main(String[] args) {

		// rt.pmCount=69314;
		// System.out.println(rt.pm());
		testEm(2,0.01);
		//testTCTime(2, 5000);
		 //System.out.println(testPm(2, 0.01));
		//testFm();
	}

	public double R;
	public ArrayList<NPoint> tests = new ArrayList<>();

	public RRT_ND(double[] min, double[] max, FailurePattern pattern, Random random, double r) {
		super(min, max, random, pattern);
		this.R = r;
	}

	public double calculateRadius(int count) {
		if (this.dimension % 2 == 0) {
			int k = this.dimension / 2;
			double kjie = 1;
			for (int i = k; i > 0; i--) {
				kjie *= i;
			}
			double temp = (this.R * totalArea * kjie) / (count * Math.pow(Math.PI, k));

			return Math.pow(temp, 1 / (double) this.dimension);
		} else {
			int k = this.dimension / 2;
			double kjie = 1;
			double k2jie = 1;
			for (int i = k; i > 0; i--) {
				kjie *= i;
			}
			for (int i = (2 * k + 1); i > 0; i--) {
				k2jie *= i;
			}
			double temp = (this.R * totalArea * k2jie) / (kjie * Math.pow(2, 2 * k + 1) * Math.pow(Math.PI, k) * count);
			// System.out.println("return R");
			return Math.pow(temp, 1 / (double) this.dimension);
		}
	}

	public NPoint generateNextTC() {
		NPoint p = null;
		if (tests.size() == 0) {
			p = randomCreator.randomPoint();
			tests.add(p);
			// return p;
		} else {
			double radius = calculateRadius(tests.size());
			boolean flag = true;
			while (flag) {
				flag = false;
				p = randomCreator.randomPoint();
				for (int i = 0; i < tests.size(); i++) {
					// 排除区域是圆
					// 计算距离
					double[] tested = tests.get(i).getXn();
					double distance = 0;
					double[] untested = p.getXn();
					for (int j = 0; j < this.dimension; j++) {
						distance += Math.pow((tests.get(i).getXn()[j] - untested[j]), 2);
					}
					distance = Math.sqrt(distance);
					if (distance < radius) {
						flag = true;
						break;
					}
					/*
					 * //排除区域是正方形 if(Math.abs(p.p-tests.get(i).p)<radius){
					 * if(Math.abs(p.q-tests.get(i).q)<radius){ flag=true; } }
					 */
				}
			}
			tests.add(p);
		}
		return p;
	}

	

	

	public void time() {
		int count = 0;
		NPoint p = randomCreator.randomPoint();
		while (count < tcCount) {
			count++;
			tests.add(p);
			double radius = calculateRadius(tests.size());
			boolean flag = true;
			while (flag) {
				flag = false;
				p = randomCreator.randomPoint();
				for (int i = 0; i < tests.size(); i++) {
					// 排除区域是圆
					// 计算距离
					double[] tested = tests.get(i).getXn();
					double distance = 0;
					double[] untested = p.getXn();
					for (int j = 0; j < this.dimension; j++) {
						distance += Math.pow((tests.get(i).getXn()[j] - untested[j]), 2);
					}
					distance = Math.sqrt(distance);
					if (distance < radius) {
						flag = true;
						// break;
					}
					/*
					 * //排除区域是正方形 if(Math.abs(p.p-tests.get(i).p)<radius){
					 * if(Math.abs(p.q-tests.get(i).q)<radius){ flag=true; } }
					 */
				}
			}
		}
	}

	
	

	public static double testFm() {
		int d = 2;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		double R = 0;
		if (d == 1) {
			R = 0.75;
		} else {
			R = 1.5;
		}

		int temp = 0;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.005;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			RRT_ND rt = new RRT_ND(min, max, failurePattern, new Random(i * 5), R);
			temp = rt.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		double fm = sums / (double) times;
		System.out.println("fm:" + fm + " time:" + ((endTime - startTime) / (double) times));
		return fm;
	}

	public static double[] testEm(int dimension, double failrate) {
		int d = dimension;
		int emTime = 6;
		double[] result = new double[emTime];
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;
		double R = 0;
		if (d == 1) {
			R = 0.75;
		} else {
			R = 1.5;
		}
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = failrate;
		for (int k = 0; k < emTime; k++) {
			long sums = 0;
			long startTime = System.currentTimeMillis();
			for (int i = 0; i < times; i++) {
				RRT_ND nd = new RRT_ND(min, max, failurePattern, new Random(i * 5), R);
				nd.emCount = (k + 1) * 500;
				temp = nd.em();
				sums += temp;
			}
			long endTime = System.currentTimeMillis();
			double em = sums / (double) times;
			result[k] = em;
			System.out.println("em:" + em + " time:" + ((endTime - startTime) / (double) times));
		}
		System.out.println();
		return result;
	}

	public static double testTCTime(int d, int tcCount) {
		double R = d == 1 ? 0.75 : 1.5;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 1;

		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.0001;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			RRT_ND nd = new RRT_ND(min, max, failurePattern, new Random(i * 5), R);
			nd.tcCount = tcCount;
			nd.time2();
		}
		long endTime = System.currentTimeMillis();
		System.out.println((endTime - startTime) / (double) times);
		return ((endTime - startTime) / (double) times);
	}

	public static double testPm(int d, double failrate) {
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double[] min = dataCreator.minCreator(d);
		double[] max = dataCreator.maxCreator(d);
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = failrate;
		double R = 0.75;
		if (d > 1) {
			R = 1.5;
		}
		int n = 1000;
		int pm = 0;
		for (int i = 0; i < n; i++) {
			RRT_ND nd = new RRT_ND(min, max, failurePattern, new Random(i * 5), R);
			if (nd.pm()) {
				pm++;
			}
		}
		return pm / (double) n;
	}

}
