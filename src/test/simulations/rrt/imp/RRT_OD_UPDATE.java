package test.simulations.rrt.imp;

import java.util.ArrayList;
import java.util.Random;

class BeginEnd {
	public double start;
	public double end;
	public BeginEnd next;

	public BeginEnd(double start, double end) {
		this.start = start;
		this.end = end;
	}
}

class Node {
	public double value;
	public Node next;

	public Node(double value) {
		this.value = value;
	}
}

/**
 * 此算法用来改善RRT的时间问题
 */
public class RRT_OD_UPDATE {
	public static void main(String[] args) {

		// for (int i = 0; i < 3; i++) {
		// double a=Math.random();
		// System.out.println(a);
		// rrt_OD_UPDATE.addNewNode(a);
		// BeginEnd beroot=null;
		// System.out.println(rrt_OD_UPDATE.getDatumAndBE(beroot));
		// }
		int times = 8000;
		long sums = 0;
		//////////////
		///////////////
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			RRT_OD_UPDATE rrt_OD_UPDATE = new RRT_OD_UPDATE(0, 1, 0.75, 0.0005, i * 3);
			int fm = rrt_OD_UPDATE.run();
			sums += fm;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime));
	}
	double min;
	double max;
	double fail_start;
	double fail_rate;
	double R;
	int randomseed;
	public Node root;

	public int size = 0;

	// ArrayList<TestCase> tests = new ArrayList<>();
	public RRT_OD_UPDATE(double min, double max, double r, double fail_rate, int randomseed) {
		super();
		this.min = min;
		this.max = max;
		this.R = r;
		this.fail_rate = fail_rate;
		this.randomseed = randomseed;
	}

	public void addNewNode(double value) {
		if (root == null) {
			root = new Node(value);
			size++;
		} else {
			Node current = root;
			Node pre = null;
			while (current != null && current.value < value) {
				pre = current;
				current = current.next;
			}
			Node newNode = new Node(value);
			newNode.next = current;
			if (pre == null)
				root = newNode;
			else
				pre.next = newNode;
			size++;
		}
	}

	public ArrayList getDatumAndBE() {
		double sum = 0;
		BeginEnd beroot = null;
		double radius = R / (2 * size);
		Node current = root;
		BeginEnd becurrent = null;
		if (root.value - radius - min > 0) {
			sum += root.value - radius - min;
			if (beroot == null) {
				beroot = new BeginEnd(min, root.value - radius);
				becurrent = beroot;
			}
		}
		while (current.next != null) {
			if (current.next.value - current.value > (2 * radius)) {
				sum += current.next.value - current.value - (2 * radius);
				if (beroot == null) {
					beroot = new BeginEnd(current.value + radius, current.next.value - radius);
					becurrent = beroot;
				} else {
					becurrent.next = new BeginEnd(current.value + radius, current.next.value - radius);
					becurrent = becurrent.next;
				}
			}
			current = current.next;
		}
		if (current.value + radius < max) {
			sum += max - (current.value + radius);
			if (beroot == null) {
				beroot = new BeginEnd(current.value + radius, max);
				becurrent = beroot;
			} else {
				becurrent.next = new BeginEnd(current.value + radius, max);
				becurrent = becurrent.next;
			}
		}
		ArrayList result = new ArrayList();
		result.add(1.0 / sum);
		result.add(beroot);
		return result;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public void print() {
		Node current = root;
		while (current != null) {
			System.out.println(current.value);
			current = current.next;
		}
	}

	public int run() {
		int count = 0;
		Random random = new Random(randomseed);
		fail_start = random.nextDouble() * (max - min - fail_rate) + min;
		double value = random.nextDouble() * (max - min) + min;

		while (isCorrect(value)) {
			count++;
			addNewNode(value);
			// 先求基准线o(n)并求得开始和结束
			ArrayList datumandbe = getDatumAndBE();
			double datum = (double) datumandbe.get(0);
			BeginEnd beroot = (BeginEnd) datumandbe.get(1);
			// T(0-1)
			double T = random.nextDouble();
			//
			BeginEnd becurrent = beroot;
			BeginEnd bepre = null;
			double PreInt = 0;
			double SumInt = 0;
			while (becurrent != null && (SumInt < T)) {
				PreInt = SumInt;
				SumInt += (becurrent.end - becurrent.start) * datum;
				bepre = becurrent;
				becurrent = becurrent.next;
			}
			// 产生下一个测试用例
			// PreInt+int(bepre.start->x)=T;
			value = ((T - PreInt) / datum) + bepre.start;
		}
		return count;
	}
}