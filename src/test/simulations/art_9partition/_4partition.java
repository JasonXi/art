package test.simulations.art_9partition;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import util.PaiLie;
import util.data.ZeroOneCreator;

public class _4partition extends ART {

	public _4partition(double[] min, double[] max, Random random, FailurePattern failurePattern) {
		super(min, max, random, failurePattern);
	}

	private ArrayList<NPoint> tests = new ArrayList<>();
	int count = 0;
	boolean isErrorOccurs=false;
	
	
	@Override
	public NPoint generateNextTC() {
		NPoint p = null;
		return p;
	}


	public NRectRegion splitRegionDistance(NPoint t1, NRectRegion region) {
		NRectRegion diagonalRegion = null;
		double[] pxn=t1.getXn();
		double[] start=region.getStart().getXn();
		double[] end=region.getEnd().getXn();
		double[] beSplitedPoint=new double[pxn.length];
		for(int i=0;i<start.length;i++){
			double firstPart=pxn[i]-start[i];
			double secondPart=end[i]-pxn[i];
			if(firstPart>secondPart){
				beSplitedPoint[i]=0.5*(pxn[i]+start[i]);
			}else{
				beSplitedPoint[i]=0.5*(end[i]+pxn[i]);
			}
		}
		ArrayList<NRectRegion> regions=new ArrayList<>();
		try {
			addRegionsInND(region, new NPoint(beSplitedPoint), regions);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// judge which is diagonal Region
		for(int i=0;i<regions.size();i++){
			NRectRegion tempRegion=regions.get(i);
			NPoint tempPoint=randomCreator.randomPoint(tempRegion);
			double[] tempXn=tempPoint.getXn();
			boolean flag=true;
			for(int j=0;j<tempXn.length;j++){
				if(pxn[j]>=beSplitedPoint[j]){
					if(tempXn[j]>=beSplitedPoint[j]){
						flag=false;
					}
				}
				if(pxn[j]<=beSplitedPoint[j]){
					if(tempXn[j]<=beSplitedPoint[j]){
						flag=false;
					}
				}
			}
			if(flag){
				diagonalRegion=tempRegion;
				break;
			}
		}
		return diagonalRegion;
	}
	public ArrayList<NRectRegion> splitRegionDistance(ArrayList<NPoint> points, ArrayList<NRectRegion> region) {
		ArrayList<NRectRegion> diagonalRegion = new ArrayList<>();
		for(int i=0;i<region.size();i++){
			for(int j=0;j<points.size();j++){
				if(InArea(region.get(i),points.get(j))){
					diagonalRegion.add(splitRegionDistance(points.get(j),region.get(i)));
				}
			}
		}
		return diagonalRegion;
	}
	private boolean InArea(NRectRegion nRectRegion, NPoint nPoint) {
		boolean flag = true;
		double[] pxn = nPoint.getXn();
		double[] start = nRectRegion.getStart().getXn();
		double[] end = nRectRegion.getEnd().getXn();
		for (int i = 0; i < this.dimension; i++) {
			if (pxn[i] < start[i] || pxn[i] > end[i]) {
				flag = false;
			}
		}
		return flag;
	}
	
	@Override
	public int run() {
		
		NPoint p = randomCreator.randomPoint();
		tests.add(p);
		System.out.println(p);
		ArrayList<NRectRegion> t2regions = new ArrayList<>();
		ArrayList<NRectRegion> regions = new ArrayList<>();
		regions.add(new NRectRegion(new NPoint(min), new NPoint(max)));
		while (this.failPattern.isCorrect(p)) {
			count++;
			t2regions=splitRegionDistance(tests, regions);
			generateT2(t2regions);
			if(!isErrorOccurs){
				regions=bisection(regions);
				generateT3orT4(regions,tests);
				if(isErrorOccurs){
					return count;
				}
			}else{
				return count;
			}
		}
		return count;
	}

	private void generateT3orT4(ArrayList<NRectRegion> regions,ArrayList<NPoint> tests) {
		for(int i=0;i<regions.size();i++){
			//no point in this region
			boolean isPointIn=false;
			for(int j=0;j<tests.size();j++){
				if(InArea(regions.get(i),tests.get(j))){
					isPointIn=true;
					break;
				}
			}
			if(!isPointIn){
				//generate test
				NPoint temp=randomCreator.randomPoint(regions.get(i)); 
				tests.add(temp);
				System.out.println(temp);
				if(!this.failPattern.isCorrect(temp)){
					isErrorOccurs=true;
					return;
				}
				count++;
			}
		}
	}

	private ArrayList<NRectRegion> bisection(ArrayList<NRectRegion> regions) {
		ArrayList<NRectRegion> bisectRegions=new ArrayList<>();
		for(int i=0;i<regions.size();i++){
			try {
				addRegionsInND(regions.get(i), midPoint(regions.get(i)),bisectRegions);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return bisectRegions;
	}
	private NPoint midPoint(NRectRegion region){
		double[] start=region.getStart().getXn();
		double[] end=region.getEnd().getXn();
		double[] result=new double[start.length];
		for(int i=0;i<result.length;i++){
			double midpoint=0.5*(start[i]+end[i]);
			result[i]=midpoint;
		}
		return new NPoint(result);
	}
	private void generateT2(ArrayList<NRectRegion> t2regions) {
		for(int i=0;i<t2regions.size();i++){
			NPoint temp=randomCreator.randomPoint(t2regions.get(i)); 
			System.out.println(temp);
			tests.add(temp);
			if(!this.failPattern.isCorrect(temp)){
				isErrorOccurs=true;
				return;
			}
			count++;
		}
	}
	public void addRegionsInND(NRectRegion region, NPoint p,ArrayList<NRectRegion> regions) throws Exception {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}
			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			regions.add(tempRegion);
		}
	}
	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}
	public static void main(String[] args) {
		int d = 2;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 100;
//
		

		int temp = 0;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			_4partition rt = new _4partition(min, max, new Random(i * 5), failurePattern);
			temp = rt.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		double fm = sums / (double) times;
		System.out.println("fm:" + fm + " time:" + ((endTime - startTime) / (double) times));
	}
}
