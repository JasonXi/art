package test.simulations.art_9partition;

import java.util.Arrays;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.failurepattern.FailurePattern;
import test.ART;

class TestFor9partition2 extends ART{

	public TestFor9partition2(double[] min, double[] max, Random random, FailurePattern failurePattern) {
		super(min, max, random, failurePattern);
	}

	@Override
	public NPoint generateNextTC() {
		return null;
	}
	
	public int run(){
		int count=0;
		NPoint p=randomCreator.randomPoint();
		while(this.failPattern.isCorrect(p)){
			count++;
			//split regions
			//首先确定那么几个点
			//x1....xn
			//x1(X,x12p,....x1np)
			for(int i=0;i<this.dimension;i++){
				double[] xn=Arrays.copyOf(p.getXn(), p.getXn().length);
				double min=this.min[i];
				double max=this.max[i];
				double pxi=xn[i];
				xn[i]=max-pxi>pxi-min?0.5*(max+pxi):0.5*(min+pxi);
				NPoint xi=new NPoint(xn);
			}
			//根据n个点划分3^n次方个区域来
			
		}
		
		return count;
	}
}