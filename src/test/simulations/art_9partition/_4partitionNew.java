package test.simulations.art_9partition;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import util.PaiLie;
import util.data.ZeroOneCreator;

class _4NRectRegion {
	NRectRegion region;
	boolean isCanGen;
}

public class _4partitionNew extends ART {

	ArrayList<_4NRectRegion> regions = new ArrayList<>();
	double[] eachRegionLength = new double[this.dimension];
	ArrayList<NPoint> tests = new ArrayList<>();

	public _4partitionNew(double[] min, double[] max, Random random, FailurePattern failurePattern) throws Exception {
		super(min, max, random, failurePattern);
		ArrayList<NRectRegion> initRegions = SplitRegionIn4(new NRectRegion(new NPoint(min), new NPoint(max)));
		for (NRectRegion initRegion : initRegions) {
			_4NRectRegion region = new _4NRectRegion();
			region.region = initRegion;
			region.isCanGen = true;
			regions.add(region);
		}
		// calculate eachRegionLength
		if (regions.size() > 0) {
			calculateEachRegionLength(regions.get(0).region);
		} else {
			System.out.println("error in calculateEachRegionLength");
		}
	}

	public void calculateEachRegionLength(NRectRegion region) {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();

		for (int i = 0; i < this.dimension; i++) {
			eachRegionLength[i] = end[i] - start[i];
		}
	}
	public ArrayList<NRectRegion> SplitRegionIn2(NRectRegion originRegion) throws Exception {
		int count = (int) Math.pow(2, this.dimension);

		// split once
		ArrayList<NRectRegion> tempRegions = new ArrayList<>();
		addRegionsInND(originRegion, midPoint(originRegion), tempRegions);

		if (count != tempRegions.size()) {
			System.out.println("error in split region to 4");
		}

		return tempRegions;
	}
	public ArrayList<NRectRegion> SplitRegionIn4(NRectRegion originRegion) throws Exception {
		int count = (int) Math.pow(4, this.dimension);

		// split once
		ArrayList<NRectRegion> tempRegions = new ArrayList<>();
		addRegionsInND(originRegion, midPoint(originRegion), tempRegions);

		// split twice
		ArrayList<NRectRegion> resultRegions = new ArrayList<>();
		for (NRectRegion tempRegion : tempRegions) {
			addRegionsInND(tempRegion, midPoint(tempRegion), resultRegions);
		}
		if (count != resultRegions.size()) {
			System.out.println("error in split region to 4");
		}

		return resultRegions;
	}

	private NPoint midPoint(NRectRegion region) {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] result = new double[start.length];
		for (int i = 0; i < result.length; i++) {
			double midpoint = 0.5 * (start[i] + end[i]);
			result[i] = midpoint;
		}
		return new NPoint(result);
	}

	public void addRegionsInND(NRectRegion region, NPoint p, ArrayList<NRectRegion> regions) throws Exception {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}
			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			regions.add(tempRegion);
		}
	}

	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}

	public boolean isTwoRegionOrg(NRectRegion region1, NRectRegion region2) {
		double[] start1 = region1.getStart().getXn();
		double[] end1 = region1.getEnd().getXn();
		double[] start2 = region2.getStart().getXn();
		double[] end2 = region2.getEnd().getXn();

		for (int i = 0; i < this.dimension; i++) {
			if (start1[i] == start2[i]) {
				if (end1[i] == end2[i]) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isTwoRegionNeighbor(NRectRegion region1, NRectRegion region2) {
		double[] start1 = region1.getStart().getXn();
		double[] end1 = region1.getEnd().getXn();
		double[] start2 = region2.getStart().getXn();
		double[] end2 = region2.getEnd().getXn();

		boolean flag = true;
		for (int i = 0; i < this.dimension; i++) {
			if (Math.abs(start1[i] - start2[i]) > eachRegionLength[i]
					|| Math.abs(end1[i] - end2[i]) > eachRegionLength[i]) {
				flag = false;
			}
		}
		return flag;
	}

	public void judgeExclusion(_4NRectRegion randomRegion) {
		// exclusion 1
		//System.out.println("judge 1");
		for (int i = 0; i < regions.size(); i++) {
			NRectRegion eachRegion = regions.get(i).region;
			if (isTwoRegionOrg(randomRegion.region, eachRegion)) {
				regions.get(i).isCanGen = false;
				//System.out.println("exclsuion zone:"+regions.get(i).region);
			}
		}
		//System.out.println("judge 2");
		// exclusion 2 (neighborhood)
		for (int i = 0; i < regions.size(); i++) {
			NRectRegion eachRegion = regions.get(i).region;
			if (isTwoRegionNeighbor(randomRegion.region, eachRegion)) {
				regions.get(i).isCanGen = false;
				//System.out.println("exclsuion zone:"+regions.get(i).region);
			}
		}
	}

	private boolean InArea(NRectRegion nRectRegion, NPoint nPoint) {
		boolean flag = true;
		double[] pxn = nPoint.getXn();
		double[] start = nRectRegion.getStart().getXn();
		double[] end = nRectRegion.getEnd().getXn();
		for (int i = 0; i < this.dimension; i++) {
			if (pxn[i] < start[i] || pxn[i] > end[i]) {
				flag = false;
			}
		}
		return flag;
	}

	@Override
	public NPoint generateNextTC() {
		NPoint p = null;
		if (tests.size() == 0) {
			_4NRectRegion randomRegion = regions.get(random.nextInt(regions.size()));
			p = randomCreator.randomPoint(randomRegion.region);
			tests.add(p);
			// exclusion
			judgeExclusion(randomRegion);
		} else {
			// judge if exist canGen region
			ArrayList<_4NRectRegion> availableRegions = new ArrayList<>();
			for (int i = 0; i < regions.size(); i++) {
				_4NRectRegion eachRegion = regions.get(i);
				if (eachRegion.isCanGen) {
					availableRegions.add(eachRegion);
				}
			}
			while (availableRegions.size() == 0) {
				//System.out.println("split iterative ");
				// redefine
				// regions=new ArrayList<>();
				ArrayList<_4NRectRegion> tempResultRegions = new ArrayList<>();
				for (int i = 0; i < regions.size(); i++) {
					try {
						ArrayList<NRectRegion> temp = SplitRegionIn2(regions.get(i).region);
						for (int j = 0; j < temp.size(); j++) {
							NRectRegion tempRegion = temp.get(j);
							_4NRectRegion _4Region = new _4NRectRegion();
							_4Region.region = tempRegion;
							_4Region.isCanGen = true;
							tempResultRegions.add(_4Region);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				regions = tempResultRegions;
				//calculate eachLength;
				calculateEachRegionLength(regions.get(0).region);
				
				// map new Point
				for (int i = 0; i < tests.size(); i++) {
					for (int j = 0; j < regions.size(); j++) {
						if (InArea(regions.get(j).region, tests.get(i))) {
							regions.get(j).isCanGen = false;
							judgeExclusion(regions.get(j));
						}
					}
				}
				availableRegions = new ArrayList<>();
				for (int i = 0; i < regions.size(); i++) {
					_4NRectRegion eachRegion = regions.get(i);
					if (eachRegion.isCanGen) {
						availableRegions.add(eachRegion);
					}
				}
			}
			// generate RandomPoint
			_4NRectRegion randomRegion = availableRegions.get(random.nextInt(availableRegions.size()));
			p = randomCreator.randomPoint(randomRegion.region);
			tests.add(p);

			// exclusion
			judgeExclusion(randomRegion);

		}
		return p;
	}

	public static void main(String[] args) throws Exception {
		int d = 2;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double[] min = dataCreator.minCreator(d);
		double[] max = dataCreator.maxCreator(d);
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.01;
		failurePattern.min = min;
		failurePattern.max = max;
		failurePattern.dimension = d;

		
		long fm=0;
		int times=500;
		for(int i=0;i<times;i++){
			ART rt = new _4partitionNew(min, max, new Random(i*3), failurePattern);
			int temp=rt.run();
			fm+=temp;
		}
		System.out.println(fm/(double)times);
//		for (int i = 0; i < 10; i++) {
//			System.out.println(rt.generateNextTC());
//		}
	}
}
