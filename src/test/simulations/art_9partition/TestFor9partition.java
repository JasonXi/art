package test.simulations.art_9partition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

public class TestFor9partition {
	static ArrayList<Integer> BeginAndEnd(int a,int size){
		ArrayList<Integer> BnN = new ArrayList<>();	
		BnN.add(a);
		BnN.add(a+size);
		return BnN;
	}
	static ArrayList<Integer> SortData(ArrayList<Integer> seq, ArrayList<Integer> BnN, int size)
	{
		int b=BnN.get(0);
		int e=BnN.get(1); 
		
		int s=size/seq.size();

		ArrayList<Integer> data = new ArrayList<>(); ArrayList<Integer> sorted=new ArrayList<>();
		int c=0;

		for(int i=b;i<e;i+=s){
			data.add(i);
		}
		
		for(int k=0;k<seq.size();++k)
		{
			sorted.add(data.get(seq.get(k)));
		}
		return sorted;	
	}
	public static ArrayList<Integer> toList(int[] arr){
		if(arr==null){return null;}
		ArrayList<Integer> result=new ArrayList<>(arr.length);
		for(int i=0;i<arr.length;i++){
			result.add(arr[i]);
		}
		return result;
	}
	static ArrayList<Integer> Sort(int R, int D) {

		int factor = (int) Math.pow(3, D);
		int N = (int) Math.pow(factor, R);
		ArrayList<Integer> seq = new ArrayList<>();
		int Dim = D;
		switch (Dim) {
		case 1: {
			int arr1[] = { 0, 2, 1 };
			seq.addAll(toList(arr1));
			}
			break;

		case 2: {
			int arr2[] = { 1, 8, 3, 2, 7, 0, 5, 6, 4 }; // 2,16,6,4,14,0,10,12,8
			seq.addAll(toList(arr2));
		}
			break;

		case 3: {
			int arr3[] = { 1, 17, 20, 9, 5, 6, 10, 26, 12, 16, 8, 0, 22, 3, 25, 11, 15, 19, 4, 24, 13, 21, 14, 7, 18, 2,
					23 };
			seq.addAll(toList(arr3));
		}
			break;

		case 4:
			int arr4[] = { 0, 8, 72, 84, 40, 24, 28, 44, 108, 52, 68, 36, 4, 92, 16, 104, 48, 64, 80, 20, 100, 56, 88,
					60, 32, 76, 12, 96, 216, 272, 204, 116, 296, 112, 136, 168, 316, 120, 252, 180, 240, 128, 260, 144,
					284, 228, 176, 304, 192, 156, 292, 224, 116, 256, 152, 312, 200, 132, 236, 184, 320, 268, 148, 208,
					288, 164, 244, 140, 300, 228, 124, 280, 196, 220, 308, 160, 276, 212, 172, 264, 188 };
			seq.addAll(toList(arr4));
			break;
		}

		ArrayList<Integer> NumberArr = new ArrayList<>();

		int size = N / factor;

		for (int s = 0; s < seq.size(); ++s) {
			NumberArr.add(seq.get(s) * size);
		}

		ArrayList<Integer> NewData=new ArrayList<>();
		ArrayList<Integer> Data=new ArrayList<>();
		ArrayList<Integer> bnN;

		while (size > 1) {
			NewData.clear();
			for (int a = 0; a < NumberArr.size(); ++a) {
				bnN = BeginAndEnd(NumberArr.get(a), size);

				Data = SortData(seq, bnN, size);

				for (int d = 0; d < Data.size(); ++d) {
					NewData.add(Data.get(d));
				}
				Data.clear();
			}
			NumberArr.clear();

			for (int n = 0; n < NewData.size(); ++n) {
				NumberArr.add(NewData.get(n));
			}

			size = size / factor;
		}

		return NumberArr;
	}
	public static void main(String[] args) {
		int D=2;//Dimension
		int r=1;//round
		
		ArrayList<Integer> indexes=new ArrayList<>();
		indexes=Sort(r,D);
		int ct=0;
		for(int i=0;i<indexes.size();++i)
		{
			System.out.println(indexes.get(i));
			ct+=1;
		}
		
		
		/*int ctr=0; int count=0;

		int factor=(int)Math.pow(3,D);
		int N=(int)Math.pow(factor,r);

		
		
		int Mul=ct/factor; ArrayList<Integer> Testcases=new ArrayList<>();
		
		for(int i=0;i<ct/factor;++i)
		{
			for(int y=0;y<factor;++y)
			{
				int ptr=i+y*Mul;
				Testcases.add(indexes.get(ptr));	
			}	
		}
		int counttest=0;
		for(int a=0;a<Testcases.size();++a)
		{
			System.out.println(Testcases.get(a));
			counttest+=1;
		}*/
		
	//	cout<<"Total: "<<ct<<endl;
	//	cout<<"Total Testcases : "<<counttest<<endl;
	}
}
