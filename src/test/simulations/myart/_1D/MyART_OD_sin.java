package test.simulations.myart._1D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

/**
 * 模拟实验，beta取固定的值 左曲线：
 * 
 * 第一段： C*(e0-An)*(sin((2pi*x/(e0-An)-0.5pi-An*(2pi)/(e0-An))+1)
 * 第二段: C*(ei1-ei)*(sin((2pi*x)/(ei1-ei)-0.5pi-ei*(2pi)/(ei1-ei))+1)
 * 第三段：C*(Bn-en)*a*(sin((2pi*x)/(Bn-en)0.5pi-Bn*(2pi)/(Bn-en))+1)
 * 
 *
 */
public class MyART_OD_sin {
	private static double PI=Math.PI;
	
	//public static int ZUOQUXIAN = 1;
	//public static int YOUQUXIAN = 2;
	public static void main(String[] args) throws Exception {
		int times = 1;
		long sums = 0;
		int temp = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			MyART_OD_sin ccrr = new MyART_OD_sin(0, 1, 0.001, 0.5, i * 3);
			temp = ccrr.test();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	double min;
	double max;
	int seed;
	double fail_rate;
	double fail_start;
	double beta;

	ArrayList<TestCase> tests = new ArrayList<>();

	public MyART_OD_sin(double min, double max, double fail_rate, double beta, int seed) {
		this.min = min;
		this.max = max;
		this.fail_rate = fail_rate;
		this.seed = seed;
		this.beta = beta;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + (max - min) * fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}
	public double sin(double a){
		return Math.sin(a);
	}
	public double cos(double a){
		return Math.cos(a);
	}
	/**
	 * @return F-measure
	 * @throws Exception
	 */
	public int test() {
		Random random = new Random(seed);
		double fail_size = (max - min) * (fail_rate);
		fail_start = random.nextDouble() * (max - fail_size);
		int count = 0;
		TestCase p = new TestCase();
		// 执行第一个测试用例
		p.p = random.nextDouble() * (max - min) + min;
		
		while (isCorrect(p.p)&&count<=1) {
			count++;
			System.out.println(p);
			if (tests.size() == 0) {
				tests.add(p);
			} else
				sortTestCases(p);
			///
			double datum_line;// 基准线，待会求出
			double tempProbability = 0.0;
			double sumProbability = 0.0;
			ArrayList<double[]> integrals = new ArrayList<>();
			/// 下面产生下一个测试用例,根据自己的概率曲线图
			// 先求第一段
			double e0=tests.get(0).p;
			double An=random.nextDouble() * (e0 - min) + (2 * min - e0);
			double EA=e0-An;
			//jifen
			tempProbability =(EA*EA/2*PI)*(cos(-0.5*PI-An*(2*PI)/EA)+2*PI+An*2*PI/EA) ;
			sumProbability += tempProbability;
			double[] informations = new double[5];
			informations[0] = tempProbability;
			informations[1] = 0;
			informations[2] = min;
			informations[3] = e0;
			informations[4] = beta;
			integrals.add(informations);
			// 求中间一段的积分值
			for (int i = 0; i < tests.size() - 1; i++) {
				double ei1=tests.get(i+1).p;
				double ei=tests.get(i).p;
				//jifen
				tempProbability = (ei1-ei)*(ei1-ei);
				sumProbability += tempProbability;
				informations = new double[5];
				informations[0] = tempProbability;
				informations[1] = 0;
				informations[2] = ei;
				informations[3] = ei1;
				informations[4] = beta;
				integrals.add(informations);
				
			}
			double en=tests.get(tests.size()-1).p;
			double Bn=random.nextDouble() * (max - en) + max;
			double Be=Bn-en;
			//jifen
			tempProbability = ((Be)*Be/(2*PI))*(-1*cos((2*PI*(1-Bn)/Be)-0.5*PI)+((2*PI*(1-Bn)/(Be))-0.5*PI)+2.5*PI);
			sumProbability += tempProbability;
			informations = new double[5];
			informations[0] = tempProbability;
			informations[1] = 0;
			informations[2] = en;
			informations[3] = max;
			informations[4] = beta;
			integrals.add(informations);
			datum_line = 1.0 / sumProbability;
			System.out.println("datum:"+datum_line);
			double T = random.nextDouble() * 1.0;
			// 确定在哪一段，然后求解出下一个点
			double SumIntegral = 0.0;
			double PreIntegral = 0.0;
			int temp = 0;
			// 这里有问题，temp的值不一定就是前面一个的temp,理解错了，其实没有错
			for (int i = 0; i < integrals.size(); i++) {
				if (SumIntegral < T) {
					PreIntegral = SumIntegral;
					temp = i;
				}
				SumIntegral += integrals.get(i)[0] * datum_line;
			}
			// draw picture
			// double drawtemp[]=integrals.get(0);

			// 求下一个测试用例
			//int type = (int) integrals.get(temp)[1];
			double start = integrals.get(temp)[2];
			double end = integrals.get(temp)[3];
			if(temp==0){
				
			}else if(temp==tests.size()){
				
			}else{
				
			}
			if (Double.isNaN(p.p) || p.p < min || p.p > max) {
				System.out.println("Interrupt!!");
			}
		}

		return count;
	}

}
