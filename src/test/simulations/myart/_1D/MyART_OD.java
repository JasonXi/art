package test.simulations.myart._1D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

/**
 * 模拟实验，beta取固定的值 左曲线：
 * 
 */
public class MyART_OD {
	public static int ZUOQUXIAN = 1;
	public static int YOUQUXIAN = 2;
	public static void main(String[] args) throws Exception {
		int times = 170;
		
		
		for (int i = 0; i < times; i++) {
			long startTime = System.currentTimeMillis();
			long sums = 0;
			int temp = 0;
			double be=1.95+(i+1)*0.05;
			for(int j=0;j<2000;j++){
				MyART_OD ccrr = new MyART_OD(0, 1, 0.002, be, j * 3);
				//ccrr.beta=i+1;
				temp = ccrr.test();
				sums += temp;
			}
			long endTime = System.currentTimeMillis();
			System.out.println( (be)+" "+(sums /2000.0));
			//System.out.println("Time: " + (endTime - startTime) / (double) times);
		}
		
	}
	double min;
	double max;
	int seed;
	double fail_rate;
	double fail_start;
	double beta;

	ArrayList<TestCase> tests = new ArrayList<>();

	public MyART_OD(double min, double max, double fail_rate, double beta, int seed) {
		this.min = min;
		this.max = max;
		this.fail_rate = fail_rate;
		this.seed = seed;
		this.beta = beta;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + (max - min) * fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}

	/**
	 * @return F-measure
	 * @throws Exception
	 */
	public int test() {
		Random random = new Random(seed);
		double fail_size = (max - min) * (fail_rate);
		fail_start = random.nextDouble() * (max - fail_size);
		int count = 0;
		TestCase p = new TestCase();
		// 执行第一个测试用例
		p.p = random.nextDouble() * (max - min) + min;
		
		while (isCorrect(p.p)) {
			count++;
			//System.out.println(p);
			if (tests.size() == 0) {
				tests.add(p);
			} else
				sortTestCases(p);
			///
			double datum_line;// 基准线，待会求出
			double tempProbability = 0.0;
			double sumProbability = 0.0;
			ArrayList<double[]> integrals = new ArrayList<>();
			/// 下面产生下一个测试用例,根据自己的概率曲线图
			// 先求第一段
			tempProbability = Math.pow((tests.get(0).p - min), 2.0) / (beta + 1.0);
			sumProbability += tempProbability;
			double[] informations = new double[5];
			informations[0] = tempProbability;
			informations[1] = ZUOQUXIAN;
			informations[2] = min;
			informations[3] = tests.get(0).p;
			informations[4] = beta;
			integrals.add(informations);
			// 求中间一段的积分值
			for (int i = 0; i < tests.size() - 1; i++) {
				// 右边
				tempProbability = Math.pow(((tests.get(i + 1).p - tests.get(i).p) / 2.0), 2.0) * (1.0 / (beta + 1.0));
				sumProbability += tempProbability;
				informations = new double[5];
				informations[0] = tempProbability;
				informations[1] = YOUQUXIAN;
				informations[2] = tests.get(i).p;
				informations[3] = (tests.get(i + 1).p + tests.get(i).p) / 2.0;
				informations[4] = beta;
				integrals.add(informations);
				// 下一个点的左边
				tempProbability = Math.pow(((tests.get(i + 1).p - tests.get(i).p) / 2.0), 2.0) * (1.0 / (beta + 1.0));
				sumProbability += tempProbability;
				informations = new double[5];
				informations[0] = tempProbability;
				informations[1] = ZUOQUXIAN;
				informations[2] = (tests.get(i + 1).p + tests.get(i).p) / 2.0;
				informations[3] = tests.get(i + 1).p;
				informations[4] = beta;
				integrals.add(informations);
			}
			tempProbability = Math.pow((max - tests.get(tests.size() - 1).p), 2.0) / (beta + 1.0);
			sumProbability += tempProbability;
			informations = new double[5];
			informations[0] = tempProbability;
			informations[1] = YOUQUXIAN;
			informations[2] = tests.get(tests.size() - 1).p;
			informations[3] = max;
			informations[4] = beta;
			integrals.add(informations);
			datum_line = 1.0 / sumProbability;
			//System.out.println("datum:"+datum_line);
			double T = random.nextDouble() * 1.0;
			// 确定在哪一段，然后求解出下一个点
			double SumIntegral = 0.0;
			double PreIntegral = 0.0;
			int temp = 0;
			// 这里有问题，temp的值不一定就是前面一个的temp,理解错了，其实没有错
			for (int i = 0; i < integrals.size(); i++) {
				if (SumIntegral < T) {
					PreIntegral = SumIntegral;
					temp = i;
				}
				SumIntegral += integrals.get(i)[0] * datum_line;
			}
			// draw picture
			// double drawtemp[]=integrals.get(0);

			// 求下一个测试用例
			int type = (int) integrals.get(temp)[1];
			double start = integrals.get(temp)[2];
			double end = integrals.get(temp)[3];
			if (type == ZUOQUXIAN) {
				double temp1 = end - start;
				double temp2 = integrals.get(temp)[4] + 1.0;
				p = new TestCase();
				double temp3 = (1.0 - (T - PreIntegral) * (temp2) / ((datum_line) * (Math.pow(temp1, 2.0))));
				p.p = end - temp1 * Math.pow(temp3, (1.0 / temp2));
			} else {
				double temp1 = end - start;
				// 这里也是错误的->double temp2=p.coverage+1.0
				double temp2 = integrals.get(temp)[4] + 1.0;
				p = new TestCase();
				p.p = start + temp1
						* Math.pow((T - PreIntegral) * temp2 / (datum_line * Math.pow(temp1, 2.0)), (1.0 / temp2));
				// p.coverage=10;
			}
			if (Double.isNaN(p.p) || p.p < min || p.p > max) {
				System.out.println("Interrupt!!");
			}
		}

		return count;
	}

}
