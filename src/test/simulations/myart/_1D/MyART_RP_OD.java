package test.simulations.myart._1D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

public class MyART_RP_OD {
	public static int ZUOQUXIAN = 1;
	public static int YOUQUXIAN = 2;
	public static void main(String[] args) {
		int times = 3000;
		long sums = 0;
		int temp = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			MyART_RP_OD myART_RP_OD = new MyART_RP_OD(0, 1, 0.001, 10, i * 3);
			temp = myART_RP_OD.test();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	double min;
	double max;
	int seed;
	double beta;
	double fail_rate;
	double fail_start;

	ArrayList<TestCase> tests = new ArrayList<>();

	public MyART_RP_OD(double min, double max, double fail_rate, double beta, int seed) {
		this.min = min;
		this.max = max;
		this.seed = seed;
		this.fail_rate = fail_rate;
		this.beta = beta;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}

	public int test() {
		Random random = new Random(seed);
		int count = 0;
		fail_start = random.nextDouble() * (max - min - fail_rate);
		TestCase p = new TestCase();
		// 执行第一个测试用例
		p.p = random.nextDouble() * (max - min) + min;
		// System.out.println("po:" + p.p);
		while (isCorrect(p.p)) {
			count++;
			if (tests.size() == 0) {
				tests.add(p);
			} else
				sortTestCases(p);
			double datum_line;// 基准线，待会求出
			// System.out.println("radius:"+radius);
			ArrayList<double[]> integrals = new ArrayList<>();
			double[] informations = null;
			/// 下面产生下一个测试用例,根据自己的概率曲线图
			// 先求第一段
			double e1 = tests.get(0).p;// first node
			double en = tests.get(tests.size() - 1).p;// last node
			// 求出最大的一段
			// from to 積分的上下限
			double from[] = null;
			double to[] = null;
			double tempInt[] = null;
			double maxdistance = 0;
			// 第一段
			if (tests.size() == 1) {
				maxdistance = max - min;
				from = new double[] { min, e1 };
				to = new double[] { e1, max };
				tempInt = new double[] { Math.pow((tests.get(0).p - min), 2.0) / (beta + 1.0),
						Math.pow((max - en), 2.0) / (beta + 1.0) };
			} else {
				for (int i = 0; i < tests.size(); i++) {
					double distance;
					if (i == 0) {
						distance = (tests.get(1).p + e1) / 2.0 - min;
						if (distance > maxdistance) {
							maxdistance = distance;
							from = new double[] { min, e1 };
							to = new double[] { e1, (tests.get(1).p + e1) / 2.0 };
							tempInt = new double[] { Math.pow((tests.get(0).p - min), 2.0) / (beta + 1.0),
									Math.pow(((tests.get(i + 1).p - tests.get(i).p) / 2.0), 2.0)
											* (1.0 / (beta + 1.0)) };
						}
					} else if (i == tests.size() - 1) {
						distance = max - ((en + tests.get(tests.size() - 2).p) / 2.0);
						if (distance > maxdistance) {
							maxdistance = distance;
							from = new double[] { (en + tests.get(tests.size() - 2).p) / 2.0, en };
							to = new double[] { en, max };
							tempInt = new double[] {
									Math.pow(((tests.get(i).p - tests.get(i - 1).p) / 2.0), 2.0) * (1.0 / (beta + 1.0)),
									Math.pow((max - tests.get(tests.size() - 1).p), 2.0) / (beta + 1.0) };
						}
					} else {
						double ei_1 = tests.get(i - 1).p;
						double ei = tests.get(i).p;
						double ei1 = tests.get(i + 1).p;
						distance = (ei1 + ei) / 2.0 - ((ei + ei_1) / 2.0);
						if (distance > maxdistance) {
							maxdistance = distance;
							from = new double[] { (ei_1 + ei) / 2.0, ei };
							to = new double[] { ei, (ei + ei1) / 2.0 };
							tempInt = new double[] {
									Math.pow(((tests.get(i).p - tests.get(i - 1).p) / 2.0), 2.0) * (1.0 / (beta + 1.0)),
									Math.pow(((tests.get(i + 1).p - tests.get(i).p) / 2.0), 2.0)
											* (1.0 / (beta + 1.0)) };
						}
					}

				}
			}
			// System.out.println("from:" + (from == null) + " to:" + (to == null) + "
			// temp:" + (tempInt == null) + " ");
			// System.out.println("maxdistance:" + maxdistance);
			// System.out.println(from[0] + " -> " + to[0]);
			// System.out.println(from[1] + " -> " + to[1]);
			// System.out.println(tempInt[0] + " , " + tempInt[1]);
			informations = new double[7];
			informations[0] = tempInt[0];
			informations[1] = ZUOQUXIAN;
			informations[2] = from[0];
			informations[3] = to[0];
			informations[4] = beta;
			integrals.add(informations);
			informations = new double[7];
			informations[0] = tempInt[1];
			informations[1] = YOUQUXIAN;
			informations[2] = from[1];
			informations[3] = to[1];
			informations[4] = beta;
			integrals.add(informations);
			// 求出int

			datum_line = 1.0 / (tempInt[0] + tempInt[1]);
			double T = random.nextDouble() * 1.0;
			// 确定在哪一段，然后求解出下一个点
			double SumIntegral = 0.0;
			double PreIntegral = 0.0;
			int temp = 0;
			// 这里有问题，temp的值不一定就是前面一个的temp,理解错了，其实没有错
			for (int i = 0; i < integrals.size(); i++) {
				if (SumIntegral < T) {
					PreIntegral = SumIntegral;
					temp = i;
				}
				SumIntegral += integrals.get(i)[0] * datum_line;
			}
			// 求下一个测试用例
			int type = (int) integrals.get(temp)[1];
			double start = integrals.get(temp)[2];
			double end = integrals.get(temp)[3];
			// System.out.println("type:"+type+" start:"+start+" end:"+end);
			if (type == ZUOQUXIAN) {
				double temp1 = end - start;
				double temp2 = integrals.get(temp)[4] + 1.0;
				p = new TestCase();
				double temp3 = (1.0 - (T - PreIntegral) * (temp2) / ((datum_line) * (Math.pow(temp1, 2.0))));
				p.p = end - temp1 * Math.pow(temp3, (1.0 / temp2));
				if (!(p.p > start && p.p < end)) {
					System.out.println("error left");
				}
			} else {
				double temp1 = end - start;
				double temp2 = integrals.get(temp)[4] + 1.0;
				p = new TestCase();
				p.p = start + temp1
						* Math.pow((T - PreIntegral) * temp2 / (datum_line * Math.pow(temp1, 2.0)), (1.0 / temp2));
				if (!(p.p > start && p.p < end)) {
					System.out.println("error right");
				}
			}
			if (Double.isNaN(p.p) || p.p < min || p.p > max) {
				System.out.println("Interrupt!!");
			}
		}
		return count;
	}
}
