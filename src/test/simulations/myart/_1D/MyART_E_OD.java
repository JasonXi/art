package test.simulations.myart._1D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

/*
 * CCofMyART2 with exclusion region （R）
 * maybe have some problems because the result is not very correct
 * */
public class MyART_E_OD {
	public static int ZUOQUXIAN = 1;
	public static int YOUQUXIAN = 2;
	public static void main(String[] args) throws Exception {
		int times = 1;
		long sums = 0;
		int temp = 0;
		long startTime = System.currentTimeMillis();

		for (int i = 0; i < times; i++) {
			MyART_E_OD ccrr = new MyART_E_OD(0, 1, 0.001, 10, 0.75, i * 9);
			temp = ccrr.test();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	double min;
	double max;
	int seed;
	double R;
	double beta;
	double fail_rate;
	double fail_start;
	double coverage = 10;

	ArrayList<TestCase> tests = new ArrayList<>();

	public MyART_E_OD(double min, double max, double fail_rate, double beta, double R, int seed) {
		this.min = min;
		this.max = max;
		this.seed = seed;
		this.R = R;
		this.fail_rate = fail_rate;
		this.beta = beta;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}

	/**
	 * @return F-measure
	 *
	 */
	public int test() {
		Random random = new Random(seed);
		fail_start = random.nextDouble() * (max - min - fail_rate);
		TestCase p = new TestCase();
		// 执行第一个测试用例
		p.p = random.nextDouble() * (max - min) + min;
		p.coverage = coverage;// useless
		int count = 0;
		while (isCorrect(p.p)) {
			count++;
			if (tests.size() == 0) {
				tests.add(p);
			} else
				sortTestCases(p);
			double datum_line;// 基准线，待会求出
			double tempProbability = 0.0;
			double sumProbability = 0.0;
			double radius = R / (2 * tests.size());
			// System.out.println("radius:"+radius);
			ArrayList<double[]> integrals = new ArrayList<>();
			double[] informations = null;
			/// 下面产生下一个测试用例,根据自己的概率曲线图
			// 先求第一段
			if (tests.get(0).p - radius > min) {
				// calcu by wolfamalpha d(e1^(b+1)-r^(b+1))/((b+1)*m)
				double m = tests.get(0).p - min;// m表示间距
				double n = tests.get(0).p;// n表示对应的点
				double from = min;// 积分下限
				double to = tests.get(0).p - radius;// 积分上限
				double temp2 = beta + 1.0;//
				tempProbability = ((-m * m) / (temp2))
						* (Math.pow((n - to) / m, temp2) - Math.pow((n - from) / m, temp2));
				sumProbability += tempProbability;
				informations = new double[7];
				informations[0] = tempProbability;
				informations[1] = ZUOQUXIAN;
				informations[2] = from;
				informations[3] = to;
				informations[4] = beta;
				informations[5] = m;
				informations[6] = n;
				integrals.add(informations);
			}
			// 求中间一段的积分值
			for (int i = 0; i < tests.size() - 1; i++) {
				// 右
				if (tests.get(i + 1).p - tests.get(i).p > 2 * radius) {
					double m = (tests.get(i + 1).p - tests.get(i).p) / 2.0;
					double n = tests.get(i).p;
					double from = tests.get(i).p + radius;
					double to = (tests.get(i + 1).p + tests.get(i).p) / 2.0;
					double temp2 = beta + 1.0;
					tempProbability = (m * m / temp2)
							* (Math.pow((to - n) / m, temp2) - Math.pow((from - n) / m, temp2));
					sumProbability += tempProbability;
					informations = new double[7];
					informations[0] = tempProbability;
					informations[1] = YOUQUXIAN;
					informations[2] = from;
					informations[3] = to;
					informations[4] = beta;
					informations[5] = m;
					informations[6] = n;
					integrals.add(informations);
					// 下一个点的左边
					n = tests.get(i + 1).p;
					from = (tests.get(i + 1).p + tests.get(i).p) / 2.0;
					to = tests.get(i + 1).p - radius;
					temp2 = beta + 1.0;
					tempProbability = ((-m * m) / (temp2))
							* (Math.pow((n - to) / m, temp2) - Math.pow((n - from) / m, temp2));
					sumProbability += tempProbability;
					informations = new double[7];
					informations[0] = tempProbability;
					informations[1] = ZUOQUXIAN;
					informations[2] = from;
					informations[3] = to;
					informations[4] = beta;
					informations[5] = m;
					informations[6] = n;
					integrals.add(informations);
				}
			}
			if (tests.get(tests.size() - 1).p + radius < max) {
				int indexofLast = tests.size() - 1;
				double m = max - tests.get(indexofLast).p;
				double n = tests.get(indexofLast).p;
				double from = n + radius;
				double to = max;
				double temp2 = beta + 1.0;
				tempProbability = (m * m / temp2) * (Math.pow((to - n) / m, temp2) - Math.pow((from - n) / m, temp2));
				;
				sumProbability += tempProbability;
				informations = new double[7];
				informations[0] = tempProbability;
				informations[1] = YOUQUXIAN;
				informations[2] = from;
				informations[3] = to;
				informations[4] = beta;
				informations[5] = m;
				informations[6] = n;
				integrals.add(informations);
				// logger.info("end part");
			}
			datum_line = 1.0 / sumProbability;
			double T = random.nextDouble() * 1.0;
			double SumIntegral = 0.0;
			double PreIntegral = 0.0;
			int temp = 0;
			for (int i = 0; i < integrals.size(); i++) {
				if (SumIntegral < T) {
					PreIntegral = SumIntegral;
					temp = i;
				}
				SumIntegral += integrals.get(i)[0] * datum_line;
			}
			// 求下一个测试用例
			int type = (int) integrals.get(temp)[1];
			double start = integrals.get(temp)[2];// 积分下限 from to x
			double end = integrals.get(temp)[3];
			double m = integrals.get(temp)[5];// 间距
			double n = integrals.get(temp)[6];// 关联的点
			if (type == ZUOQUXIAN) {
				double temp2 = integrals.get(temp)[4] + 1.0;
				p = new TestCase();
				double temp3 = ((T - PreIntegral) * temp2 / (-datum_line * m * m)) + Math.pow((n - start) / m, temp2);
				p.p = n - m * (Math.pow(temp3, 1.0 / temp2));
				if (!(p.p > start && p.p < end)) {
					System.out.println("error left");
				}
			} else {
				double temp2 = integrals.get(temp)[4] + 1.0;
				p = new TestCase();
				double temp3 = ((temp2 * (T - PreIntegral)) / (datum_line * m * m)) + Math.pow((start - n) / m, temp2);
				p.p = n + m * (Math.pow(temp3, 1.0 / temp2));
				if (!(p.p > start && p.p < end)) {
					System.out.println("error right");
				}
			}
			if (Double.isNaN(p.p) || p.p < min || p.p > max) {
				System.out.println("Interrupt!!");
			}
		}
		return count;
	}
}
