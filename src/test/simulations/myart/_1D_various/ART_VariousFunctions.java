package test.simulations.myart._1D_various;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.OD.TPVariousInfo;
import datastructure.failurepattern.FailurePattern;
import test.ART;

public class ART_VariousFunctions extends ART{
	
	private static int ZUO=0;
	private static int YOU=1;
	
	private int function=1;
	private List<NPoint> tests=new ArrayList<>();//test cases
	
	////积分信息，点的起始点（start,end） 实际开始的起始点（from,to），左右曲线（flag=0，左，flag=1，右）
	//积分值probability，
	private List<TPVariousInfo> integrals=new ArrayList<>();
	
	//当前生成的测试用例在哪一个区域上
	private int index=-1;
	
	public ART_VariousFunctions(double[] min, double[] max, Random random, FailurePattern failurePattern) {
		super(min, max, random, failurePattern);
		
	}

	@Override
	public NPoint generateNextTC() {
		NPoint p=null;
		if(tests.size()==0){
			p=randomCreator.randomPoint();
			tests.add(p);
		}else{
			//根据已执行的测试用例构造测试剖面，这里直接重新计算，不做时间上面的考量
				//先求积分总和
			if(integrals.size()==0){
				//init integrals
				initIntegrals();
			}else{
				updateIntegrals(index);
			}
			double C=calC();
			double T = random.nextDouble() * 1.0;
			// 确定在哪一段，然后求解出下一个点
			double SumIntegral = 0.0;
			double PreIntegral = 0.0;
			int temp = 0;
			// temp 
			for (int i = 0; i < integrals.size(); i++) {
				if (SumIntegral < T) {
					PreIntegral = SumIntegral;
					temp = i;
				}
				SumIntegral += integrals.get(i).probality * C;
			}
			index=temp;
			//生成下一个测试用例
			//p
			TPVariousInfo info=integrals.get(temp);
			p=nextTC(function,info,C,T,SumIntegral,PreIntegral);
			//sort
			sortAndAdd(p);
		}
		return p;
	}
	private void updateIntegrals(int index2) {
		
	}

	private NPoint nextTC(int function2, TPVariousInfo info, double C,double T, double sumIntegral, double preIntegral) {
		
		return null;
	}

	private double calC(){
		double allProbability=0.0;
		for(int i=0;i<this.integrals.size();i++){
			allProbability+=this.integrals.get(i).probality;
		}
		return allProbability;
	}
	private void initIntegrals(){
		//one point and two(or four) integrals
		double p=tests.get(0).getXn()[0];
		double An=random.nextDouble() * (p - this.min[0]) + (2 * this.min[0] - p);
		double Bn= random.nextDouble() * (this.max[0] - p) + this.max[0];
		
		double mid1=(An+p)*0.5;
		double mid2=(Bn+p)*0.5;
		//第一段
		TPVariousInfo info1=new TPVariousInfo();
		info1.from=An;
		info1.to=mid1;
		info1.start=this.min[0];
		info1.end=mid1;
		info1.flag=ZUO;
		info1.probality=calIntegrals(function, info1);
		//中间
		info1=new TPVariousInfo();
		info1.from=mid1;
		info1.to=p;
		info1.start=mid1;
		info1.end=p;
		info1.flag=YOU;
		info1.probality=calIntegrals(function, info1);
		
		info1=new TPVariousInfo();
		info1.from=p;
		info1.to=mid2;
		info1.start=p;
		info1.end=mid2;
		info1.flag=ZUO;
		info1.probality=calIntegrals(function, info1);
		//最后一段
		info1=new TPVariousInfo();
		info1.from=mid2;
		info1.to=Bn;
		info1.start=mid2;
		info1.end=this.max[0];
		info1.flag=YOU;
		info1.probality=calIntegrals(function, info1);
	}
	
	//only for one dimension
	private void sortAndAdd(NPoint p) {
		double pp=p.getXn()[0];
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (pp > tests.get(mid).getXn()[0]) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (pp < tests.get(mid).getXn()[0]) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}
	
	
	private double  calIntegrals(int function,TPVariousInfo info){
		double result=0.0;
		double from=info.from;
		double to=info.to;
		double start=info.start;
		double end=info.end;
		int flag=info.flag;
		switch(function){
		case 1:
			//直线
			if(flag==ZUO){
				
			}
			break;
		}
		return result;
	}
}

