package test.simulations.art_orb;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;

public class ComplexRegion {
	public NRectRegion region;
	public NPoint pointInRegion;
}
