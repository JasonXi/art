package test.simulations.art_orb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.StripPatternIn2D;

@Deprecated
public class ORB_ND {
	public static void main(String[] args) {
		double[] min = { 0.0, 0.0 };
		double[] max = { 1.0, 1.0 };
		int times = 1000;
		int fm = 0;
		for (int i = 0; i < times; i++) {
			// FailurePattern pattern=new BlockPattern();
			FailurePattern pattern = new StripPatternIn2D();
			pattern.min = min;
			pattern.max = max;
			pattern.fail_rate = 0.001;
			pattern.dimension = 2;

			ORB_ND test = new ORB_ND(min, max, pattern, i * 5 + 3);
			int temp = test.run();
			fm += temp;
		}
		System.out.println(fm / (double) times);
	}
	double[] min;
	double[] max;
	double fail_rate;
	// String fail_mode;
	// double[] fail_start;
	long seed;
	int dimension;
	double totalAreaS;
	double failAreaS;
	Random random;
	// double eachFailLength;
	ArrayList<ComplexRegion> regions = new ArrayList<>();

	FailurePattern failurePattern = null;

	public ORB_ND(double[] min, double[] max, FailurePattern failurePattern, long seed) {
		this.min = min;
		this.max = max;
		// this.fail_rate = fail_rate;
		this.seed = seed;
		this.dimension = min.length;
		random = new Random(seed);
		totalAreaS = 1.0;
		for (int i = 0; i < this.dimension; i++) {
			totalAreaS *= max[i] - min[i];
		}
		this.failAreaS = fail_rate * totalAreaS;

		failurePattern.random = random;
		failurePattern.genFailurePattern();
		this.failurePattern = failurePattern;
		// this.eachFailLength = Math.pow(failAreaS, 1.0 / (double)
		// this.dimension);
		// generate Fail start;
		// fail_start = new double[this.dimension];
		// for (int i = 0; i < this.dimension; i++) {
		// fail_start[i] = random.nextDouble() * (max[i] - min[i] -
		// this.eachFailLength) + min[i];
		// }
	}

	public int findMaxRegion() {
		double maxSize = -1;
		int index = 0;
		for (int i = 0; i < regions.size(); i++) {
			if (regions.get(i).region.size() > maxSize) {
				maxSize = regions.get(i).region.size();
				index = i;
			}
		}
		return index;
	}

	public boolean isPointInRegion(NRectRegion region, NPoint p) {
		boolean flag = true;
		for (int i = 0; i < p.getXn().length; i++) {
			if (p.getXn()[i] < region.getStart().getXn()[i] || p.getXn()[i] > region.getEnd().getXn()[i]) {
				flag = false;
			}
		}
		return flag;
	}

	public NPoint randomTC() {
		// generate from the input domain
		NPoint point = new NPoint();
		double[] xn = new double[this.dimension];
		for (int i = 0; i < xn.length; i++) {
			xn[i] = random.nextDouble() * (max[i] - min[i]) + min[i];
		}
		point.setDimension(this.dimension);
		point.setXn(xn);
		return point;
	}

	// public boolean isCorrect(NPoint p) {
	// // boolean flag=true;
	// double[] xn = p.getXn();
	// boolean lead2Fail = false;
	// for (int i = 0; i < this.dimension; i++) {
	// if (xn[i] < this.fail_start[i] || xn[i] > (this.fail_start[i] +
	// this.eachFailLength)) {
	// lead2Fail = true;
	// }
	// }
	// // System.out.println(Arrays.toString(nDPoints));
	// // System.out.println("isFail:"+lead2Fail);
	// // lead2Fail=false,失效，=true不失效
	// return lead2Fail;
	// }

	public NPoint randomTC(NRectRegion region) {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] xn = new double[this.dimension];
		for (int i = 0; i < xn.length; i++) {
			xn[i] = random.nextDouble() * (end[i] - start[i]) + start[i];
		}
		NPoint result = new NPoint(xn);
		result.setDimension(this.dimension);
		return result;
	}

	public int run() {

		int count = 0;
		// first test case
		NPoint p = randomTC();
		// System.out.println("t1:" + p.toString());
		ComplexRegion region = new ComplexRegion();
		region.region = new NRectRegion(new NPoint(min), new NPoint(max));
		region.pointInRegion = p;
		regions.add(region);
		if (!failurePattern.isCorrect(p)) {
			return 1;
		}
		count++;
		NPoint t2 = randomTC();
		// System.out.println("t2:" + t2.toString());
		splitRegion(p, t2, regions.get(0));
		regions.remove(0);
		// System.out.println("------------------");
		while (failurePattern.isCorrect(t2)) {
			count++;

			// 再生成一个测试用例t2

			int index = findMaxRegion();
			ComplexRegion maxRegion = regions.get(index);
			// System.out.println(region.toString());
			// System.out.println("find max Region:"+(maxRegion.region));
			t2 = randomTC(maxRegion.region);
			// System.out.println("t2:" + t2.toString());
			// System.out.println("is T2 leads failure:"+!isCorrect(t2));

			if (!failurePattern.isCorrect(t2)) {
				break;
			}
			// split region
			// System.out.println("split region");
			regions.remove(index);
			splitRegion(maxRegion.pointInRegion, t2, maxRegion);
			// System.out.println("------------------");
			// // 生成p
			// index = findMaxRegion();
			// System.out.println("max region index:" + index);
			//
			// region = regions.get(index);
			// System.out.println("max region:"+region);
			// p = null;
			// p = randomTC(region);
			// System.out.println("t1:" + p.toString());
		}
		return count;
	}

	public void splitRegion(NPoint p, NPoint t2, ComplexRegion region) {
		double[] pn = p.getXn();
		double[] t2n = t2.getXn();

		double[] mid = new double[pn.length];
		for (int i = 0; i < mid.length; i++) {
			mid[i] = (pn[i] + t2n[i]) / 2.0;
		}
		// System.out.println("middle point:"+Arrays.toString(mid));
		// 找到最大的边
		double maxBian = 0.0;
		int maxIndex = 0;
		for (int i = 0; i < region.region.getStart().getXn().length; i++) {
			if (region.region.getEnd().getXn()[i] - region.region.getStart().getXn()[i] > maxBian) {
				maxBian = region.region.getEnd().getXn()[i] - region.region.getStart().getXn()[i];
				maxIndex = i;
			}
		}
		// System.out.println("1 "+(region.region.getEnd().getXn()[1] -
		// region.region.getStart().getXn()[1]));
		// System.out.println("2 "+(region.region.getEnd().getXn()[0] -
		// region.region.getStart().getXn()[0]));

		// System.out.println("max edge:"+(maxIndex+1));
		// 一分为二
		NRectRegion region1 = new NRectRegion();
		NRectRegion region2 = new NRectRegion();

		region1.setStart(region.region.getStart());
		double[] end = Arrays.copyOf(region.region.getEnd().getXn(), region.region.getEnd().getXn().length);
		end[maxIndex] = mid[maxIndex];
		region1.setEnd(new NPoint(end));

		double[] start = Arrays.copyOf(region.region.getStart().getXn(), region.region.getStart().getXn().length);
		start[maxIndex] = mid[maxIndex];
		region2.setStart(new NPoint(start));
		region2.setEnd(region.region.getEnd());
		// if (maxIndex == 0) {// x轴长
		// region1.setStart(region.region.getStart());
		// region1.setEnd(new NPoint(new double[] { mid[0],
		// region.region.getEnd().getXn()[1] }));
		//
		// region2.setStart(new NPoint(new double[] { mid[0],
		// region.region.getStart().getXn()[1] }));
		// region2.setEnd(region.region.getEnd());
		// } else if (maxIndex == 1) {// y轴长
		// region1.setStart(region.region.getStart());
		// region1.setEnd(new NPoint(new double[] { region.region.getEnd().getXn()[0],
		// mid[1] }));
		//
		// region2.setStart(new NPoint(new double[] {
		// region.region.getStart().getXn()[0], mid[1] }));
		// region2.setEnd(region.region.getEnd());
		// }
		ComplexRegion cr1 = new ComplexRegion();
		cr1.region = region1;
		if (isPointInRegion(region1, p)) {
			cr1.pointInRegion = p;
		} else {
			cr1.pointInRegion = t2;
		}
		ComplexRegion cr2 = new ComplexRegion();
		cr2.region = region2;
		if (isPointInRegion(region2, t2)) {
			cr2.pointInRegion = t2;
		} else {
			cr2.pointInRegion = p;
		}
		regions.add(cr1);
		regions.add(cr2);
	}
}
