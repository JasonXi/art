package test.simulations.art_orb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import util.data.ZeroOneCreator;


/**
 * Hilary's method
 * */
public class ORB_RRT_ND_Xi_Discrepancy extends ART {
	public static void main(String[] args) {
		int dimension = 3;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double[] min = dataCreator.minCreator(dimension);
		double[] max = dataCreator.maxCreator(dimension);
		int times = 2000;
		int fm = 0;
		for (int i = 0; i < times; i++) {
			// FailurePattern pattern=new BlockPattern();
			FailurePattern pattern = new BlockPattern();
			pattern.fail_rate = 0.001;

			ORB_RRT_ND_Xi_Discrepancy test = new ORB_RRT_ND_Xi_Discrepancy(min, max, 0.75, pattern, new Random(i * 5 + 3));
			int temp = test.run();
			fm += temp;
		}
		System.out.println(fm / (double) times);
	}
	double R;

	ArrayList<ComplexRegion> regions = new ArrayList<>();

	public ORB_RRT_ND_Xi_Discrepancy(double[] min, double[] max, double R, FailurePattern failurePattern, Random random) {
		super(min, max, random, failurePattern);
		this.R = R;
	}

	public double calculateRadius(NRectRegion region) {
		// 二维的
		//return Math.sqrt(R * region.size() / (Math.PI));
		
		//return Math.sqrt(R * region.size() / (Math.PI));
		
		//One D
		//return (R * region.size()) / 2;
		//Two D
		//return Math.pow((R * region.size() / (Math.PI)),0.5);       
		//Three D
		//return Math.pow((3 * R * region.size() /(4* (Math.PI))),(1/3));
		//Four D
		return Math.pow((2*R * region.size() / (Math.pow((Math.PI),2))),0.25);
	}

	public int findMaxRegion() {
		double maxSize = -1;
		int index = 0;
		for (int i = 0; i < regions.size(); i++) {
			if (regions.get(i).region.size() > maxSize) {
				maxSize = regions.get(i).region.size();
				index = i;
			}
		}
		return index;
	}

	public boolean isPointInRegion(NRectRegion region, NPoint p) {
		boolean flag = true;
		for (int i = 0; i < p.getXn().length; i++) {
			if (p.getXn()[i] < region.getStart().getXn()[i] || p.getXn()[i] > region.getEnd().getXn()[i]) {
				flag = false;
			}
		}
		return flag;
	}

	public NPoint randomTC(ComplexRegion region) {
		double radius = calculateRadius(region.region);
		NPoint result = randomCreator.randomPoint(region.region);
		boolean flag = true;
		while (flag) {
			flag = false;
			result = randomCreator.randomPoint(region.region);

			// 排除区域是圆
			// 计算距离
			double[] tested = region.pointInRegion.getXn();
			double distance = 0;
			double[] untested = result.getXn();
			for (int j = 0; j < this.dimension; j++) {
				distance += Math.pow((tested[j] - untested[j]), 2);
			}
			distance = Math.sqrt(distance);
			if (distance < radius) {
				flag = true;
				// break;
			}
			/*
			 * //排除区域是正方形 if(Math.abs(p.p-tests.get(i).p)<radius){
			 * if(Math.abs(p.q-tests.get(i).q)<radius){ flag=true; } }
			 */
		}
		return result;
	}

	@Override
	public NPoint generateNextTC() {
		return null;
	}



	public int run() {
		int count = 0;

		// first
		NPoint p = randomCreator.randomPoint();
		ComplexRegion region = new ComplexRegion();
		region.region = new NRectRegion(new NPoint(min), new NPoint(max));
		region.pointInRegion = p;
		regions.add(region);
		if (!failPattern.isCorrect(p)) {
			return 1;
		}
		count++;

		// second
		NPoint t2 = randomCreator.randomPoint();
		splitRegion(p, t2, regions.get(0));
		regions.remove(0);

		while (failPattern.isCorrect(t2)) {
			count++;

			// 再生成一个测试用例t2
			int index = findMaxRegion();
			ComplexRegion maxRegion = regions.get(index);
			t2 = randomTC(maxRegion);

			if (!failPattern.isCorrect(t2)) {
				break;
			}
			// split region
			regions.remove(index);
			splitRegion(maxRegion.pointInRegion, t2, maxRegion);
		}
		return count;
	}

	public void splitRegion(NPoint p, NPoint t2, ComplexRegion region) {
		double[] pn = p.getXn();
		double[] t2n = t2.getXn();

		double[] mid = new double[pn.length];
		for (int i = 0; i < mid.length; i++) {
			mid[i] = (pn[i] + t2n[i]) / 2.0;
		}
		// System.out.println("middle point:"+Arrays.toString(mid));
		// 找到最大的边
		double maxBian = 0.0;
		int maxIndex = 0;
		for (int i = 0; i < region.region.getStart().getXn().length; i++) {
			if (region.region.getEnd().getXn()[i] - region.region.getStart().getXn()[i] > maxBian) {
				maxBian = region.region.getEnd().getXn()[i] - region.region.getStart().getXn()[i];
				maxIndex = i;
			}
		}

		// 一分为二
		NRectRegion region1 = new NRectRegion();
		NRectRegion region2 = new NRectRegion();

		region1.setStart(region.region.getStart());
		double[] end = Arrays.copyOf(region.region.getEnd().getXn(), region.region.getEnd().getXn().length);
		end[maxIndex] = mid[maxIndex];
		region1.setEnd(new NPoint(end));

		double[] start = Arrays.copyOf(region.region.getStart().getXn(), region.region.getStart().getXn().length);
		start[maxIndex] = mid[maxIndex];
		region2.setStart(new NPoint(start));
		region2.setEnd(region.region.getEnd());

		//// 二维
		// if (maxIndex == 0) {// x轴长
		// region1.setStart(region.region.getStart());
		// region1.setEnd(new NPoint(new double[] { mid[0],
		// region.region.getEnd().getXn()[1] }));
		//
		// region2.setStart(new NPoint(new double[] { mid[0],
		// region.region.getStart().getXn()[1] }));
		// region2.setEnd(region.region.getEnd());
		// } else if (maxIndex == 1) {// y轴长
		// region1.setStart(region.region.getStart());
		// region1.setEnd(new NPoint(new double[] { region.region.getEnd().getXn()[0],
		// mid[1] }));
		//
		// region2.setStart(new NPoint(new double[] {
		// region.region.getStart().getXn()[0], mid[1] }));
		// region2.setEnd(region.region.getEnd());
		// }
		ComplexRegion cr1 = new ComplexRegion();
		cr1.region = region1;
		if (isPointInRegion(region1, p)) {
			cr1.pointInRegion = p;
		} else {
			cr1.pointInRegion = t2;
		}
		ComplexRegion cr2 = new ComplexRegion();
		cr2.region = region2;
		if (isPointInRegion(region2, t2)) {
			cr2.pointInRegion = t2;
		} else {
			cr2.pointInRegion = p;
		}
		regions.add(cr1);
		regions.add(cr2);
	}

	@Override
	public int em() {
		// TODO Auto-generated method stub
		return 0;
	}


	public NPoint[] generateTC(int num) {
		NPoint[] tcs=new NPoint[num+1];

		int count = 0;

		// first
		NPoint p = randomCreator.randomPoint();
		ComplexRegion region = new ComplexRegion();
		region.region = new NRectRegion(new NPoint(min), new NPoint(max));
		region.pointInRegion = p;
		regions.add(region);
		tcs[0]=p;
		count++;

		// second
		NPoint t2 = randomCreator.randomPoint();
		splitRegion(p, t2, regions.get(0));
		regions.remove(0);
		tcs[1]=t2;
		while (count<=(num-2)) {
			count++;

			// 再生成一个测试用例t2
			int index = findMaxRegion();
			ComplexRegion maxRegion = regions.get(index);
			t2 = randomTC(maxRegion);

			tcs[count]=t2;

			// split region
			regions.remove(index);
			splitRegion(maxRegion.pointInRegion, t2, maxRegion);
		}
		return tcs;
		//return count;
	
	
	
	 	}

	public double calTwoPointDistance(NPoint p1, NPoint p2) {
		double[] p1xn = p1.getXn();
		double[] p2xn = p2.getXn();
		double distance = 0.0;
		for (int i = 0; i < p1xn.length; i++) {
			distance += Math.pow((p2xn[i] - p1xn[i]), 2);
		}
		distance = Math.sqrt(distance);
		return distance;
	}
}
