package test.simulations.art_orb;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import test.simulations.art_rp.ART_RP_ND;
import util.PaiLie;
import util.data.ZeroOneCreator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by xijiaxiang on 2018/10/29.
 */
public class ORB2_ND extends ART {
    ArrayList<NPoint> tests = new ArrayList<>();
    ArrayList<NRectRegion> regions = new ArrayList<>();
    NRectRegion currentRegion = new NRectRegion(new NPoint(min), new NPoint(max));
    NPoint p = null;//currentTestCase
    double offsetRatio=0.2;

    public ORB2_ND(double[] min, double[] max, Random random, FailurePattern failurePattern) {
        super(min, max, random, failurePattern);
        regions.add(new NRectRegion(new NPoint(min), new NPoint(max)));//add the default region
    }
    public ORB2_ND(double[] min, double[] max, Random random, FailurePattern failurePattern,double ratio){
        super(min, max, random, failurePattern);
        regions.add(new NRectRegion(new NPoint(min), new NPoint(max)));//add the default region
        this.offsetRatio=ratio;
    }
    public NRectRegion maxRegion() {
        if (regions == null || regions.size() == 0) {
            System.out.println("can not find the largest region because the regions is null or has no data");
            return null;
        }
        if (regions.size() == 1) {
            return regions.get(0);
        } else {
            NRectRegion ret = regions.get(0);
            for (int i = 1; i < regions.size(); i++) {
                if (regions.get(i).size() > ret.size()) {
                    ret = regions.get(i);
                }
            }
            return ret;
        }
    }

    public void splitRegions(NPoint point, NRectRegion region) {
        this.regions.remove(region);
        double[] start = region.getStart().getXn();
        double[] end = region.getEnd().getXn();
        double[] pxn = point.getXn();
        List<List<Double>> result1 = splitRegions(start, pxn);
        List<List<Double>> result2 = splitRegions(pxn, end);
        if (result1.size() != result2.size()) {
            System.out.println("result1's size!=result2's size ,split region wrong");
        }
        for (int i = 0; i < result1.size(); i++) {
            List<Double> temp1 = result1.get(i);
            List<Double> temp2 = result2.get(i);
            double[] newStart = new double[temp1.size()];
            double[] newEnd = new double[temp2.size()];
            for (int j = 0; j < temp1.size(); j++) {
                newStart[j] = temp1.get(j);
                newEnd[j] = temp2.get(j);
            }
            NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
            this.regions.add(tempRegion);
        }
    }

    public List<List<Double>> splitRegions(double[] start, double[] end) {
        ArrayList<double[]> values = new ArrayList<>();
        for (int i = 0; i < start.length; i++) {
            double[] temp = new double[2];

            temp[0] = start[i];
            temp[1] = end[i];
            values.add(temp);
        }

        ArrayList<List<Double>> result = new ArrayList<>();
        PaiLie.per(values, 0, new ArrayList<>(), result);
        return result;
    }

    public NPoint isObtainTestCase(NRectRegion region) {
        //boolean ret = false;
        NPoint ret=null;
        for (int i = 0; i < tests.size(); i++) {
            NPoint point = tests.get(i);
            if (region.isPointInRegion(point)) {
                ret = point;
            }
        }
        return ret;
    }

    public NRectRegion calculateRegion(NPoint point, NRectRegion region) {
        double[] start = new double[this.dimension];
        double[] end = new double[this.dimension];
        double[] middlePoints = new double[this.dimension];
        for (int i = 0; i < point.getXn().length; i++) {
            double firstPartLength = point.getXn()[i] - region.getStart().getXn()[i];
            double secondPartLength = region.getEnd().getXn()[i] - point.getXn()[i];
            if (firstPartLength > secondPartLength) {
                start[i] = region.getStart().getXn()[i];
                end[i] = point.getXn()[i];
            } else {
                start[i] = point.getXn()[i];
                end[i] = region.getEnd().getXn()[i];
            }
            middlePoints[i] = 0.5 * (end[i] + start[i]);
            if (middlePoints[i] > point.getXn()[i]) {
                start[i] = middlePoints[i];
            } else {
                end[i] = middlePoints[i];
            }

        }
        splitRegions(new NPoint(middlePoints), region);
        return new NRectRegion(new NPoint(start), new NPoint(end));
    }

    @Override
    public NPoint generateNextTC() {

        /*if(tests.size()==0) {
            p = randomCreator.randomPoint(currentRegion);
            tests.add(p);
            isSplitRegion=true;
            return p;
        }else{
            if(isSplitRegion){
                currentRegion=calculateRegion(p,currentRegion);
                p=randomCreator.randomPoint(currentRegion);
                tests.add(p);
                return p;
            }else{
                //find max
                currentRegion=maxRegion();
                //is obtain test case
                p=randomCreator.randomPoint(currentRegion);
                //find largest region

                //
            }*/
        // first step is to find the largest region
        //if the largest region doesn't have test case, noted as p, then random create one
        //else split the region into four and find the furthest region from p
        //    then random create next test case.
        currentRegion = maxRegion();
        NPoint testCaseInRegion;
        if ((testCaseInRegion=isObtainTestCase(currentRegion))==null) {
            //random generate
            p = randomCreator.randomPoint(currentRegion,offsetRatio);
            tests.add(p);
        } else {
            //calculate
            currentRegion = calculateRegion(testCaseInRegion, currentRegion);
            p = randomCreator.randomPoint(currentRegion,offsetRatio);
            tests.add(p);
        }

        return p;

    }

    public static void main(String[] args) {
       /* int d = 2;
        ZeroOneCreator dataCreator = new ZeroOneCreator();
        double min[] = dataCreator.minCreator(d);
        double max[] = dataCreator.maxCreator(d);

        int testCaseCount = 4;

        FailurePattern failurePattern = new BlockPattern();
        failurePattern.fail_rate = 0.005;
        ORB2_ND rt = new ORB2_ND(min, max, new Random(3), failurePattern);
        for (int i = 0; i < testCaseCount; i++) {
            System.out.println(rt.generateNextTC());
        }*/
       testFm();
    }
    public static void testFm(){

        int d = 2;
        double ratio=0.25;//must between [0,0.5)
        ZeroOneCreator dataCreator = new ZeroOneCreator();
        double min[] = dataCreator.minCreator(d);
        double max[] = dataCreator.maxCreator(d);
        FailurePattern failurePattern = new BlockPattern();
        failurePattern.fail_rate = 0.005;

        int times = 6000;
        long fm=0;
        for (int i = 0; i < times; i++) {

            ORB2_ND rt = new ORB2_ND(min, max, new Random(i*3), failurePattern,ratio);

            fm+=rt.run();
        }
        System.out.println(fm/(double)times);
    }
}
