package test.simulations.art_orb;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import org.apache.poi.ss.formula.functions.Complex;
import test.ART;
import util.data.ZeroOneCreator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;


/**
 * Hilary's method
 * */
public class ORB_FSCS_ND extends ART {
	public static void main(String[] args) {
		int dimension = 3;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double[] min = dataCreator.minCreator(dimension);
		double[] max = dataCreator.maxCreator(dimension);
		int times = 3000;
		int fm = 0;
		for (int i = 0; i < times; i++) {
			// FailurePattern pattern=new BlockPattern();
			FailurePattern pattern = new BlockPattern();
			pattern.fail_rate = 0.001;

			ORB_FSCS_ND test = new ORB_FSCS_ND(min, max,  pattern, new Random(i * 5 + 3));
			int temp = test.run();
			fm += temp;
		}
		System.out.println(fm / (double) times);
	}

	private ArrayList<ComplexRegion> regions = new ArrayList<>();
	//offset Ratio
	private double offsetRatio=0.5;
	private int k=10;

	public ORB_FSCS_ND(double[] min, double[] max, FailurePattern failurePattern, Random random) {
		super(min, max, random, failurePattern);
	}


	public int findMaxRegion() {
		double maxSize = -1;
		int index = 0;
		for (int i = 0; i < regions.size(); i++) {
			if (regions.get(i).region.size() > maxSize) {
				maxSize = regions.get(i).region.size();
				index = i;
			}
		}
		return index;
	}

	public boolean isPointInRegion(NRectRegion region, NPoint p) {
		boolean flag = true;
		for (int i = 0; i < p.getXn().length; i++) {
			if (p.getXn()[i] < region.getStart().getXn()[i] || p.getXn()[i] > region.getEnd().getXn()[i]) {
				flag = false;
			}
		}
		return flag;
	}
	//public ArrayList<NPoint> judge
	public NPoint randomTC(ComplexRegion offsetRegion,ComplexRegion originRegion) {
		//judge how many test cases in this region and return these test cases;
		ArrayList<NPoint> alreadyPointsInOffsetRegion=new ArrayList<>();
		for(int i=0;i<regions.size();i++){
			if(isPointInRegion(offsetRegion.region,regions.get(i).pointInRegion)){
				alreadyPointsInOffsetRegion.add(regions.get(i).pointInRegion);
			}
		}
		//generate k test cases in origin region
		double maxDistance = -1.0;
		NPoint bestCandidate = null;
		for (int i = 0; i < k; i++) {
			NPoint candidate = randomCreator.randomPoint(originRegion.region);
			// 计算两个点的距离
			double minDistance = Double.MAX_VALUE;

			for (int j = 0; j < alreadyPointsInOffsetRegion.size(); j++) {
				double tempDistance = candidate.distanceToAnotherPoint(alreadyPointsInOffsetRegion.get(j));
				if (tempDistance < minDistance) {
					minDistance = tempDistance;
				}
			}
			if (maxDistance < minDistance) {
				maxDistance = minDistance;
				bestCandidate = candidate;
			}
		}


		return bestCandidate;
	}

	public int run() {
		int count = 0;

		// first
		NPoint p = randomCreator.randomPoint();
		ComplexRegion region = new ComplexRegion();
		region.region = new NRectRegion(new NPoint(min), new NPoint(max));
		region.pointInRegion = p;
		regions.add(region);
		if (!failPattern.isCorrect(p)) {
			return 1;
		}
		count++;

		// second
		NPoint t2 = randomCreator.randomPoint();
		splitRegion(p, t2, regions.get(0));
		regions.remove(0);

		while (failPattern.isCorrect(t2)) {
			count++;

			// 再生成一个测试用例t2
			int index = findMaxRegion();
			ComplexRegion maxRegion = regions.get(index);

			//generate offset
			ComplexRegion offsetRegion=generateOffset(maxRegion);
			t2 = randomTC(offsetRegion,maxRegion);

			if (!failPattern.isCorrect(t2)) {
				break;
			}
			// split region
			regions.remove(index);
			splitRegion(maxRegion.pointInRegion, t2, maxRegion);
		}
		return count;
	}
	public ComplexRegion generateOffset(ComplexRegion region){
		double[] regionStart=region.region.getStart().getXn();
		double[] regionEnd=region.region.getEnd().getXn();
		double[] newRegionStart=new double[regionStart.length];
		double[] newRegionEnd=new double[regionEnd.length];
		for(int i=0;i<regionStart.length;i++){
			double length=offsetRatio*(regionEnd[i]-regionStart[i]);
			newRegionStart[i]=regionStart[i]-length>this.min[i]?regionStart[i]-length:this.min[i];
			newRegionEnd[i]=regionEnd[i]+length<this.max[i]?regionEnd[i]+length:this.max[i];
		}
		ComplexRegion offsetRegion=new ComplexRegion();
		offsetRegion.region=new NRectRegion(new NPoint(newRegionStart),new NPoint(newRegionEnd));
		offsetRegion.pointInRegion=region.pointInRegion;
		return offsetRegion;
	}
	public void splitRegion(NPoint p, NPoint t2, ComplexRegion region) {
		double[] pn = p.getXn();
		double[] t2n = t2.getXn();

		double[] mid = new double[pn.length];
		for (int i = 0; i < mid.length; i++) {
			mid[i] = (pn[i] + t2n[i]) / 2.0;
		}
		// System.out.println("middle point:"+Arrays.toString(mid));
		// 找到最大的边
		double maxBian = 0.0;
		int maxIndex = 0;
		for (int i = 0; i < region.region.getStart().getXn().length; i++) {
			if (region.region.getEnd().getXn()[i] - region.region.getStart().getXn()[i] > maxBian) {
				maxBian = region.region.getEnd().getXn()[i] - region.region.getStart().getXn()[i];
				maxIndex = i;
			}
		}

		// 一分为二
		NRectRegion region1 = new NRectRegion();
		NRectRegion region2 = new NRectRegion();

		region1.setStart(region.region.getStart());
		double[] end = Arrays.copyOf(region.region.getEnd().getXn(), region.region.getEnd().getXn().length);
		end[maxIndex] = mid[maxIndex];
		region1.setEnd(new NPoint(end));

		double[] start = Arrays.copyOf(region.region.getStart().getXn(), region.region.getStart().getXn().length);
		start[maxIndex] = mid[maxIndex];
		region2.setStart(new NPoint(start));
		region2.setEnd(region.region.getEnd());

		//// 二维
		// if (maxIndex == 0) {// x轴长
		// region1.setStart(region.region.getStart());
		// region1.setEnd(new NPoint(new double[] { mid[0],
		// region.region.getEnd().getXn()[1] }));
		//
		// region2.setStart(new NPoint(new double[] { mid[0],
		// region.region.getStart().getXn()[1] }));
		// region2.setEnd(region.region.getEnd());
		// } else if (maxIndex == 1) {// y轴长
		// region1.setStart(region.region.getStart());
		// region1.setEnd(new NPoint(new double[] { region.region.getEnd().getXn()[0],
		// mid[1] }));
		//
		// region2.setStart(new NPoint(new double[] {
		// region.region.getStart().getXn()[0], mid[1] }));
		// region2.setEnd(region.region.getEnd());
		// }
		ComplexRegion cr1 = new ComplexRegion();
		cr1.region = region1;
		if (isPointInRegion(region1, p)) {
			cr1.pointInRegion = p;
		} else {
			cr1.pointInRegion = t2;
		}
		ComplexRegion cr2 = new ComplexRegion();
		cr2.region = region2;
		if (isPointInRegion(region2, t2)) {
			cr2.pointInRegion = t2;
		} else {
			cr2.pointInRegion = p;
		}
		regions.add(cr1);
		regions.add(cr2);
	}

	@Override
	public int em() {
		// 从TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NPoint generateNextTC() {
		// TODO Auto-generated method stub
		return null;
	}
}
