package test.simulations.rprrt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import util.PaiLie;
import util.data.ZeroOneCreator;
import util.triangle.TriangleUtil2D;

/**
 * @author xijiaxiang
 * @date   2017/12/13
 * 先采用直线， 后采用弧形 
 */
public class ART_RP_RRT2 extends ART {

	public ArrayList<NPoint> tests = new ArrayList<>();
	public ArrayList<NRectRegion> regions = new ArrayList<>();

	double R;

	public ART_RP_RRT2(double[] min, double[] max, Random random, FailurePattern failurePattern, double R) {
		super(min, max, random, failurePattern);
		this.R = R;
	}

	@Override
	public int run() {
		int count = 0;
		NPoint p = randomCreator.randomPoint();
		//System.out.println("p0:"+p);
		NRectRegion maxRegion = new NRectRegion(new NPoint(min), new NPoint(max));
		this.regions.add(maxRegion);
		int index = 0;

		while (this.failPattern.isCorrect(p)) {
			// count++;
			count++;

			// add tests
			this.tests.add(p);

			// split region
			splitRegion(index, p);
			// max region
			index = maxRegion();
			//System.out.println("maxregion:"+this.regions.get(index));
			// make the region min
			///NRectRegion[] smallerRegions = makeMaxRegionSmall(this.regions.get(index));
			// another point
			p = randomPoint(this.regions.get(index));
		}
		return count;
	}

	// 只生成测试用例的方法
	public NPoint generateTC() {
		NPoint p = null;

		if (this.tests.size() == 0) {
			p = randomCreator.randomPoint();
			this.regions.add(new NRectRegion(new NPoint(min), new NPoint(max)));
			splitRegion(0, p);
		} else {
			int index = maxRegion();
			p = randomCreator.randomPoint((this.regions.get(index)));
			splitRegion(index, p);
		}
		this.tests.add(p);

		return p;
	}

	public NPoint randomPoint(NRectRegion maxregion){
		NPoint p=super.randomCreator.randomPoint(maxregion);
		//System.out.println("temp0:"+p);
		while(!isOutArc(p, maxregion)){
			//System.out.println("temp:"+p);
			p=randomPoint(maxregion);
		}
		return p;
	}

	public boolean isOutArc(NPoint p,NRectRegion region){
		boolean result=false;
		
		double px1=p.getXn()[0];
		double px2=p.getXn()[1];
		double xmin=region.getStart().getXn()[0];
		double ymin=region.getStart().getXn()[1];
		double xmax=region.getEnd().getXn()[0];
		double ymax=region.getEnd().getXn()[1];
		double midx=0.5*(xmax+xmin);
		double midy=0.5*(ymax+ymin);
		if(px1<midx&&px2<midy){
			//zuoxia
			result=isOutArc(xmin, xmax, midx, midy, p,0);
		}
		if(px1>midx&&px2<midy){
			//youxia
			result=isOutArc(midx, xmax, ymin, midy, p,1);
		}
		if(px1>midx&&px2>midy){
			//youshang
			result=isOutArc(midx, xmax, midy, ymax, p,2);
		}
		if(px1<midx&&px2>midy){
			result=isOutArc(xmin, midx, midy, ymax, p,3);
		}
		return result;
	}
	public boolean isOutArc(double xmin,double xmax,double ymin,double ymax,NPoint p,int order){
		double midx=R*(xmin+xmax);
		double midy=R*(ymin+ymax);
		
		if(order==0){
			boolean flag1=TriangleUtil2D.IsPointInTriangle(new NPoint(new double[]{xmin,ymin}), new NPoint(new double[]{midx,ymax}), new NPoint(new double[]{xmin,ymax}), p);
			boolean flag2=TriangleUtil2D.IsPointInTriangle(new NPoint(new double[]{xmin,ymin}), new NPoint(new double[]{xmax,ymin}), new NPoint(new double[]{xmax,midy}), p);
			return !(flag1||flag2);
		}
		if(order==1){
			boolean flag1=TriangleUtil2D.IsPointInTriangle(new NPoint(new double[]{xmin,ymin}), new NPoint(new double[]{xmin,midy}), new NPoint(new double[]{xmax,ymin}), p);
			boolean flag2=TriangleUtil2D.IsPointInTriangle(new NPoint(new double[]{xmax,ymin}), new NPoint(new double[]{midx,ymax}), new NPoint(new double[]{xmax,ymax}), p);
			return !(flag1||flag2);
		}
		if(order==2){
			boolean flag1=TriangleUtil2D.IsPointInTriangle(new NPoint(new double[]{midx,ymin}), new NPoint(new double[]{xmax,ymin}), new NPoint(new double[]{xmax,ymax}), p);
			boolean flag2=TriangleUtil2D.IsPointInTriangle(new NPoint(new double[]{xmin,midy}), new NPoint(new double[]{xmin,ymax}), new NPoint(new double[]{xmax,ymax}), p);
			return !(flag1||flag2);
		}
		if(order==3){
			boolean flag1=TriangleUtil2D.IsPointInTriangle(new NPoint(new double[]{xmin,ymin}), new NPoint(new double[]{midx,ymin}), new NPoint(new double[]{xmin,ymax}), p);
			boolean flag2=TriangleUtil2D.IsPointInTriangle(new NPoint(new double[]{xmin,ymax}), new NPoint(new double[]{xmax,midy}), new NPoint(new double[]{xmax,ymax}), p);
			return !(flag1||flag2);
		}
		return false;
	}
	
	public void splitRegion(int index, NPoint p) {
		if (index < 0 || index >= this.regions.size()) {
			System.out.println("split region error! index not correct!");
			return;
		}
		// first remove it
		NRectRegion region = this.regions.remove(index);
		try {
			// add regions;
			addRegionsInND(region, p);
		} catch (Exception e) {
			System.out.println("split region error in split region rec");
		}
	}

	public void addRegionsInND(NRectRegion region, NPoint p) throws Exception {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		// System.out.println(result1.size());
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}

			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			this.regions.add(tempRegion);
		}
	}

	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}

	public int maxRegion() {
		int index = 0;
		double maxRegionSize = 0.0;
		for (int i = 0; i < this.regions.size(); i++) {
			double tempRegionSize = this.regions.get(i).size();
			if (tempRegionSize > maxRegionSize) {
				maxRegionSize = tempRegionSize;
				index = i;
			}
		}
		return index;
	}

	public static void main(String[] args) {
		int d=2;//only in 2dimension
		int times=3000;
		double failure_rate=0.001;
		double R=0.6;
		int fm=0;
		
		ZeroOneCreator dataCreator=new ZeroOneCreator();
		double[] min=dataCreator.minCreator(d);
		double[] max=dataCreator.maxCreator(d);
		
		for (int i = 0; i < times; i++) {
			FailurePattern pattern=new BlockPattern();
			pattern.fail_rate=failure_rate;
			ART_RP_RRT2 center = new ART_RP_RRT2(min, max, new Random(i * 3), pattern, R);
			int temp=center.run();
			fm+=temp;
		}
		System.out.println(fm/(double)times);
	}
	//效果不行，跟不均匀有关

	@Override
	public int em() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NPoint generateNextTC() {
		// TODO Auto-generated method stub
		return null;
	}
}
