package test.simulations.rprrt;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import test.simulations.art_center.ART_Center_ND;
import util.PaiLie;
import util.data.ZeroOneCreator;

public class ART_RP_RRT extends ART {

	public ArrayList<NPoint> tests = new ArrayList<>();
	public ArrayList<NRectRegion> regions = new ArrayList<>();

	double R;

	public ART_RP_RRT(double[] min, double[] max, Random random, FailurePattern failurePattern, double R) {
		super(min, max, random, failurePattern);
		this.R = R;
	}

	@Override
	public int run() {
		int count = 0;
		NPoint p = randomCreator.randomPoint();
		NRectRegion maxRegion = new NRectRegion(new NPoint(min), new NPoint(max));
		this.regions.add(maxRegion);
		int index = 0;

		while (this.failPattern.isCorrect(p)) {
			// count++;
			count++;

			// add tests
			this.tests.add(p);

			// split region
			splitRegion(index, p);
			// max region
			index = maxRegion();
			// make the region min
			NRectRegion[] smallerRegions = makeMaxRegionSmall(this.regions.get(index));
			// another point
			p = randomCreator.randomPoint(smallerRegions);
		}
		return count;
	}

	// 只生成测试用例的方法
	public NPoint generateTC() {
		NPoint p = null;

		if (this.tests.size() == 0) {
			p = randomCreator.randomPoint();
			this.regions.add(new NRectRegion(new NPoint(min), new NPoint(max)));
			splitRegion(0, p);
		} else {
			int index = maxRegion();
			p = randomCreator.randomPoint(makeMaxRegionSmall(this.regions.get(index)));
			splitRegion(index, p);
		}
		this.tests.add(p);

		return p;
	}

	public NRectRegion[] makeMaxRegionSmall(NRectRegion maxregion) {
		double[] start = maxregion.getStart().getXn();
		double[] end = maxregion.getEnd().getXn();

		double eachExcludedSize = (maxregion.size() * R) / (double) (Math.pow(2, this.dimension));

		// 四个角上都去掉(针对2维)
		// 敲定每个方块的每一维的长度。按比例缩小
		double[] xn = new double[this.dimension];
		double xlength = Math.sqrt(eachExcludedSize * (end[0] - start[0]) / (end[1] - start[1]));
		double ylength = xlength * ((end[1] - start[1])) / (end[0] - start[0]);

		NRectRegion[] results = new NRectRegion[5];
		results[0] = new NRectRegion(new NPoint(new double[] { start[0] + xlength, start[1] }),
				new NPoint(new double[] { end[0] - xlength, start[1] + ylength }));
		results[1] = new NRectRegion(new NPoint(new double[] { end[0] - xlength, start[1] + ylength }),
				new NPoint(new double[] { end[0], end[1] - ylength }));
		results[2] = new NRectRegion(new NPoint(new double[] { start[0] + xlength, end[1] - ylength }),
				new NPoint(new double[] { end[0] - xlength, end[1] }));
		results[3] = new NRectRegion(new NPoint(new double[] { start[0], start[1] + ylength }),
				new NPoint(new double[] { start[0] + xlength, end[1] - ylength }));
		results[4] = new NRectRegion(new NPoint(new double[] { start[0] + xlength, start[1] + ylength }),
				new NPoint(new double[] { end[0] - xlength, end[1] - ylength }));
		// return new NRectRegion(new NPoint(s1),new NPoint(e1));
		return results;
	}

	public void splitRegion(int index, NPoint p) {
		if (index < 0 || index >= this.regions.size()) {
			System.out.println("split region error! index not correct!");
			return;
		}
		// first remove it
		NRectRegion region = this.regions.remove(index);
		try {
			// add regions;
			addRegionsInND(region, p);
		} catch (Exception e) {
			System.out.println("split region error in split region rec");
		}
	}

	public void addRegionsInND(NRectRegion region, NPoint p) throws Exception {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		// System.out.println(result1.size());
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}

			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			this.regions.add(tempRegion);
		}
	}

	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}

	public int maxRegion() {
		int index = 0;
		double maxRegionSize = 0.0;
		for (int i = 0; i < this.regions.size(); i++) {
			double tempRegionSize = this.regions.get(i).size();
			if (tempRegionSize > maxRegionSize) {
				maxRegionSize = tempRegionSize;
				index = i;
			}
		}
		return index;
	}

	public static void main(String[] args) {
		int d=2;//only in 2dimension
		int times=3000;
		double failure_rate=0.001;
		double R=0.49999;
		int fm=0;
		
		ZeroOneCreator dataCreator=new ZeroOneCreator();
		double[] min=dataCreator.minCreator(d);
		double[] max=dataCreator.maxCreator(d);
		
		for (int i = 0; i < times; i++) {
			FailurePattern pattern=new BlockPattern();
			pattern.fail_rate=failure_rate;
			ART_RP_RRT center = new ART_RP_RRT(min, max, new Random(i * 3), pattern, R);
			int temp=center.run();
			fm+=temp;
		}
		System.out.println(fm/(double)times);
	}

	@Override
	public int em() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NPoint generateNextTC() {
		// TODO Auto-generated method stub
		return null;
	}

}
