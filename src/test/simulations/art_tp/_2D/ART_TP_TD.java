package test.simulations.art_tp._2D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.TPInfo;
import util.X3;

/*
 * 测试
 * 生成二维的ART_TP，用来探索n维的测试用例
 * 其中使用二维特殊化的，均有二维特殊化提醒
 * */
public class ART_TP_TD {
	public static void main(String[] args) {
		// start,end 表示边界，from to表示Anbn
		double[] min = { 0, 0 };
		double[] max = { 1, 1 };
		int times = 3000;
		int fm = 0;
		long start = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_TP_TD test = new ART_TP_TD(min, max, 0.01, i * 3 + 3);
			int temp = test.run();
			fm += temp;
		}
		long end = System.currentTimeMillis();
		System.out.println(fm / (double) times);
		System.out.println((end - start) / (double) times);
	}
	double[] min;
	double[] max;
	double fail_rate;
	double[] fail_start;
	long seed;
	int dimension;
	double totalAreaS;
	double failAreaS;
	double eachFailLength;
	Random random;
	ArrayList<NPoint> tests = new ArrayList<>();
	ArrayList<TPInfo> regions = new ArrayList<>();

	double C;// 常数

	public ART_TP_TD(double[] min, double[] max, double fail_rate, long seed) {
		this.min = min;
		this.max = max;
		this.fail_rate = fail_rate;
		this.seed = seed;
		random = new Random(this.seed);
		if (min.length == max.length) {
			this.dimension = min.length;
		}
		totalAreaS = 1.0;
		for (int i = 0; i < this.dimension; i++) {
			totalAreaS *= max[i] - min[i];
		}
		this.failAreaS = fail_rate * totalAreaS;
		this.eachFailLength = Math.pow(failAreaS, 1.0 / (double) this.dimension);
		// generate Fail start;
		fail_start = new double[this.dimension];
		for (int i = 0; i < this.dimension; i++) {
			fail_start[i] = random.nextDouble() * (max[i] - min[i] - this.eachFailLength) + min[i];
		}
	}

	public void addFromAndTo(TPInfo region) {
		NPoint start = region.start;
		NPoint end = region.end;
		double from1 = 0.0;
		double to1 = 0.0;
		double from2 = 0.0;
		double to2 = 0.0;
		if (start.getXn()[0] == min[0]) {
			from1 = random.nextDouble() * (end.getXn()[0] - min[0]) + (2 * min[0] - end.getXn()[0]);
		} else {
			from1 = start.getXn()[0];
		}
		if (start.getXn()[1] == min[1]) {
			from2 = random.nextDouble() * (end.getXn()[1] - min[1]) + (2 * min[1] - end.getXn()[1]);
		} else {
			from2 = start.getXn()[1];
		}
		if (end.getXn()[0] == max[0]) {
			to1 = random.nextDouble() * (max[0] - end.getXn()[0]) + max[0];
		} else {
			to1 = end.getXn()[0];
		}
		if (end.getXn()[1] == max[1]) {
			to2 = random.nextDouble() * (max[1] - end.getXn()[1]) + max[1];
		} else {
			to2 = end.getXn()[1];
		}
		region.from = new NPoint(new double[] { from1, from2 });
		region.to = new NPoint(new double[] { to1, to2 });
	}

	public void addTDRegions(double[] min, double[] max, NPoint p) {

		TPInfo info1 = new TPInfo();
		info1.start = new NPoint(new double[] { min[0], min[1] });
		info1.end = new NPoint(p.getXn());
		addFromAndTo(info1);

		TPInfo info2 = new TPInfo();
		info2.start = new NPoint(new double[] { p.getXn()[0], min[1] });
		info2.end = new NPoint(new double[] { max[0], p.getXn()[1] });
		addFromAndTo(info2);

		TPInfo info3 = new TPInfo();
		info3.start = new NPoint(p.getXn());
		info3.end = new NPoint(new double[] { max[0], max[1] });
		addFromAndTo(info3);

		TPInfo info4 = new TPInfo();
		info4.start = new NPoint(new double[] { min[0], p.getXn()[1] });
		info4.end = new NPoint(new double[] { p.getXn()[0], max[1] });
		addFromAndTo(info4);

		regions.add(info1);
		regions.add(info2);
		regions.add(info3);
		regions.add(info4);

		// System.out.println("add four regions:");
		// System.out.println(info1.start + "," + info1.end);
		// System.out.println(info2.start + "," + info2.end);
		// System.out.println(info3.start + "," + info3.end);
		// System.out.println(info4.start + "," + info4.end);
		// System.out.println("-------------------");
	}

	// public double calEachIntEC(double u, double v, double f, double t) {
	// // int((x-u)*(v-x),f,t)
	// return -(1.0 / 3.0) * (t * t * t - f * f * f) + (0.5 * (u + v) * (t * t -
	// f * f)) - u * v * (t - f);
	// }
	public double calEachIntEC2(double s, double e, double f, double t) {
		// int(x-f)*(t-x) s,t
		return (-1.0 / 6.0) * (e - s)
				* (e * (-3 * f + 2 * s - 3 * t) - 3 * f * s + 6 * f * t + 2 * s * s - 3 * s * t + 2 * e * e);
	}

	public double calEachRegion() {
		double tempC = 0.0;
		// System.out.println("each region cdf:");
		for (int i = 0; i < regions.size(); i++) {
			// 二维特殊化
			// NRectRegion temp=regions.get(i);
			NPoint start = regions.get(i).start;
			NPoint end = regions.get(i).end;
			NPoint from = regions.get(i).from;
			NPoint to = regions.get(i).to;
			double a = calEachIntEC2(start.getXn()[0], end.getXn()[0], from.getXn()[0], to.getXn()[0]);
			double b = calEachIntEC2(start.getXn()[1], end.getXn()[1], from.getXn()[1], to.getXn()[1]);
			regions.get(i).probality = a * b;
			regions.get(i).proa = a;
			regions.get(i).prob = b;
			tempC += (a * b);
			// System.out.println("int (x-" + from.getXn()[0] + ")*(" + to.getXn()[0] + "-x"
			// + ") from " + start.getXn()[0]
			// + " to " + end.getXn()[0]);
			// System.out.println("int (x-" + from.getXn()[1] + ")*(" + to.getXn()[1] + "-x"
			// + ") from " + start.getXn()[1]
			// + " to " + end.getXn()[1]);
			// System.out.println("from:"+(from1)+","+from2+" to:"+to1+","+to2);
			// System.out.println("start:"+(start.getXn()[0])+","+start.getXn()[1]+"
			// to:"+end.getXn()[0]+","+end.getXn()[1]);

			// System.out.println("eachValue:" + (a) + "," + b + " multi:" + (a * b));
			// System.out.println("*********");
		}
		// System.out.println("tempC:" + tempC);
		// System.out.println("------------");
		return tempC;
	}

	public double genNextEachDimension(double start, double end, double from, double to, double C, double aorb,
			double Pre, double T) {

		// System.out.println("cal next test case");
		// System.out.println("start:"+start+",end:"+end+",from:"+from+",to:"+to+",C:"+C+",aorb:"+aorb+",Pre:"+Pre+",T:"+T);
		// pre+c*(a|b)*int(start to x)((x-from)*(to-x))=T;
		double A = (-1.0 / 3.0) * C * aorb;
		double B = 0.5 * (from + to) * (C) * (aorb);
		double C1 = -from * to * C * aorb;
		double D = C * aorb
				* ((1.0 / 3.0) * (start * start * start) - 0.5 * (start * start) * (from + to) + from * to * start) - T
				+ Pre;
		double[] roots = X3.shengjinFormula(A, B, C1, D);
		// System.out.println("roots:"+Arrays.toString(roots));
		double next = -1.0;
		boolean flag = false;
		for (int i = 0; i < roots.length; i++) {
			if (roots[i] > start && roots[i] < end) {
				flag = true;
				next = roots[i];
				break;
			}
		}
		if (!flag) {
			System.out.println("x3 error!");
			// genNextEachDimension(start, end, from, to, C1, aorb, Pre, next)
			return Double.MIN_VALUE;
		}
		return next;
	}

	public boolean isCorrect(NPoint p) {
		// boolean flag=true;
		double[] xn = p.getXn();
		boolean lead2Fail = false;
		for (int i = 0; i < this.dimension; i++) {
			if (xn[i] < this.fail_start[i] || xn[i] > (this.fail_start[i] + this.eachFailLength)) {
				lead2Fail = true;
			}
		}
		// System.out.println(Arrays.toString(nDPoints));
		// System.out.println("isFail:"+lead2Fail);
		// lead2Fail=false,失效，=true不失效
		return lead2Fail;
	}

	public boolean isInRegion(NPoint start1, NPoint end1, NPoint p) {
		boolean flag = true;
		double[] pxn = p.getXn();
		double[] start = start1.getXn();
		double[] end = end1.getXn();
		for (int i = 0; i < this.dimension; i++) {
			if (pxn[i] < start[i] || pxn[i] > end[i]) {
				flag = false;
			}
		}
		return flag;
	}

	public NPoint randomTC() {
		// generate from the input domain
		NPoint point = new NPoint();
		double[] xn = new double[this.dimension];
		for (int i = 0; i < xn.length; i++) {
			xn[i] = random.nextDouble() * (max[i] - min[i]) + min[i];
		}
		point.setDimension(this.dimension);
		point.setXn(xn);
		return point;
	}

	public int run() {
		int count = 0;
		// generate a new test case random
		NPoint p = randomTC();
		// System.out.println("p:" + p.toString());
		while (isCorrect(p)) {
			count++;
			tests.add(p);
			updateRegions(p);// 重新计算现有的区域
			// generate next test case by test profile
			// ArrayList<Double> eachRegionProbility = new
			// ArrayList<>(this.regions.size());
			C = calEachRegion();
			C = 1.0 / C;
			// 随机产生一个0-1的数
			double T = random.nextDouble();
			// double T2=random.nextDouble();
			// System.out.println("C:" + C);
			// System.out.println("T:" + T);
			double SumIntegral = 0.0;// 积分值总和
			double PreIntegral = 0.0;
			int temp = 0;// 落在哪个区间
			for (int i = 0; i < regions.size(); i++) {
				if (SumIntegral < T) {
					PreIntegral = SumIntegral;
					temp = i;
				}
				// System.out.println("proba:" + regions.get(i).probality * C);
				SumIntegral += regions.get(i).probality * C;
			}
			// System.out.println("temp index:" + temp);
			// 计算积分值Pre+int((C)*(x1-from1)*(to1-x1)*(x2-from2)*(to2-x2))
			// 二维特殊化
			double[] start = this.regions.get(temp).start.getXn();
			double[] end = this.regions.get(temp).end.getXn();
			double[] from = this.regions.get(temp).from.getXn();
			double[] to = this.regions.get(temp).to.getXn();

			double T1 = random.nextDouble() * (regions.get(temp).probality * C) + PreIntegral;
			double T2 = random.nextDouble() * (regions.get(temp).probality * C) + PreIntegral;
			double x = genNextEachDimension(start[0], end[0], from[0], to[0], C, this.regions.get(temp).prob,
					PreIntegral, T1);
			while (x == Double.MIN_VALUE) {
				T1 = random.nextDouble() * (regions.get(temp).probality * C) + PreIntegral;
				x = genNextEachDimension(start[0], end[0], from[0], to[0], C, this.regions.get(temp).prob, PreIntegral,
						T1);
			}
			double y = genNextEachDimension(start[1], end[1], from[1], to[1], C, this.regions.get(temp).proa,
					PreIntegral, T);
			while (y == Double.MIN_VALUE) {
				T2 = random.nextDouble() * (regions.get(temp).probality * C) + PreIntegral;
				y = genNextEachDimension(start[1], end[1], from[1], to[1], C, this.regions.get(temp).proa, PreIntegral,
						T2);
			}
			p = null;
			p = new NPoint(new double[] { x, y });
			// System.out.println("p" + count + ":" + p);
		}
		return count;
	}

	public void updateRegions(NPoint p) {
		if (regions.size() == 0) {
			// 初始区域，添加四块区域（2的m次快）
			int regionsCount = (int) Math.pow(2, this.dimension);
			// 这里针对二维的
			addTDRegions(this.min, this.max, p);
			/// 二维特殊化
		} else {
			for (int i = 0; i < regions.size(); i++) {
				// 先判断这个点在不在这个区域内,要先删除区域
				if (isInRegion(regions.get(i).start, regions.get(i).end, p)) {
					TPInfo region = regions.remove(i);
					i--;
					addTDRegions(region.start.getXn(), region.end.getXn(), p);
					/// 二维特殊化
					break;
				}
			}
		}
	}
}
