package test.simulations.art_tp._2D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;
import datastructure.TD._2DPoint;
import datastructure.TD._2DRegion;

/**
 * 放弃开发二维的，直接开发N维ART_TP_RP
 */
public class ART_TP_RP_TD {
	// public double genNextK(Random random,double fixu,double fixv,double u,double
	// v){
	//
	// }
	public static void main(String[] args) {
		long fm = 0;
		for (int i = 0; i < 100; i++) {
			ART_TP_RP_TD art_TP_RP_TD = new ART_TP_RP_TD(null, 0.005, 3);
			// System.out.println(art_TP_RP_TD.partOfCo(0.5, 0.8, 0.5, 0.8));;
		}
	}
	int seedOfRandom;
	public _2DRegion initRegion;
	double fail_rate;
	double xfail_start;
	double yfail_start;
	Random random = null;
	ArrayList<TestCase> tests = new ArrayList<>();

	ArrayList<_2DRegion> regions = new ArrayList<>();

	public ART_TP_RP_TD(_2DRegion region, double fail_rate, int sed) {
		this.initRegion = region;
		this.fail_rate = fail_rate;
		this.seedOfRandom = sed;
		random = new Random(seedOfRandom);
	}

	public boolean isCorrect(TestCase p) {
		double x = p.p;
		double y = p.q;
		double bianchang = Math.pow(fail_rate, 0.5);
		if ((x > xfail_start && x < xfail_start + bianchang) && (y > yfail_start && y < yfail_start + bianchang)) {
			return false;
		} else {
			return true;
		}
	}

	public double partOfCo(double u, double v, double min, double max) {
		return (-1.0 / 3.0) * (max * max * max - min * min * min) + ((u + v) / 2.0) * (max * max - min * min)
				- u * v * (max - min);
	}

	public int run() {
		int count = 0;
		xfail_start = random.nextDouble() * (1.0 - Math.sqrt(fail_rate));
		yfail_start = random.nextDouble() * (1.0 - Math.sqrt(fail_rate));
		_2DRegion region = initRegion;
		TestCase p = new TestCase();
		p.p = random.nextDouble() * (region.max.x - region.min.x) + region.min.x;
		p.q = random.nextDouble() * (region.max.y - region.max.y) + region.min.y;
		while (isCorrect(p)) {
			count++;
			double maxsize = 0;
			_2DRegion maxregion = null;
			int maxregion_index = 0;
			for (int i = 0; i < regions.size(); i++) {
				_2DRegion temp = regions.get(i);
				if (temp.size() > maxsize) {
					maxsize = temp.size();
					maxregion = temp;
					maxregion_index = i;
				}
			}
			//
			regions.remove(maxregion_index);
			// generate next one test case by art_tp
			// 积分式：：int(xmin:xmax,ymin:ymax) (x1-u1)*(v1-x1)*(x2-u2)*(v2-x2)
			// 积分的上下限
			double xmin = maxregion.min.x;
			double xmax = maxregion.min.x + maxregion.max.x - maxregion.min.x;
			double ymin = maxregion.min.y;
			double ymax = maxregion.min.y + maxregion.max.y - maxregion.min.y;

			// 从哪到哪
			double u1 = xmin;
			double v1 = xmax;
			double u2 = ymin;
			double v2 = ymax;
			if (xmin == initRegion.min.x) {
				//
			}
			if (ymin == initRegion.min.y) {

			}
			if (xmax == initRegion.max.x) {

			}
			if (ymax == initRegion.max.y) {

			}
			double Co;
			// Co=1.0/(x-xmin)*(xmax-x)*(y-ymin)*(ymax-y)的曲面积分
			double intTempx = partOfCo(u1, v1, xmin, xmax);
			double intTempy = partOfCo(u2, v2, ymin, ymax);
			Co = 1.0 / (intTempx * intTempy);
			// random T
			double T = random.nextDouble();

			p = new TestCase();
			// p.p = random.nextDouble() * (xmax - xmin) + xmin;
			// p.q = random.nextDouble() * (ymax - ymin) + ymin;

			// add four regions
			_2DRegion first = new _2DRegion(new _2DPoint(xmin, ymin), new _2DPoint(p.p, p.q));
			_2DRegion second = new _2DRegion(new _2DPoint(xmin, p.q), new _2DPoint(p.p, ymax));
			_2DRegion third = new _2DRegion(new _2DPoint(p.p, p.q), new _2DPoint(xmax, ymax));
			_2DRegion fourth = new _2DRegion(new _2DPoint(p.p, ymin), new _2DPoint(xmax, p.q));
			regions.add(first);
			regions.add(second);
			regions.add(third);
			regions.add(fourth);
		}
		return count;
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}
}
