package test.simulations.art_tp._ND;
/*
 * 原生的ART_TP，不包括ART_Etp，ART_Btp，ART_RPtp
 * 
 * **/

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.TPInfo2;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import test.simulations.art_b.ART_B_ND;
import test.simulations.art_rp.ART_RP_ND;
import test.simulations.art_tpp.ART_TPP;
import util.X3;
import util.data.ZeroOneCreator;

//TODO 加入覆盖率的信息进行增强

public class ART_TP_ND_COV extends ART {
	public static void main(String[] args) {
		// start,end 表示边界，from to表示Anbn int d = 2; double[] min =
		 //testTCTime(2,10000);
		//testEm(1, 0.01);
		testFm();
	}

	ArrayList<NPoint> tests = new ArrayList<>();

	ArrayList<TPInfo2> regions = new ArrayList<>();

	double C;// 常数

	public ART_TP_ND_COV(double[] min, double[] max, FailurePattern pattern, Random random) {
		super(min, max, random, pattern);
	}

	public void addFromAndTo(TPInfo2 region) {
		double[] start = region.start.getXn();
		double[] end = region.end.getXn();
		double[] fromarr = new double[this.dimension];
		double[] toarr = new double[this.dimension];
		for (int i = 0; i < start.length; i++) {
			double from = 0.0;
			if (start[i] == min[i]) {
				from = random.nextDouble() * (end[i] - min[i]) + (2 * min[i] - end[i]);
			} else {
				from = start[i];
			}
			fromarr[i] = from;
			double to = 0.0;
			if (end[i] == max[i]) {
				to = random.nextDouble() * (max[i] - end[i]) + max[i];
			} else {
				to = end[i];
			}
			toarr[i] = to;
		}
		region.from = new NPoint(fromarr);
		region.to = new NPoint(toarr);
	}

	public void addRegions(double[] min, double[] max, double[] xn) {
		int count = (int) Math.pow(2, this.dimension);
		ArrayList<double[]> lists = new ArrayList<>(this.dimension);
		for (int i = 0; i < min.length; i++) {
			lists.add(new double[] { min[i], xn[i] });
		}
		ArrayList<double[]> lists2 = new ArrayList<>(this.dimension);
		for (int i = 0; i < min.length; i++) {
			lists2.add(new double[] { xn[i], max[i] });
		}
		ArrayList<double[]> result1 = new ArrayList<>(count);
		addRegionsRec(this.dimension, 0, new ArrayList<Double>(), lists, result1);

		ArrayList<double[]> result2 = new ArrayList<>(count);
		addRegionsRec(this.dimension, 0, new ArrayList<Double>(), lists2, result2);

		// for( int i=0;i<result1.size();i++){
		// System.out.println(Arrays.toString(result1.get(i))+"
		// "+Arrays.toString(result2.get(i)));
		// }
		for (int i = 0; i < result1.size(); i++) {
			TPInfo2 info = new TPInfo2();
			info.start = new NPoint(result1.get(i));
			info.end = new NPoint(result2.get(i));
			addFromAndTo(info);
			this.regions.add(info);
		}
	}

	public void addRegionsRec(int n, int k, List<Double> list, List<double[]> lists, ArrayList<double[]> result1) {
		if (list.size() == n) {
			// double[] temp=list.toArray();
			double[] temp = new double[n];
			for (int i = 0; i < temp.length; i++) {
				temp[i] = list.get(i);
			}
			result1.add(temp);
		} else {
			for (int i = 0; i < 2; i++) {
				List<Double> list2 = new ArrayList<Double>(list);
				list2.add(lists.get(k)[i]);
				addRegionsRec(n, ++k, list2, lists, result1);
				k--;
			}
		}
	}

	public double calEachIntEC(double s, double e, double f, double t) {
		// int(x-f)*(t-x) s,t
		return (-1.0 / 6.0) * (e - s)
				* (e * (-3 * f + 2 * s - 3 * t) - 3 * f * s + 6 * f * t + 2 * s * s - 3 * s * t + 2 * e * e);
	}

	public double calEachRegion() {
		double tempC = 0.0;
		// System.out.println("each region cdf:");
		for (int i = 0; i < regions.size(); i++) {
			// 二维特殊化
			// NRectRegion temp=regions.get(i);
			double[] start = regions.get(i).start.getXn();
			double[] end = regions.get(i).end.getXn();
			double[] from = regions.get(i).from.getXn();
			double[] to = regions.get(i).to.getXn();
			double probality = 1.0;
			regions.get(i).eachProbality = new double[this.dimension];
			for (int j = 0; j < start.length; j++) {
				double temp = calEachIntEC(start[j], end[j], from[j], to[j]);
				probality *= temp;
				regions.get(i).eachProbality[j] = temp;
			}
			// double b = calEachIntEC(start.getXn()[1], end.getXn()[1],
			// from.getXn()[1], to.getXn()[1]);
			regions.get(i).probality = probality;
			// regions.get(i).proa = a;
			// regions.get(i).prob = b;
			tempC += probality;
			// System.out.println("int (x-" + from.getXn()[0] + ")*(" +
			// to.getXn()[0] + "-x" + ") from " + start.getXn()[0]
			// + " to " + end.getXn()[0]);
			// System.out.println("int (x-" + from.getXn()[1] + ")*(" +
			// to.getXn()[1] + "-x" + ") from " + start.getXn()[1]
			// + " to " + end.getXn()[1]);
			// System.out.println("from:"+(from1)+","+from2+" to:"+to1+","+to2);
			// System.out.println("start:"+(start.getXn()[0])+","+start.getXn()[1]+"
			// to:"+end.getXn()[0]+","+end.getXn()[1]);

			// System.out.println("eachValue:" + (a) + "," + b + " multi:" + (a
			// * b));
			// System.out.println("*********");
		}
		// System.out.println("tempC:" + tempC);
		// System.out.println("------------");
		return tempC;
	}

	public double genNextEachDimension(double start, double end, double from, double to, double C, double aorb,
			double Pre, double T) {

		// System.out.println("cal next test case");
		// System.out.println("start:"+start+",end:"+end+",from:"+from+",to:"+to+",C:"+C+",aorb:"+aorb+",Pre:"+Pre+",T:"+T);
		// pre+c*(a|b)*int(start to x)((x-from)*(to-x))=T;
		double A = (-1.0 / 3.0) * C * aorb;
		double B = 0.5 * (from + to) * (C) * (aorb);
		double C1 = -from * to * C * aorb;
		double D = C * aorb
				* ((1.0 / 3.0) * (start * start * start) - 0.5 * (start * start) * (from + to) + from * to * start) - T
				+ Pre;
		double[] roots = X3.shengjinFormula(A, B, C1, D);
		// System.out.println("roots:"+Arrays.toString(roots));
		double next = -1.0;
		boolean flag = false;
		for (int i = 0; i < roots.length; i++) {
			if (roots[i] > start && roots[i] < end) {
				flag = true;
				next = roots[i];
				break;
			}
		}
		if (!flag) {
			// System.out.println("x3 error!");
			// next=genNextEachDimension(start, end, from, to, C1, aorb, Pre,
			// next);
			next = random.nextDouble();
			// return Double.MIN_VALUE;
		}
		return next;
	}

	public NPoint genNextTestCase(int index, double PreIntegral) {

		double[] start = this.regions.get(index).start.getXn();
		double[] end = this.regions.get(index).end.getXn();
		double[] from = this.regions.get(index).from.getXn();
		double[] to = this.regions.get(index).to.getXn();
		double[] result = new double[this.dimension];
		for (int i = 0; i < this.dimension; i++) {
			double T = random.nextDouble() * (regions.get(index).probality * C) + PreIntegral;
			double temp = genNextEachDimension(start[i], end[i], from[i], to[i], C,
					this.regions.get(index).probality / this.regions.get(index).eachProbality[i], PreIntegral, T);
			result[i] = temp;
		}
		NPoint p = new NPoint(result);
		return p;
	}

	public void updateRegions(NPoint p, int index) {
		if (regions.size() == 0) {
			addRegions(this.min, this.max, p.getXn());
		} else {
			TPInfo2 info = regions.remove(index);
			addRegions(info.start.getXn(), info.end.getXn(), p.getXn());
		}
	}
	public boolean isInRegion(NPoint start1, NPoint end1, NPoint p) {
		boolean flag = true;
		double[] pxn = p.getXn();
		double[] start = start1.getXn();
		double[] end = end1.getXn();
		for (int i = 0; i < this.dimension; i++) {
			if (pxn[i] < start[i] || pxn[i] > end[i]) {
				flag = false;
			}
		}
		return flag;
	}
	@Override
	public NPoint generateNextTC() {
		NPoint p=null;
		
		int temp=0;
		if(tests.size()==0){
			p=randomCreator.randomPoint();
			tests.add(p);
			updateRegions(p, temp);
		}else{
			C = 1.0 / calEachRegion();
			// 确定在哪一个区域
			double T = random.nextDouble();
			double PreIntegral = 0.0;
			double SumIntegral = 0.0;// 积分值总和
			// int temp = 0;// 落在哪个区间
			temp = 0;
			for (int i = 0; i < regions.size(); i++) {
				if (SumIntegral < T) {
					PreIntegral = SumIntegral;
					temp = i;
				}
				SumIntegral += regions.get(i).probality * C;
			}

			p = null;
			p = genNextTestCase(temp, PreIntegral);
			tests.add(p);
			updateRegions(p, temp);
		}
		return p;
	}
	
	public void time() {
		int count = 0;
		NPoint p = randomCreator.randomPoint();
		int temp = 0;
		while (count <= tcCount) {
			count++;
			tests.add(p);
			updateRegions(p, temp);
			C = 1.0 / calEachRegion();
			// 确定在哪一个区域
			double T = random.nextDouble();
			double PreIntegral = 0.0;
			double SumIntegral = 0.0;// 积分值总和
			temp = 0;
			for (int i = 0; i < regions.size(); i++) {
				if (SumIntegral < T) {
					PreIntegral = SumIntegral;
					temp = i;
				}
				SumIntegral += regions.get(i).probality * C;
			}

			p = null;
			p = genNextTestCase(temp, PreIntegral);
		}
	}
	public static double testFm() {
		int d = 2;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.002;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_TP_ND_COV rt = new ART_TP_ND_COV(min, max, failurePattern, new Random(i * 3 + 3));
			temp = rt.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		double fm = sums / (double) times;
		System.out.println("fm:" + fm + " time:" + ((endTime - startTime) / (double) times));
		return fm;
	}
	public static double testTCTime(int d, int tcCount) {
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 1;

		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_TP_ND_COV rt = new ART_TP_ND_COV(min, max, failurePattern, new Random(i * 3 + 3));
			rt.tcCount = tcCount;
			rt.time2();
		}
		long endTime = System.currentTimeMillis();
		System.out.println((endTime - startTime) / (double) times);
		return ((endTime - startTime) / (double) times);
	}

	public static double[] testEm(int dimension, double failrate) {
		int d = dimension;
		int emTime = 6;
		double[] result = new double[emTime];
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;
		int kk = 10;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = failrate;
		for (int k = 0; k < emTime; k++) {
			long sums = 0;
			long startTime = System.currentTimeMillis();
			for (int i = 0; i < times; i++) {
				ART_TP_ND_COV rt = new ART_TP_ND_COV(min, max, failurePattern, new Random(i * 3 + 3));
				rt.emCount = (k + 1) * 500;
				temp = rt.em();
				sums += temp;
			}
			long endTime = System.currentTimeMillis();
			double em = sums / (double) times;
			result[k] = em;
			System.out.println("em:" + em + " time:" + ((endTime - startTime) / (double) times));
		}
		System.out.println();
		return result;
	}
}
