package test.simulations.art_tp._ND;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.failurepattern.FailurePattern;
import test.ART;

//TODO 完成ART_TP_E
public class ART_TP_E_ND extends ART{

	double R;
	double Co;
	List<NPoint> tests=new ArrayList<>();
	public ART_TP_E_ND(double[] min, double[] max, Random random, FailurePattern failurePattern,double r) {
		super(min, max, random, failurePattern);
		this.R=r;
		
	}

	@Override
	public int run() {
		int count=0;
		NPoint p=randomCreator.randomPoint();
		while(this.failPattern.isCorrect(p)){
			//
			count++;
			this.tests.add(p);
			
			Co=calCDF();
			Co=1.0/Co;
			//next 
			p=genNextTC();
			
		}
		
		return count;
	}
	private double calCDF(){
		double temp=0.0;
		//
		return temp;
	}
	private NPoint genNextTC(){
		NPoint p=null;
		return p;
	}
	public static void main(String[] args) {
		
	}

	@Override
	public int em() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NPoint generateNextTC() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
