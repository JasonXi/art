package test.simulations.art_tp._ND;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import util.PaiLie;
import util.X3;
import util.data.ZeroOneCreator;

/*
 * 	// generate PDF in this max region
	// PDF=C*(x1-u1)*(v1-x1)*(x2-u2)*(v2-x2)*....*(xn-un)*(vn-xn)
	// first we need to get u1...un and v1...vn and
	// int(from[],to[])（积分上下限）
	// second calculate each dimension integral value
	// C=1.0/(intValueX1*intValueX2*...*intValueXn)
	// 生成每一维的边缘分布，fx1=C*IntValue2*...*intValueXn*(x1-u1)*(v1-x1) ....
	// int(fxk,from[k],xk)=T (T is a random Number from 0-1 )
	// set p=([x1,x2...,xn])
 * **/
public class ART_TP_RP_ND extends ART {
	public static void main(String[] args) {
		// 一定要修改这个
		int d = 4;
		// m 维立方体

		ZeroOneCreator dataCreator = new ZeroOneCreator();

		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 3000;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.01;
		for (int i = 0; i < times; i++) {
			ART_TP_RP_ND art_TP_RP_ND = new ART_TP_RP_ND(min, max, failurePattern, new Random(i * 3));
			int fm = art_TP_RP_ND.run();
			sums += fm;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	ArrayList<NRectRegion> regions = new ArrayList<>();

	ArrayList<NPoint> tests = new ArrayList<>();

	public ART_TP_RP_ND(double[] min, double[] max, FailurePattern pattern, Random random) {
		super(min, max, random, pattern);
	}

	public double calAllIntEC(double u[], double v[], double from[], double to[]) {
		double value = 0;
		for (int i = 0; i < u.length; i++) {
			if (i == 0) {
				value = calEachIntEC(u[i], v[i], from[i], to[i]);
			} else
				value *= calEachIntEC(u[i], v[i], from[i], to[i]);
		}
		return value;
	}

	// public void genFail_start() {
	// NPoint start = initRegion.getStart();
	// NPoint end = initRegion.getEnd();
	// double fail_size = initRegion.size() * fail_rate;
	// double each_rate = Math.pow(fail_size, 1.0 / (double) dimension);
	// ///
	// for (int i = 0; i < fail_start.length; i++) {
	// fail_start[i] = random.nextDouble() * (end.getXn()[i] - start.getXn()[i] -
	// each_rate) + start.getXn()[i];
	//// System.out.println("fail_start" + i + " (" + fail_start[i] + "," +
	// (fail_start[i] + each_rate) + ")");
	// }
	// }

	// public NPoint genFirstTC() {
	// NPoint p = new NPoint();
	// NPoint start = initRegion.getStart();
	// NPoint end = initRegion.getEnd();
	// double xn[] = new double[dimension];
	// for (int i = 0; i < xn.length; i++) {
	// xn[i] = random.nextDouble() * (end.getXn()[i] - start.getXn()[i]) +
	// start.getXn()[i];
	// }
	// p.setXn(xn);
	// return p;
	// }

	public double calEachIntEC(double u, double v, double f, double t) {
		// int((x-u)*(v-x),f,t)
		return -(1.0 / 3.0) * (t * t * t - f * f * f) + (0.5 * (u + v) * (t * t - f * f)) - u * v * (t - f);
	}

	public double calEachIntEC2(double s, double e, double f, double t) {
		return (-1.0 / 6.0) * (e - s)
				* (e * (-3 * f + 2 * s - 3 * t) - 3 * f * s + 6 * f * t + 2 * s * s - 3 * s * t + 2 * e * e);
	}

	public int findMaxRegion() {
		double maxsize = 0;
		int maxregion_index = 0;
		for (int i = 0; i < regions.size(); i++) {
			NRectRegion temp = regions.get(i);
			if (temp.size() > maxsize) {
				maxsize = temp.size();
				maxregion_index = i;
			}
		}
		return maxregion_index;
	}

	// public boolean isCorrect(NPoint p) {
	// double xn[] = p.getXn();
	// double fail_size = initRegion.size() * fail_rate;
	// double each_rate = Math.pow(fail_size, 1.0 / (double) dimension);
	// boolean flag = true;
	// for (int i = 0; i < xn.length; i++) {
	// if ((xn[i] < fail_start[i]) || (xn[i] > fail_start[i] + each_rate)) {
	// flag = false;
	// }
	// }
	// return !flag;
	// }

	public double[] genEachIntEC(double u[], double v[], double from[], double to[]) {
		double results[] = new double[u.length];
		for (int i = 0; i < u.length; i++) {
			results[i] = calEachIntEC(u[i], v[i], from[i], to[i]);
		}
		return results;
	}

	public double[] genEachNext(double u[], double v[], double from[], double to[], double[] eachIntValueEC) {
		double eachDimension[] = new double[u.length];
		for (int i = 0; i < u.length; i++) {
			double T = random.nextDouble();
			double Co = 1.0 / eachIntValueEC[i];
			// System.out.println("Co:"+Co);
			// int(CO*(x-u1)*(v1-x),from,x)=T
			// Co*{(-1.0/3.0)*(x^3)+0.5*(u+v)*x^2-u*v*x}(from.x)=T
			double A = -(1.0 / 3.0) * Co;
			double B = 0.5 * (u[i] + v[i]) * Co;
			double C = -u[i] * v[i] * Co;
			double D = -Co * (-(1.0 / 3.0) * from[i] * from[i] * from[i] + 0.5 * (u[i] + v[i]) * from[i] * from[i]
					- u[i] * v[i] * from[i]) - T;
			double results[] = X3.shengjinFormula(A, B, C, D);
			boolean flag = true;
			for (int j = 0; j < results.length; j++) {
				if (results[j] > from[i] && results[j] < to[i]) {
					eachDimension[i] = results[j];
					flag = false;
					break;
				}
			}
			if (flag) {
				System.out.println("genEachNext(" + i + "th dimension) failed");
			}
		}
		return eachDimension;
	}

	public double[] genUorV(NRectRegion region, String flag) {
		double[] results = null;
		double e1[] = minAllTC();
		double en[] = maxAllTC();
		if (flag.equalsIgnoreCase("u")) {
			// double starts[] = initRegion.getStart().getXn();
			results = Arrays.copyOf(region.getStart().getXn(), region.getStart().getXn().length);
			for (int i = 0; i < results.length; i++) {
				if (results[i] == min[i]) {
					results[i] = random.nextDouble() * (e1[i] - min[i]) + (2 * min[i] - e1[i]);
				}
			}
		} else if (flag.equalsIgnoreCase("v")) {
			results = Arrays.copyOf(region.getEnd().getXn(), region.getEnd().getXn().length);
			// double ends[] = initRegion.getEnd().getXn();
			for (int i = 0; i < results.length; i++) {
				if (results[i] == max[i]) {
					results[i] = random.nextDouble() * (max[i] - en[i]) + max[i];
					// System.out.println("endsi:"+ends[i]+" "+ en.getXn()[i]);
				}
			}
		} else {
			System.out.println("genUorV error (not u or v)");
		}
		return results;
	}

	public double[] maxAllTC() {
		double[][] results = new double[tests.size()][dimension];
		// 得到每一列的值
		for (int i = 0; i < tests.size(); i++) {
			for (int j = 0; j < tests.get(i).getXn().length; j++) {
				results[i][j] = tests.get(i).getXn()[j];
			}
		}
		double[] max = results[0];
		for (int i = 0; i < results.length; i++) {
			for (int j = 0; j < results[i].length; j++) {
				if (max[j] < results[i][j]) {
					max[j] = results[i][j];
				}
			}
		}
		return max;
	}

	public double[] minAllTC() {
		double[][] results = new double[tests.size()][dimension];
		// 得到每一列的值
		for (int i = 0; i < tests.size(); i++) {
			for (int j = 0; j < tests.get(i).getXn().length; j++) {
				results[i][j] = tests.get(i).getXn()[j];
			}
		}
		double[] min = results[0];
		for (int i = 0; i < results.length; i++) {
			for (int j = 0; j < results[i].length; j++) {
				if (min[j] > results[i][j]) {
					min[j] = results[i][j];
				}
			}
		}
		return min;
	}

	public int run() {
		int count = 0;
		// 设置最大区域

		// 添加初始区域
		NRectRegion initRegion = new NRectRegion();
		initRegion.setStart(new NPoint(min));
		initRegion.setEnd(new NPoint(max));
		regions.add(initRegion);
		// first test case
		NPoint p = randomCreator.randomPoint();
		if (!this.failPattern.isCorrect(p)) {
			return count;
		}
		tests.add(p);
		updateRegions(0, p);

		boolean flag = true;
		while (flag) {
			count++;
			// System.out.println("----------------------");
			// find the largest region
			int indexOfLargest = findMaxRegion();
			NRectRegion maxregion = regions.get(indexOfLargest);
			// System.out.println("max region:" + maxregion);

			double u[] = genUorV(maxregion, "U");
			double v[] = genUorV(maxregion, "V");
			// double u[] = maxregion.getStart().getXn();
			// double v[] = maxregion.getEnd().getXn();
			// System.out.println(maxregion);
			// 积分上下限就是max region的边界
			double from[] = maxregion.getStart().getXn();
			double to[] = maxregion.getEnd().getXn();
			// 计算每一维的积分值
			double eachIntValueEC[] = genEachIntEC(u, v, from, to);
			// double C = 1.0 / calAllIntEC(u, v, from, to);//没什么用处。。。
			// 每一维的边缘密度函数 fx1=1.0/(intValueX1)*(x1-u1)*(v1-x1)
			double results[] = genEachNext(u, v, from, to, eachIntValueEC);
			// new NPoint
			p = new NPoint();
			p.setXn(results);

			if (!this.failPattern.isCorrect(p)) {
				flag = false;
			}

			updateRegions(indexOfLargest, p);
		}
		return count;
	}

	public void updateRegions(int IndexOfMaxRegion, NPoint p) {
		NRectRegion maxregion = regions.get(IndexOfMaxRegion);
		regions.remove(IndexOfMaxRegion);
		/// update new regions
		int quyus = (int) Math.pow(2, dimension);
		ArrayList<double[]> unFormedRegion = PaiLie.GetAll(maxregion.getStart().getXn(), maxregion.getEnd().getXn());
		// System.out.println("new P:"+p);
		ArrayList<List<double[]>> newRegionArea = PaiLie.reOrder(p.getXn(), unFormedRegion);
		for (int i = 0; i < quyus; i++) {
			NPoint start = new NPoint(newRegionArea.get(0).get(i));
			NPoint end = new NPoint(newRegionArea.get(1).get(i));
			NRectRegion region = new NRectRegion(start, end);
			// System.out.println("regions:" + region);
			regions.add(region);
		}
	}

	@Override
	public int em() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public NPoint generateNextTC() {
		// TODO Auto-generated method stub
		return null;
	}
}
