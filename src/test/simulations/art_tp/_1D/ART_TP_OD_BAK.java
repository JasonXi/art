package test.simulations.art_tp._1D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

public class ART_TP_OD_BAK {
	public static void main(String[] args) throws Exception {
		double fail_rate = 0.01;
		int times = 300;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			// 暂不支持测试其他类，没有考虑清楚
			ART_TP_OD_BAK art_tp_od = new ART_TP_OD_BAK(0, 1, fail_rate, i * 3);
			int fm = art_tp_od.run();
			sums += fm;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	int seedOfRandom;
	double min;
	double max;
	double fail_rate;
	double fail_start;
	Random random = null;
	double An;
	double Bn;

	ArrayList<TestCase> tests = new ArrayList<>();

	// 构造函数
	public ART_TP_OD_BAK(double min, double max, double fail_rate, int seedOfRandom) {
		this.seedOfRandom = seedOfRandom;
		this.min = min;
		this.max = max;
		this.fail_rate = fail_rate;
		random = new Random(this.seedOfRandom);
	}

	public double genFail_start() {
		double size = (max - min) * fail_rate;
		return random.nextDouble() * (max - size);
	}

	public TestCase genNext(ArrayList<Double> integrals, double Co, TestCase p, long sed) {
		random.setSeed(seedOfRandom);
		double e1 = tests.get(0).p;// first node
		double en = tests.get(tests.size() - 1).p;// last node
		// 随机生成一个0-1的数
		double T = random.nextDouble();
		// System.out.println("An:"+An+",Bn:"+Bn+",Co:"+Co+",T:"+T);
		// 看T落在哪个区间
		double SumIntegral = 0.0;// 积分值总和
		double PreIntegral = 0.0;
		int temp = 0;
		for (int i = 0; i < integrals.size(); i++) {
			if (SumIntegral < T) {
				PreIntegral = SumIntegral;
				temp = i;
			}
			SumIntegral += integrals.get(i) * Co;
		}
		//
		double A, B, C, D;
		if (temp == 0) {
			A = -Co / 3.0;
			B = (Co / 2.0) * (e1 + An);
			C = -Co * e1 * An;
			D = -T;
		} else if (temp == integrals.size() - 1) {
			A = -Co / 3.0;
			B = (Co / 2.0) * (en + Bn);
			C = -Co * (Bn) * (en);
			D = PreIntegral - T - Co * ((1.0 / 6.0) * (Math.pow(en, 3.0)))
					+ Co * ((1.0 / 2.0) * Bn * Math.pow(en, 2.0));
		} else {
			A = -Co / 3.0;
			B = (Co / 2.0) * (tests.get(temp - 1).p + tests.get(temp).p);
			C = -Co * tests.get(temp - 1).p * tests.get(temp).p;
			D = -T + PreIntegral - Co * ((1.0 / 6.0) * (Math.pow(tests.get(temp - 1).p, 3.0))
					- (1.0 / 2.0) * (tests.get(temp).p) * (Math.pow(tests.get(temp - 1).p, 2.0)));
		}
		double[] roots = shengjinFormula(A, B, C, D);
		boolean haveAanswer = false;
		for (double root : roots) {
			if (temp == 0) {
				if (root >= 0.0 && root <= e1) {
					p = new TestCase();
					p.p = root;
					haveAanswer = true;
				}
			} else if (temp == integrals.size() - 1) {
				if (root >= en && root <= 1.0) {
					p = new TestCase();
					p.p = root;
					haveAanswer = true;
				}
			} else {
				if (root >= tests.get(temp - 1).p && root <= tests.get(temp).p) {
					p = new TestCase();
					p.p = root;
					haveAanswer = true;
				}
			}
		}
		if (!haveAanswer) {
			return null;
		} else {
			return p;
		}
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public int run() {
		fail_start = genFail_start();
		TestCase p = new TestCase();
		p.p = random.nextDouble() * (max - min) + min;
		int count = 0;
		while (isCorrect(p.p)) {
			count++;
			if (tests.size() == 0) {
				tests.add(p);
			} else {
				sortTestCases(p);
			}
			/////////////////////
			// 生成概率分布函数
			// An->(-e1,0) Bn->(1,2-en)
			double e1 = tests.get(0).p;// first node
			double en = tests.get(tests.size() - 1).p;// last node
			// calculate An and Bn
			// An (min-(e1-min)) to min |||| Bn max to max+max-en
			An = random.nextDouble() * (e1 - min) + (2 * min - e1);
			Bn = random.nextDouble() * (max - en) + max;
			// 计算系数
			double Co = 0.0;
			ArrayList<Double> integrals = new ArrayList<>();
			for (int i = 0; i < tests.size() + 1; i++) {
				if (i == 0) {
					// 1/6*e1^3-1/2*an*e1^2
					double temp = ((1.0 / 6.0) * (Math.pow(e1, 3))) - ((1.0 / 2.0) * An * (Math.pow(e1, 2)));
					Co += temp;
					integrals.add(temp);
				} else if (i == tests.size()) {
					// (-1/6)en^3+(1/2)*bn*en^2-en*bn+(1/2)*(bn+en)-(1/3)
					double temp = (-1.0 / 6.0) * Math.pow(en, 3.0) + (1.0 / 2.0) * (Bn) * Math.pow(en, 2.0) - en * Bn
							+ (1.0 / 2.0) * (Bn + en) - (1.0 / 3.0);
					Co += temp;
					integrals.add(temp);
				} else {
					double ei_1 = tests.get(i - 1).p;
					double ei = tests.get(i).p;
					// (1/6)*(ei1^3-ei^3)+(1/2)*(ei*ei1)*(ei-ei1)
					double temp = (1.0 / 6.0) * (Math.pow(ei, 3.0) - Math.pow(ei_1, 3))
							+ (1.0 / 2.0) * (ei_1 * ei) * (ei_1 - ei);
					Co += temp;
					integrals.add(temp);
				}
			}
			Co = 1.0 / Co;
			p = genNext(integrals, Co, p, seedOfRandom);
			int countErr = 0;
			while (p == null) {
				p = genNext(integrals, Co, p, System.currentTimeMillis());
				// System.out.println("error:"+countErr++);
			}
		}
		return count;
	}

	public double[] shengjinFormula(double acof, double bcof, double cof, double dof) {
		double A = bcof * bcof - 3.0 * acof * cof;// A=b^2-3ac
		double B = bcof * cof - 9.0 * acof * dof;// B=bc-9ad
		double C = cof * cof - 3.0 * bcof * dof;// C=c^2-3bd
		double delta = B * B - 4.0 * A * C;
		double root = 0.0;
		double r1 = 0.0;
		double r2 = 0.0;
		double[] roots = new double[3];
		if (delta > 0) {
			double Y1 = A * bcof + 3.0 * acof * (-B + Math.sqrt(B * B - 4.0 * A * C)) / 2.0;
			double Y2 = A * bcof + 3.0 * acof * (-B - Math.sqrt(B * B - 4.0 * A * C)) / 2.0;
			double powY1;
			double powY2;
			if (Y1 < 0) {
				powY1 = -Math.pow(-Y1, 1.0 / 3.0);
			} else {
				powY1 = Math.pow(Y1, 1.0 / 3.0);
			}
			if (Y2 < 0) {
				powY2 = -Math.pow(-Y2, 1.0 / 3.0);
			} else {
				powY2 = Math.pow(Y2, 1.0 / 3.0);
			}
			root = (-bcof - powY1 - powY2) / (3.0 * acof);
			r1 = root;
			r2 = root;
		} else if (delta == 0) {
			root = -bcof / acof + B / A;
			r1 = -B / (2.0 * A);
			r2 = r1;

		} else if (delta < 0) {
			double T = (2.0 * A * bcof - 3.0 * acof * B) / (2.0 * Math.pow(A, 3.0 / 2.0));
			double theta = Math.acos(T);
			root = (-bcof - 2.0 * Math.sqrt(A) * Math.cos(theta / 3.0)) / (3.0 * acof);
			r1 = (-bcof + Math.sqrt(A) * (Math.cos(theta / 3.0) + Math.sqrt(3.0) * Math.sin(theta / 3.0)))
					/ (3.0 * acof);
			r2 = (-bcof + Math.sqrt(A) * (Math.cos(theta / 3.0) - Math.sqrt(3.0) * Math.sin(theta / 3.0)))
					/ (3.0 * acof);
		}
		roots[0] = root;
		roots[1] = r1;
		roots[2] = r2;
		return roots;
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}
}
