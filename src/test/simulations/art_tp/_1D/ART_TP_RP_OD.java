package test.simulations.art_tp._1D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

public class ART_TP_RP_OD {
	public static void main(String[] args) throws Exception {
		double fail_rate = 0.005;
		int times = 3000;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			// 暂不支持测试其他类，没有考虑清楚
			ART_TP_RP_OD art_tp_od = new ART_TP_RP_OD(0, 1, fail_rate, i * 3);
			int fm = art_tp_od.run();
			sums += fm;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	int seedOfRandom;
	double min;
	double max;
	double fail_rate;
	double fail_start;
	Random Trandom;

	ArrayList<TestCase> tests = new ArrayList<>();

	// 构造函数
	public ART_TP_RP_OD(double min, double max, double fail_rate, int seedOfRandom) {
		this.seedOfRandom = seedOfRandom;
		this.min = min;
		this.max = max;
		this.fail_rate = fail_rate;
		Trandom = new Random(seedOfRandom);
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public int run() {
		Random random = new Random(seedOfRandom);
		fail_start = random.nextDouble() * (1 - fail_rate);
		TestCase p = new TestCase();
		p.p = random.nextDouble() * (max - min) + min;
		System.out.println("p.p:" + p.p);
		int count = 0;
		while (isCorrect(p.p)) {
			count++;
			if (tests.size() == 0) {
				tests.add(p);
			} else {
				sortTestCases(p);
			}
			/////////////////////
			// 生成概率分布函数
			double e1 = tests.get(0).p;// first node
			double en = tests.get(tests.size() - 1).p;// last node
			// calculate An and Bn
			// An (min-(e1-min)) to min |||| Bn max to max+max-en
			double Bn = random.nextDouble() * (max - en) + max;
			double An = random.nextDouble() * (e1 - min) + (2 * min - e1);
			// double Bn = random.nextDouble() * (max - en) + max;
			// 计算系数
			double Co = 0.0;
			double start = An;
			double end = 0;
			// from to 積分的上下限
			double from = min;
			double to = 0;
			double maxdistance = e1 - min;
			end = e1;
			to = e1;
			for (int i = 0; i < tests.size() - 1; i++) {
				double distance = tests.get(i + 1).p - tests.get(i).p;
				if (distance > maxdistance) {
					maxdistance = distance;
					start = tests.get(i).p;
					end = tests.get(i + 1).p;
					from = tests.get(i).p;
					to = tests.get(i + 1).p;
				}
			}
			if (maxdistance < (max - en)) {
				start = en;
				end = Bn;
				from = en;
				to = max;
			}
			// System.out.println("start:"+start+" end:"+end+" from:"+from+" to:"+to);
			// calculate by wolf
			Co = (1.0 / 3.0) * (from * from * from - to * to * to) + (0.5 * (start + end)) * (to * to - from * from)
					+ start * end * (from - to);
			// System.out.println(start+" "+end+" "+from+" "+to);
			// System.out.println("fx int:"+Co);
			Co = 1.0 / Co;
			// 随机生成一个0-1的数
			double T = Trandom.nextDouble();
			// System.out.println("T:"+T);
			double A, B, C, D;
			A = -Co / 3.0;
			B = Co * (start + end) / 2.0;
			C = -Co * start * end;
			D = -Co * ((-1.0 / 3.0) * from * from * from + 0.5 * (start + end) * from * from - start * end * from) - T;
			// System.out.println("start:"+An);
			// System.out.println("end:"+Bn);
			// System.out.println("Co:"+Co);
			// System.out.println("A:"+A+" B:"+B+" C:"+C+" D:"+D);
			double[] roots = shengjinFormula(A, B, C, D);
			boolean haveAanswer = false;
			for (double root : roots) {
				if (root >= from && root <= to) {
					p = new TestCase();
					p.p = root;
					System.out.println("---------------------");
					System.out.println("p" + count + ":" + p.p);
					haveAanswer = true;
				}
			}
			if (!haveAanswer) {
				System.out.println("error " + roots[0] + " " + roots[1] + " " + roots[2]);
				System.out.println(from + " " + to);
			}
		}
		return count;
	}

	public double[] shengjinFormula(double acof, double bcof, double cof, double dof) {
		double A = bcof * bcof - 3.0 * acof * cof;// A=b^2-3ac
		double B = bcof * cof - 9.0 * acof * dof;// B=bc-9ad
		double C = cof * cof - 3.0 * bcof * dof;// C=c^2-3bd
		double delta = B * B - 4.0 * A * C;
		double root = 0.0;
		double r1 = 0.0;
		double r2 = 0.0;
		double[] roots = new double[3];
		if (delta > 0) {
			double Y1 = A * bcof + 3.0 * acof * (-B + Math.sqrt(B * B - 4.0 * A * C)) / 2.0;
			double Y2 = A * bcof + 3.0 * acof * (-B - Math.sqrt(B * B - 4.0 * A * C)) / 2.0;
			double powY1;
			double powY2;
			if (Y1 < 0) {
				powY1 = -Math.pow(-Y1, 1.0 / 3.0);
			} else {
				powY1 = Math.pow(Y1, 1.0 / 3.0);
			}
			if (Y2 < 0) {
				powY2 = -Math.pow(-Y2, 1.0 / 3.0);
			} else {
				powY2 = Math.pow(Y2, 1.0 / 3.0);
			}
			root = (-bcof - powY1 - powY2) / (3.0 * acof);
			r1 = root;
			r2 = root;
		} else if (delta == 0) {
			root = -bcof / acof + B / A;
			r1 = -B / (2.0 * A);
			r2 = r1;

		} else if (delta < 0) {
			double T = (2.0 * A * bcof - 3.0 * acof * B) / (2.0 * Math.pow(A, 3.0 / 2.0));
			double theta = Math.acos(T);
			root = (-bcof - 2.0 * Math.sqrt(A) * Math.cos(theta / 3.0)) / (3.0 * acof);
			r1 = (-bcof + Math.sqrt(A) * (Math.cos(theta / 3.0) + Math.sqrt(3.0) * Math.sin(theta / 3.0)))
					/ (3.0 * acof);
			r2 = (-bcof + Math.sqrt(A) * (Math.cos(theta / 3.0) - Math.sqrt(3.0) * Math.sin(theta / 3.0)))
					/ (3.0 * acof);
		}
		roots[0] = root;
		roots[1] = r1;
		roots[2] = r2;
		return roots;
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}
}
