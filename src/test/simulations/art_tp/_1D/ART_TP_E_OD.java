package test.simulations.art_tp._1D;

import java.util.ArrayList;
import java.util.Random;

import datastructure.TD.TestCase;

/**
 * ART_ETP（FROM SPE）
 */
public class ART_TP_E_OD {
	public static void main(String[] args) throws Exception {
		int times = 3000;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			// 暂不支持测试其他类，没有考虑清楚
			ART_TP_E_OD art_tp_e_od = new ART_TP_E_OD(0, 1, 0.75, 0.001, i * 9);
			int fm = art_tp_e_od.run();
			sums += fm;
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Fm: " + sums / (double) times);
		System.out.println("Time: " + (endTime - startTime) / (double) times);
	}
	int seedOfRandom;
	double min;
	double max;
	double R;
	double fail_rate;
	double fail_start;

	ArrayList<TestCase> tests = new ArrayList<>();

	// 构造函数
	public ART_TP_E_OD(double min, double max, double R, double fail_rate, int seedOfRandom) {
		this.seedOfRandom = seedOfRandom;
		this.min = min;
		this.max = max;
		this.R = R;
		this.fail_rate = fail_rate;
	}

	public boolean isCorrect(double p) {
		if (p > fail_start && p < (fail_start + fail_rate)) {
			return false;
		} else {
			return true;
		}
	}

	public int run() {
		Random random = new Random(seedOfRandom);
		fail_start = random.nextDouble() * (max - min - fail_rate);
		TestCase p = new TestCase();
		p.p = random.nextDouble() * (max - min) + min;
		int count = 0;
		while (isCorrect(p.p)) {
			count++;
			if (tests.size() == 0) {
				tests.add(p);
			} else {
				sortTestCases(p);
			}
			/////////////////////
			// 生成概率分布函数
			// An->(-e1,0) Bn->(1,2-en)
			double e1 = tests.get(0).p;// first node
			double en = tests.get(tests.size() - 1).p;// last node
			double An = random.nextDouble() * (e1 - min) + (2 * min - e1);
			double Bn = random.nextDouble() * (max - en) + max;
			double radius = R / (2 * tests.size());
			// System.out.println("An:"+An+" Bn:"+Bn);
			// 计算系数
			double Co = 0.0;
			ArrayList<double[]> integrals = new ArrayList<>();
			double[] informations = null;
			for (int i = 0; i < tests.size() + 1; i++) {
				if (i == 0) {
					if (e1 - radius > min) {
						double from = min;
						double to = e1 - radius;
						// calculate by wolf
						double temp = (-An * from * from / 2.0) + (An * from * e1) - (An * e1 * to)
								+ (An * to * to / 2.0) + (from * from * from / 3.0) - (from * from * e1 / 2.0)
								+ (e1 * to * to / 2.0) - (to * to * to / 3.0);
						Co += temp;
						informations = new double[6];
						informations[0] = temp;
						informations[1] = from;
						informations[2] = to;
						informations[3] = An;
						informations[4] = e1;
						informations[5] = 0;// 用不到
						integrals.add(informations);
					}
				} else if (i == tests.size()) {
					if (en + radius < max) {
						double from = en + radius;
						double to = max;
						// calculate by wolf
						double temp = (-Bn * from * from / 2.0) + (Bn * from * en) - (Bn * en * to)
								+ (Bn * to * to / 2.0) + (from * from * from / 3.0) - (from * from * en / 2.0)
								+ (en * to * to / 2.0) - (to * to * to / 3.0);
						Co += temp;
						informations = new double[6];
						informations[0] = temp;
						informations[1] = from;
						informations[2] = to;
						informations[3] = en;
						informations[4] = Bn;
						informations[5] = 0;// 用不到
						integrals.add(informations);
					}
				} else {
					double ei_1 = tests.get(i - 1).p;
					double ei = tests.get(i).p;
					if (ei - ei_1 > 2 * radius) {
						double from = ei_1 + radius;
						double to = ei - radius;
						// calculate by wolf
						double temp = (-ei * from * from / 2.0) + (ei * from * ei_1) - (ei * ei_1 * to)
								+ (ei * to * to / 2.0) + (from * from * from / 3.0) - (from * from * ei_1 / 2.0)
								+ (ei_1 * to * to / 2.0) - (to * to * to / 3.0);
						Co += temp;
						informations = new double[6];
						informations[0] = temp;
						informations[1] = from;
						informations[2] = to;
						informations[3] = ei_1;
						informations[4] = ei;
						informations[5] = 0;// 用不到
						integrals.add(informations);
					}
				}
			}
			Co = 1.0 / Co;

			// 随机生成一个0-1的数
			double T = random.nextDouble();
			// 看T落在哪个区间
			double SumIntegral = 0.0;// 积分值总和
			double PreIntegral = 0.0;
			int temp = 0;
			for (int i = 0; i < integrals.size(); i++) {
				if (SumIntegral < T) {
					PreIntegral = SumIntegral;
					temp = i;
				}
				SumIntegral += integrals.get(i)[0] * Co;
			}
			//
			double A, B, C, D;
			double start = integrals.get(temp)[1];
			double end = integrals.get(temp)[2];
			double m = integrals.get(temp)[3];
			double n = integrals.get(temp)[4];
			A = -Co / 3.0;
			B = Co * (m + n) / 2.0;
			C = -Co * m * n;
			D = PreIntegral - T
					- Co * ((-start * start * start / 3.0) + ((m + n) * start * start / 2.0) - m * n * start);
			double[] roots = shengjinFormula(A, B, C, D);
			boolean haveAanswer = false;
			for (double root : roots) {
				if (root >= start && root <= end) {
					p = new TestCase();
					p.p = root;
					haveAanswer = true;
				}
			}
			if (!haveAanswer) {
				System.out.println("error ");
			}
		}
		return count;
	}

	public double[] shengjinFormula(double acof, double bcof, double cof, double dof) {
		double A = bcof * bcof - 3.0 * acof * cof;// A=b^2-3ac
		double B = bcof * cof - 9.0 * acof * dof;// B=bc-9ad
		double C = cof * cof - 3.0 * bcof * dof;// C=c^2-3bd
		double delta = B * B - 4.0 * A * C;
		double root = 0.0;
		double r1 = 0.0;
		double r2 = 0.0;
		double[] roots = new double[3];
		if (delta > 0) {
			double Y1 = A * bcof + 3.0 * acof * (-B + Math.sqrt(B * B - 4.0 * A * C)) / 2.0;
			double Y2 = A * bcof + 3.0 * acof * (-B - Math.sqrt(B * B - 4.0 * A * C)) / 2.0;
			double powY1;
			double powY2;
			if (Y1 < 0) {
				powY1 = -Math.pow(-Y1, 1.0 / 3.0);
			} else {
				powY1 = Math.pow(Y1, 1.0 / 3.0);
			}
			if (Y2 < 0) {
				powY2 = -Math.pow(-Y2, 1.0 / 3.0);
			} else {
				powY2 = Math.pow(Y2, 1.0 / 3.0);
			}
			root = (-bcof - powY1 - powY2) / (3.0 * acof);
			r1 = root;
			r2 = root;
		} else if (delta == 0) {
			root = -bcof / acof + B / A;
			r1 = -B / (2.0 * A);
			r2 = r1;

		} else if (delta < 0) {
			double T = (2.0 * A * bcof - 3.0 * acof * B) / (2.0 * Math.pow(A, 3.0 / 2.0));
			double theta = Math.acos(T);
			root = (-bcof - 2.0 * Math.sqrt(A) * Math.cos(theta / 3.0)) / (3.0 * acof);
			r1 = (-bcof + Math.sqrt(A) * (Math.cos(theta / 3.0) + Math.sqrt(3.0) * Math.sin(theta / 3.0)))
					/ (3.0 * acof);
			r2 = (-bcof + Math.sqrt(A) * (Math.cos(theta / 3.0) - Math.sqrt(3.0) * Math.sin(theta / 3.0)))
					/ (3.0 * acof);
		}
		roots[0] = root;
		roots[1] = r1;
		roots[2] = r2;
		return roots;
	}

	public void sortTestCases(TestCase p) {
		int low = 0, high = tests.size() - 1, mid = -1;
		while (low <= high) {
			mid = (low + high) / 2;
			if (p.p > tests.get(mid).p) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}
		if (p.p < tests.get(mid).p) {
			mid = mid - 1;
		}
		tests.add(mid + 1, p);
	}

}
