package test.simulations.rt;

import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import datastructure.failurepattern.impl.PointPatternIn1D;
import test.simulations.rrt.RRT_ND;
import util.data.ZeroOneCreator;

public class RT_ND {
	public static void main(String[] args) {
		// testTCTime();
		double failrate = 0.001;
		int d = 3;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double[] min = dataCreator.minCreator(d);
		double[] max = dataCreator.maxCreator(d);
		FailurePattern failurePattern = new PointPatternIn1D();
		failurePattern.fail_rate = failrate;
		failurePattern.min = min;
		failurePattern.max = max;
		failurePattern.dimension = d;

		RT_ND rt = new RT_ND(min, max, new Random( 3), failurePattern);
		//rt.pmCount=69314;
		System.out.println(rt.pm());

	}

	public static void testFm() {
		int times = 3000;
		double failrates[] = { 0.005, 0.002, 0.001, 0.0015, 0.0001 };
		for (int j = 0; j < failrates.length; j++) {
			double failrate = failrates[j];
			int d = 3;
			ZeroOneCreator dataCreator = new ZeroOneCreator();
			double[] min = dataCreator.minCreator(d);
			double[] max = dataCreator.maxCreator(d);
			FailurePattern failurePattern = new PointPatternIn1D();
			failurePattern.fail_rate = failrate;
			failurePattern.min = min;
			failurePattern.max = max;
			failurePattern.dimension = d;

			int fm = 0;
			long startTime = System.currentTimeMillis();
			for (int i = 0; i < times; i++) {
				RT_ND rt = new RT_ND(min, max, new Random(i * 3), failurePattern);
				int temp = rt.run();
				fm += temp;
			}
			long endTime = System.currentTimeMillis();

			System.out.println("Fm:" + (fm / (double) times) + " times:" + ((endTime - startTime) / (double) times));
		}
	}

	public static double testTCTime(int d, int tcCount) {

		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 1;

		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			RT_ND rt = new RT_ND(min, max, new Random(i * 3), failurePattern);
			rt.tcCount = tcCount;
			rt.time();
		}
		long endTime = System.currentTimeMillis();

		return ((endTime - startTime) / (double) times);
	}

	public static double[] testEm(int dimension, double failrate) {
		int emTimes = 6;
		double[] result = new double[emTimes];

		int d = dimension;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;

		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = failrate;
		for (int k = 0; k < emTimes; k++) {
			long sums = 0;
			long startTime = System.currentTimeMillis();
			for (int i = 0; i < times; i++) {
				RT_ND rt = new RT_ND(min, max, new Random(), failurePattern);
				rt.emCount = (k + 1) * 500;
				temp = rt.em();
				sums += temp;
			}
			long endTime = System.currentTimeMillis();
			double em = sums / (double) times;
			System.out.println("em:" + em + " time:" + ((endTime - startTime) / (double) times));
			result[k] = em;
		}
		System.out.println();
		return result;
	}

	public int tcCount = 0;
	public int emCount = 0;
	public int pmCount=0;
	double[] min;
	double[] max;
	int dimension;
	FailurePattern failPattern;

	Random random;

	public RT_ND(double[] min, double[] max, Random random, FailurePattern pattern) {
		this.min = min;
		this.max = max;

		this.dimension = min.length;

		this.random = random;
		pattern.min = min;
		pattern.max = max;
		pattern.dimension = this.dimension;
		pattern.random = random;
		pattern.genFailurePattern();

		this.failPattern = pattern;
		
		this.pmCount=(int) Math.round(Math.log(0.5)/Math.log(1-failPattern.fail_rate)); //95% CI
	}

	public NPoint randomTC() {
		NPoint point = new NPoint();
		point.dimension = this.dimension;
		double[] xn = new double[this.dimension];
		for (int i = 0; i < xn.length; i++) {
			xn[i] = random.nextDouble() * (max[i] - min[i]) + min[i];
		}
		point.setXn(xn);
		return point;
	}

	public int run() {
		int count = 0;
		NPoint p = randomTC();
		while (this.failPattern.isCorrect(p)) {
			// while(count<=tcCount){
			count++;
			p = randomTC();
		}
		return count;
	}

	public int em() {
		int count = 0;
		int emTemp = 0;
		NPoint p = randomTC();
		while (count <= emCount) {
			if (!this.failPattern.isCorrect(p)) {
				emTemp++;
			}
			count++;
			p = new NPoint();
			p = randomTC();
		}
		return emTemp;
	}

	public NPoint generateNextTC() {
		NPoint p = randomTC();
		return p;
	}

	public void time() {
		int count = 0;
		NPoint p = generateNextTC();
		while (count < this.tcCount) {
			count++;
			p = generateNextTC();
		}
	}

	public double pm() {
		this.pmCount=(int) Math.round(Math.log(0.5)/Math.log(1-failPattern.fail_rate));
		return 1 - Math.pow((1 - failPattern.fail_rate), pmCount);
	}
}
