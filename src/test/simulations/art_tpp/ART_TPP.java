package test.simulations.art_tpp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import util.PaiLie;
import util.data.ZeroOneCreator;

public class ART_TPP extends ART {

	public static void main(String[] args) {
		// testEm(1, 0.01);
		//testTCTime(2,5000);
		//testFm();
	}

	ArrayList<NRectRegion> regionLists = new ArrayList<>();
	ArrayList<NPoint> tests = new ArrayList<>();

	int k = 10;

	NRectRegion currentRegion = null;// 执行到哪一个区域

	public ART_TPP(double[] min, double[] max, Random random, FailurePattern failurePattern, int k) {
		super(min, max, random, failurePattern);
		this.k = k;
		regionLists.add(new NRectRegion(new NPoint(min), new NPoint(max)));
	}

	public void addRegionsIn2D(NRectRegion region, NPoint p) {
		double xmin = region.getStart().getXn()[0];
		double ymin = region.getStart().getXn()[1];
		double xmax = region.getEnd().getXn()[0];
		double ymax = region.getEnd().getXn()[1];
		double pp = p.getXn()[0];
		double qq = p.getXn()[1];

		NRectRegion first = new NRectRegion(new NPoint(new double[] { xmin, ymin }),
				new NPoint(new double[] { pp, qq }));
		NRectRegion second = new NRectRegion(new NPoint(new double[] { pp, ymin }),
				new NPoint(new double[] { xmax, qq }));
		NRectRegion third = new NRectRegion(new NPoint(new double[] { pp, qq }),
				new NPoint(new double[] { xmax, ymax }));
		NRectRegion fourth = new NRectRegion(new NPoint(new double[] { xmin, qq }),
				new NPoint(new double[] { pp, ymax }));

		this.regionLists.add(first);
		this.regionLists.add(second);
		this.regionLists.add(third);
		this.regionLists.add(fourth);
	}

	public void addRegionsInND(NRectRegion region, NPoint p) throws Exception {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}

			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			this.regionLists.add(tempRegion);
		}
	}



	public int findMaxRegion() {
		double max = -1;
		int index = 0;
		for (int i = 0; i < regionLists.size(); i++) {
			double temp = regionLists.get(i).size();
			if (max < temp) {
				max = temp;
				index = i;
			}
		}
		return index;
	}

	public NPoint hasPointInRegion(NRectRegion region) {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		NPoint result = null;
		for (int i = 0; i < tests.size(); i++) {
			double[] p = tests.get(i).getXn();
			boolean isPIn = true;
			for (int j = 0; j < p.length; j++) {
				if (p[j] < start[j] || p[j] > end[j]) {
					isPIn = false;
				}
			}
			if (isPIn) {
				result = tests.get(i);
				break;
			}
		}
		return result;
	}



	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}

	boolean Isfirst = true;
	NPoint[] points = null;
	@Override
	public NPoint generateNextTC() {
		
		if (points == null) {
			points = generateNextTwoTC();
		}
		//System.out.println(points.length);
		if (points.length == 1) {
			NPoint temp=points[0];
			points=null;
			return temp;
		} else {
			if (Isfirst) {
				Isfirst = false;
				return points[0];
			} else {
				NPoint temp = points[1];
				Isfirst=true;
				points = null;
				return temp;
			}
		}
	}

	public NPoint[] generateNextTwoTC() {
		NPoint[] results = null;
		int maxIndex = findMaxRegion();
		currentRegion = regionLists.remove(maxIndex);
		NPoint alreadyPoint = hasPointInRegion(currentRegion);
		NPoint p1 = null;
		NPoint p2 = null;

		if (alreadyPoint == null) {
			p1 = randomCreator.randomPoint(currentRegion);
			tests.add(p1);

			NPoint[] SC = new NPoint[k];
			double maxLength = 0.0;

			for (int i = 0; i < k; i++) {
				SC[i] = randomCreator.randomPoint(currentRegion);
				double length = p1.distanceToAnotherPoint(SC[i]);
				if (maxLength < length) {
					maxLength = length;
					p2 = SC[i];
				}
			}
			results = new NPoint[2];
			results[0] = p1;
			results[1] = p2;
		} else {
			p1 = alreadyPoint;
			NPoint[] SC = new NPoint[k];
			double maxLength = 0.0;

			for (int i = 0; i < k; i++) {
				SC[i] = randomCreator.randomPoint(currentRegion);
				double length = p1.distanceToAnotherPoint(SC[i]);
				if (maxLength < length) {
					maxLength = length;
					p2 = SC[i];
				}
			}
			results = new NPoint[1];
			results[0] = p2;
		}
		tests.add(p2);
		// mid point
		NPoint midPoint = p1.midPointWithAnotherPoint(p2);
		// addRegionsIn2D(currentRegion, midPoint);
		try {
			addRegionsInND(currentRegion, midPoint);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return results;
	}

	
	public void time() {
		int count = 0;
		NRectRegion currentRegion = regionLists.get(0);
		NPoint p = randomCreator.randomPoint(currentRegion);
		tests.add(p);
		count++;
		while (count < tcCount) {
			int maxIndex = findMaxRegion();
			currentRegion = regionLists.remove(maxIndex);
			NPoint alreadyPoint = hasPointInRegion(currentRegion);
			NPoint p1 = null;
			NPoint p2 = null;

			if (alreadyPoint == null) {
				p1 = randomCreator.randomPoint(currentRegion);
				tests.add(p1);
				count++;
				if (count > tcCount) {
					break;
				}
				// randomly generate k candidate points in curReg, and store
				// them in SC;
				// SC is the test input candidate set
				// select the point from SC which is the farthest from P1 as the
				// second test
				// input P2;
				NPoint[] SC = new NPoint[k];
				double maxLength = 0.0;

				for (int i = 0; i < k; i++) {
					SC[i] = randomCreator.randomPoint(currentRegion);
					double length = p1.distanceToAnotherPoint(SC[i]);
					if (maxLength < length) {
						maxLength = length;
						p2 = SC[i];
					}
				}

			} else {
				p1 = alreadyPoint;
				NPoint[] SC = new NPoint[k];
				double maxLength = 0.0;

				for (int i = 0; i < k; i++) {
					SC[i] = randomCreator.randomPoint(currentRegion);
					double length = p1.distanceToAnotherPoint(SC[i]);
					if (maxLength < length) {
						maxLength = length;
						p2 = SC[i];
					}
				}
			}
			//
			tests.add(p2);
			count++;
			if (count > tcCount) {
				break;
			}
			// mid point
			NPoint midPoint = p1.midPointWithAnotherPoint(p2);
			try {
				addRegionsInND(currentRegion, midPoint);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static double testFm() {
		int d = 2;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.01;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_TPP rt = new ART_TPP(min, max, new Random(i * 3), failurePattern, 10);
			temp = rt.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		double fm = sums / (double) times;
		System.out.println("fm:" + fm + " time:" + ((endTime - startTime) / (double) times));
		return fm;
	}

	public static double[] testEm(int dimension, double failrate) {
		int d = dimension;
		int emTime = 6;
		double result[] = new double[emTime];
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;
		int kk = 10;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = failrate;
		for (int k = 0; k < emTime; k++) {
			long sums = 0;
			long startTime = System.currentTimeMillis();
			for (int i = 0; i < times; i++) {
				ART_TPP rt = new ART_TPP(min, max, new Random(i * 3), failurePattern, 10);
				rt.emCount = (k + 1) * 500;
				temp = rt.em();
				sums += temp;
			}
			long endTime = System.currentTimeMillis();
			double em = sums / (double) times;
			result[k] = em;
			System.out.println("em:" + em + " time:" + ((endTime - startTime) / (double) times));
		}
		System.out.println();
		return result;
	}

	public static double testTCTime(int d, int tcCount) {
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 1;

		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_TPP rt = new ART_TPP(min, max, new Random(i * 3), failurePattern, 10);
			rt.tcCount = tcCount;
			rt.time2();
		}
		long endTime = System.currentTimeMillis();
		System.out.println((endTime - startTime) / (double) times);
		return ((endTime - startTime) / (double) times);
	}

}
