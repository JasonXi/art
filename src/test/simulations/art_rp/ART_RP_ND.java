package test.simulations.art_rp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.ART;
import test.simulations.art_b.ART_B_ND;
import test.simulations.fscs.FSCS_ND;
import util.PaiLie;
import util.data.ZeroOneCreator;

public class ART_RP_ND extends ART {

	public static void main(String[] args) {
		// testEm(1, 0.01);
		 testTCTime(2,5000);
		//testFm();
	}

	public NRectRegion initRegion;
	ArrayList<NRectRegion> regions = new ArrayList<>();

	public ART_RP_ND(double[] min, double[] max, Random random, FailurePattern pattern) {
		super(min, max, random, pattern);
		// initRegion = region;
		NRectRegion initRegion = new NRectRegion();
		initRegion.setStart(new NPoint(min));
		initRegion.setEnd(new NPoint(max));
		this.initRegion = initRegion;
		regions.add(initRegion);
	}

	public void addRegionsIn2D(NRectRegion region, NPoint p) {
		double xmin = region.getStart().getXn()[0];
		double ymin = region.getStart().getXn()[1];
		double xmax = region.getEnd().getXn()[0];
		double ymax = region.getEnd().getXn()[1];
		double pp = p.getXn()[0];
		double qq = p.getXn()[1];

		NRectRegion first = new NRectRegion(new NPoint(new double[] { xmin, ymin }),
				new NPoint(new double[] { pp, qq }));
		NRectRegion second = new NRectRegion(new NPoint(new double[] { pp, ymin }),
				new NPoint(new double[] { xmax, qq }));
		NRectRegion third = new NRectRegion(new NPoint(new double[] { pp, qq }),
				new NPoint(new double[] { xmax, ymax }));
		NRectRegion fourth = new NRectRegion(new NPoint(new double[] { xmin, qq }),
				new NPoint(new double[] { pp, ymax }));

		this.regions.add(first);
		this.regions.add(second);
		this.regions.add(third);
		this.regions.add(fourth);
	}

	public void addRegionsInND(NRectRegion region, NPoint p) throws Exception {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}
			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			this.regions.add(tempRegion);
		}
	}

	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}

	@Override
	public NPoint generateNextTC() {
		NPoint p = null;
		//find max 
		NRectRegion maxregion = null;
		int maxregion_index = 0;
		double maxsize = 0;
		for (int i = 0; i < regions.size(); i++) {
			NRectRegion temp = regions.get(i);
			if (temp.size() > maxsize) {
				maxsize = temp.size();
				maxregion = temp;
				maxregion_index = i;
			}
		}
		regions.remove(maxregion_index);
		p = new NPoint();
		p = randomCreator.randomPoint(maxregion);
		try {
			addRegionsInND(maxregion, p);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return p;
	}


	public void time() {
		int count = 0;
		//regions.add(initRegion);
		NPoint p = randomCreator.randomPoint(initRegion);
		while (count < tcCount) {
			count++;
			// 找出最大区域
			double maxsize = 0;
			NRectRegion maxregion = null;
			int maxregion_index = 0;
			for (int i = 0; i < regions.size(); i++) {
				NRectRegion temp = regions.get(i);
				if (temp.size() > maxsize) {
					maxsize = temp.size();
					maxregion = temp;
					maxregion_index = i;
				}
			}
			//
			regions.remove(maxregion_index);
			//// generate next one test case
			p = new NPoint();
			p = randomCreator.randomPoint(maxregion);
			// add 2^m 次方的
			try {
				addRegionsInND(maxregion, p);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	public static double testFm() {
		int d = 2;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.005;
		long sums = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_RP_ND rt = new ART_RP_ND(min, max, new Random(i * 3), failurePattern);
			temp = rt.run();
			sums += temp;
		}
		long endTime = System.currentTimeMillis();
		double fm = sums / (double) times;
		System.out.println("fm:" + fm + " time:" + ((endTime - startTime) / (double) times));
		return fm;
	}

	
	public static double[] testEm(int dimension, double failrate) {
		int d = dimension;
		int emTime = 6;
		double result[] = new double[emTime];
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 2000;

		int temp = 0;
		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = failrate;
		for (int k = 0; k < emTime; k++) {
			long sums = 0;
			long startTime = System.currentTimeMillis();
			for (int i = 0; i < times; i++) {
				ART_RP_ND rt = new ART_RP_ND(min, max, new Random(i * 3), failurePattern);
				rt.emCount = (k + 1) * 500;
				temp = rt.em();
				sums += temp;
			}
			long endTime = System.currentTimeMillis();
			double em = sums / (double) times;
			result[k] = em;
			System.out.println("em:" + em + " time:" + ((endTime - startTime) / (double) times));
		}
		System.out.println();
		return result;
	}

	public static double testTCTime(int d, int tcCount) {
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double min[] = dataCreator.minCreator(d);
		double max[] = dataCreator.maxCreator(d);

		int times = 1;

		FailurePattern failurePattern = new BlockPattern();
		failurePattern.fail_rate = 0.001;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < times; i++) {
			ART_RP_ND rt = new ART_RP_ND(min, max, new Random(i * 3), failurePattern);
			rt.tcCount = tcCount;
			rt.time2();
		}
		long endTime = System.currentTimeMillis();
		System.out.println((endTime - startTime) / (double) times);
		return ((endTime - startTime) / (double) times);
	}
}
