package test.simulations.art_dc;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.ND.NRectRegion;
import datastructure.failurepattern.FailurePattern;
import datastructure.failurepattern.impl.BlockPattern;
import test.simulations.rrt.RRT_ND;
import util.PaiLie;
import util.data.ZeroOneCreator;

/**
 * @author xijiaxiang
 * @date 2017/11/25 二维DivideAndConquer,使用RRT方法产生测试用例
 */
public class RRT_DC extends RRT_ND {

	public static void main(String[] args) {
		int n = 5;
		ZeroOneCreator dataCreator = new ZeroOneCreator();
		double[] min = dataCreator.minCreator(n);
		double[] max = dataCreator.maxCreator(n);

		int lamda = 100;

		int times = 3000;

		int fm = 0;
		for (int i = 0; i < times; i++) {
			FailurePattern failurePattern = new BlockPattern();
			failurePattern.fail_rate = 0.005;
			RRT_DC dc = new RRT_DC(min, max, failurePattern, new Random(i * 3), 1.5, lamda);
			int temp = dc.run();
			fm += temp;
		}
		System.out.println(fm / (double) times);
	}
	private int lamda;

	ArrayList<NRectRegion> d_queue = new ArrayList<>();

	public RRT_DC(double[] min, double[] max, FailurePattern pattern, Random random, double r, int lamda) {
		super(min, max, pattern, random, r);
		this.lamda = lamda;
	}

	public void addNewRegions(ArrayList<NRectRegion> d_queue, NRectRegion region) {
		// only for two dimension
		// addRegionsIn2D(region);

		// for n dimesnion
		NPoint midPoint = region.getStart().midPointWithAnotherPoint(region.getEnd());
		try {
			addRegionsInND(region, midPoint);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addRegionsIn2D(NRectRegion region) {
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();

		double xmin = start[0];
		double ymin = start[1];
		double xmax = end[0];
		double ymax = end[1];
		double pp = 0.5 * (end[0] - start[0]);
		double qq = 0.5 * (end[1] - start[1]);

		NRectRegion first = new NRectRegion(new NPoint(new double[] { xmin, ymin }),
				new NPoint(new double[] { pp, qq }));
		NRectRegion second = new NRectRegion(new NPoint(new double[] { pp, ymin }),
				new NPoint(new double[] { xmax, qq }));
		NRectRegion third = new NRectRegion(new NPoint(new double[] { pp, qq }),
				new NPoint(new double[] { xmax, ymax }));
		NRectRegion fourth = new NRectRegion(new NPoint(new double[] { xmin, qq }),
				new NPoint(new double[] { pp, ymax }));

		d_queue.add(first);
		d_queue.add(second);
		d_queue.add(third);
		d_queue.add(fourth);
	}

	public void addRegionsInND(NRectRegion region, NPoint p) throws Exception {
		// int regions=(int) Math.pow(2, this.dimension);
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = p.getXn();
		List<List<Double>> result1 = splitRegions(start, pxn);
		List<List<Double>> result2 = splitRegions(pxn, end);
		// System.out.println(result1.size());
		if (result1.size() != result2.size()) {
			throw new Exception("result1's size!=result2's size ,split region wrong");
		}
		for (int i = 0; i < result1.size(); i++) {
			List<Double> temp1 = result1.get(i);
			List<Double> temp2 = result2.get(i);
			double[] newStart = new double[temp1.size()];
			double[] newEnd = new double[temp2.size()];
			for (int j = 0; j < temp1.size(); j++) {
				newStart[j] = temp1.get(j);
				newEnd[j] = temp2.get(j);
			}

			NRectRegion tempRegion = new NRectRegion(new NPoint(newStart), new NPoint(newEnd));
			this.d_queue.add(tempRegion);
		}
	}

	public double calculateRadius(NRectRegion region, int count) {
		if (this.dimension % 2 == 0) {
			int k = this.dimension / 2;
			double kjie = 1;
			for (int i = k; i > 0; i--) {
				kjie *= i;
			}
			double temp = (this.R * region.size() * kjie) / (count * Math.pow(Math.PI, k));

			return Math.pow(temp, 1 / (double) this.dimension);
		} else {
			int k = this.dimension / 2;
			double kjie = 1;
			double k2jie = 1;
			for (int i = k; i > 0; i--) {
				kjie *= i;
			}
			for (int i = (2 * k + 1); i > 0; i--) {
				k2jie *= i;
			}
			double temp = (this.R * region.size() * k2jie)
					/ (kjie * Math.pow(2, 2 * k + 1) * Math.pow(Math.PI, k) * count);
			// System.out.println("return R");
			return Math.pow(temp, 1 / (double) this.dimension);
		}
	}

	public int findLeastTestCaseInD(ArrayList<NPoint> pointsInDi) {
		// NRectRegion region=null;
		int index = -1;
		int count = Integer.MAX_VALUE;
		for (int i = 0; i < d_queue.size(); i++) {
			int temp = 0;
			for (int j = 0; j < this.tests.size(); j++) {
				if (isTCInD(d_queue.get(i), this.tests.get(j))) {
					temp++;
					pointsInDi.add(this.tests.get(j));
				}
			}
			if (temp < count) {
				count = temp;
				index = i;
				// region=d_queue.get(i);
			}
		}
		return index;
	}

	public boolean isTCInD(NRectRegion region, NPoint point) {
		boolean result = true;
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		double[] pxn = point.getXn();
		for (int i = 0; i < start.length; i++) {
			if (pxn[i] < start[i] || pxn[i] > end[i]) {
				result = false;
				break;
			}
		}
		return result;
	}


	public NPoint randomTCByRRT(NRectRegion region, ArrayList<NPoint> tests) {
		NPoint point = null;
		double radius = calculateRadius(region, tests.size());

		boolean flag = true;
		while (flag) {
			flag = false;
			point = randomCreator.randomPoint(region);
			for (int i = 0; i < tests.size(); i++) {
				// 排除区域是圆
				// 计算距离
				double[] tested = tests.get(i).getXn();
				double distance = 0;
				double[] untested = point.getXn();
				for (int j = 0; j < this.dimension; j++) {
					distance += Math.pow((tests.get(i).getXn()[j] - untested[j]), 2);
				}
				distance = Math.sqrt(distance);
				if (distance < radius) {
					flag = true;
					// break;
				}
				/*
				 * //排除区域是正方形 if(Math.abs(p.p-tests.get(i).p)<radius){
				 * if(Math.abs(p.q-tests.get(i).q)<radius){ flag=true; } }
				 */
			}

		}
		return point;
	}

	public NPoint randomTCByRT(NRectRegion region, ArrayList<NPoint> tests) {
		NPoint point = new NPoint();
		double[] start = region.getStart().getXn();
		double[] end = region.getEnd().getXn();
		// NPoint point = new NPoint();
		point.dimension = this.dimension;
		double[] xn = new double[this.dimension];
		for (int i = 0; i < xn.length; i++) {
			xn[i] = random.nextDouble() * (end[i] - start[i]) + start[i];
		}
		point.setXn(xn);
		return point;
	}

	@Override
	public int run() {
		int count = 0;
		int depth = 0;

		NPoint p = randomCreator.randomPoint();
		d_queue.add(new NRectRegion(new NPoint(this.min), new NPoint(this.max)));

		ArrayList<NRectRegion> temp_d_queue = new ArrayList<>();
		// t_queue.add(new )
		while (this.failPattern.isCorrect(p)) {
			// select the least populated sub-domain Di from d_queue;
			/* that is, find the least number of test cases in all RectRegion */
			this.tests.add(p);

			count++;

			ArrayList<NPoint> pointsInDi = new ArrayList<>();
			int indexOfLeastTCRegion = findLeastTestCaseInD(pointsInDi);
			NRectRegion leastTCRegion = d_queue.get(indexOfLeastTCRegion);

			p = randomTCByRRT(leastTCRegion, pointsInDi);
			// p=randomTCByRT(leastTCRegion,pointsInDi);

			if (pointsInDi.size() == lamda) {
				// remove this region and add it into temp
				this.d_queue.remove(indexOfLeastTCRegion);
				// add new regions into temp
				// temp_d_queue.add()
				addNewRegions(temp_d_queue, leastTCRegion);
				// leastTCRegion.getStart();
			}
			if (d_queue.size() == 0) {
				depth++;
				d_queue = temp_d_queue;
				temp_d_queue = new ArrayList<>();
			}
		}

		return count;
	}

	public List<List<Double>> splitRegions(double[] start, double[] end) {
		ArrayList<double[]> values = new ArrayList<>();
		for (int i = 0; i < start.length; i++) {
			double[] temp = new double[2];

			temp[0] = start[i];
			temp[1] = end[i];
			values.add(temp);
		}

		ArrayList<List<Double>> result = new ArrayList<>();
		PaiLie.per(values, 0, new ArrayList<>(), result);
		return result;
	}
}
