package test;

import java.util.Random;

import datastructure.ND.NPoint;
import datastructure.failurepattern.FailurePattern;
import util.RandomCreator;

public abstract class ART {
	public double[] min;
	public double[] max;
	public int dimension;
	public Random random;//随机数，可以选用CRandomNumber和MerseneTwisterFast(这个较好)
	public int pmCount;//算pm时设置的测试用例数量
	public int emCount=1000;//算em时设置的测试用例数量
	public int tcCount=1000;//算时间设置的测试用例数量
	public FailurePattern failPattern;//失效模式
	public double totalArea;
	public RandomCreator randomCreator;//随机生成器（特定区域，特定区域组中生成）

	public ART(double[] min, double[] max, Random random, FailurePattern failurePattern) {
		this.min = min;
		this.max = max;

		this.dimension = min.length;
		this.random = random;

		failurePattern.min = min;
		failurePattern.max = max;
		failurePattern.dimension = dimension;
		failurePattern.random = random;
		failurePattern.genFailurePattern();
		this.failPattern = failurePattern;

		totalArea = 1.0;
		for (int i = 0; i < this.dimension; i++) {
			totalArea *= (max[i] - min[i]);
		}

		randomCreator = new RandomCreator(random, dimension, min, max);
		
		this.pmCount=(int) Math.round(Math.log(0.5)/Math.log(1-failPattern.fail_rate)); //95% CI
	}

	public abstract NPoint generateNextTC();//核心方法，生成测试用例的方法
	
 
	public int run(){
		int count = 0;
		NPoint p = generateNextTC();
		while (this.failPattern.isCorrect(p)) {
			count++;
			p = generateNextTC();
		}
		return count;
	}
	
	public int em() {
		int emTemp = 0;
		NPoint p = generateNextTC();
		int count = 0;
		while (count < emCount) {
			if (!this.failPattern.isCorrect(p)) {
				emTemp++;
				break;//直接跳出
			}
			count++;
			p = generateNextTC();
		}
		return emTemp;
	}
	
	public  void time2(){
		//生成测试用例所需时间
		int count = 0;
		NPoint p = generateNextTC();
		while (count < tcCount) {
			count++;
			p = generateNextTC();
		}
	}
	
	public  boolean pm(){
		boolean Detected = false;
		int count = 0;
		NPoint p = generateNextTC();
		//System.out.println(pmCount);
		while (count < pmCount) {
			if (!this.failPattern.isCorrect(p)) {
				Detected = true;
				break;
			}
			count++;
			p = generateNextTC();
		}
		return Detected;
	}
	

}
