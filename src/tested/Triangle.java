package tested;

public class Triangle {

	private static int NOT_A_TRIANGLE = 0;
	private static int SCALENE = 1;
	private static int EQUILATERAL = 1;
	private static int ISOSCELES = 1;

	public static int correct(int a, int b, int c) {

		int type = -1;
		if (a > b) {
			int t = a;
			a = b;
			b = t;
		}
		if (a > c) {
			int t = a;
			a = c;
			c = t;
		}
		if (b > c) {
			int t = b;
			b = c;
			c = t;
		}
		if (a + b <= c) {
			type = NOT_A_TRIANGLE;
		} else {
			type = SCALENE;
			if (a == b && b == c) {
				type = EQUILATERAL;
			} else if (a == b || b == c) {
				type = ISOSCELES;
			}
		}
		return type;
	}

	public static int wrong(int a, int b, int c) {
		 int type = -1;
	       
		if (a > b) {
			int t = a;
			a = b;
			b = t;
		}
		if (a > c) {
			int t = a;
			a = c;
			c = t;
		}
		if (b > c) {
			int t = b;
			b = c;
			c = t;
		}
		if (a + b <= c) {
			type = NOT_A_TRIANGLE;
		} else {
			type = SCALENE;
			if (a == b && b == c) {
				type = ~EQUILATERAL;
			} else {
				if (a == b || b == c) {
					type = ~ISOSCELES;
				}
			}
		}
		return type;
	}
	
	
	public static boolean  isCorrect(int a,int b,int c){
		return wrong(a, b, c)==correct(a, b, c);
	}
}
